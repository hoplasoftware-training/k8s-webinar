# CHAPTER 4

In these labs we will cover all the topics learned on de Chapter4.

- We will learn how to use Advanced Scheduling with ***Labels Selector*** and ***NodeSelector***

- We will learn how to use the ***Node Affinity*** functionality to schedule the pods in the nodes that we consider appropriate or more convenient.

- We will learn how to use ***Taints*** and ***Tolerations*** to either schedule pods on certain nodes or to prevent pods from being scheduled on certain nodes.

---

## LAB1 - LABELS SELECTOR & NODE SELECTOR

In this lab we will work with advanced scheduling options such as ***Labels Selector*** and ***Node Selector.***

**LABELS SELECTOR**

For the Labels Selector lab we are going to deploy 3 services from the command line. Each of them will automatically generate a Selector to a label whose name-value will be the same as the name of the service. We will "play" to associate the different pods that we deploy with the services through those Labels Selectors.


1- Create the namespace for these labs.
```
student@master:~$ kubectl create ns ch4
```

2- Create 3 services from the command line with the names: service1, service2 and service3.
```
student@master:~$ kubectl create svc clusterip service1 --tcp=8060:80 -n ch4

service/service1 created

student@master:~$ kubectl create svc clusterip service2 --tcp=8070:80 -n ch4

service/service2 created

student@master:~$ kubectl create svc clusterip service3 --tcp=8080:80 -n ch4

service/service3 created
```
3- Check the deployment of the services, as well as the associated endpoints.
```
student@master:~$ kubectl get svc -n ch4

NAME       TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service1   ClusterIP   10.97.34.245   <none>        8060/TCP   73s
service2   ClusterIP   10.96.93.233   <none>        8070/TCP   59s
service3   ClusterIP   10.96.141.59   <none>        8080/TCP   46s

student@master:~$ kubectl get ep -n ch4

NAME       ENDPOINTS   AGE
service1   <none>      79s
service2   <none>      65s
service3   <none>      52s
```

4- Check by using describe or get the selector that has one of the services configured.
```
student@master:~$ kubectl describe svc service1 -n ch4

Name:              service1
Namespace:         ch4
Labels:            app=service1
Annotations:       <none>
Selector:          app=service1
Type:              ClusterIP
IP:                10.97.34.245
Port:              8060-80  8060/TCP
TargetPort:        80/TCP
Endpoints:         <none>
Session Affinity:  None
Events:            <none>

student@master:~$ kubectl get svc service1 -n ch4 -o yaml --export 

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: service1
  name: service1
  selfLink: /api/v1/namespaces/ch4/services/service1
spec:
  ports:
  - name: 8060-80
    port: 8060
    protocol: TCP
    targetPort: 80
  selector:
    app: service1
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```

5- Deploy 4 pods from the command line. 3 of them (nginx, nginx-txct and httpd) will "connect" them to the previously deployed services.
The fourth pod (busybox) will be used for connectivity testing.
```
student@master:~$ kubectl run -n ch4 nginx --image=nginx --restart=Never

pod/nginx created

student@master:~$ kubectl run -n ch4 nginx-txt --image=nginxdemos/hello:plain-text --restart=Never

pod/nginx-txt created

student@master:~$ kubectl run -n ch4 httpd --image=httpd --restart=Never

pod/httpd created

student@master:~$ kubectl run -n ch4 busybox --image=radial/busyboxplus:curl --restart=Never -- sleep 7200

pod/busybox created

student@master:~$ kubectl get pod -n ch4 -o wide

NAME        READY   STATUS    RESTARTS   AGE     IP              NODE                     NOMINATED NODE   READINESS GATES
busybox     1/1     Running   0          3m15s   11.11.190.179   worker1   <none>           <none>
httpd       1/1     Running   0          3m28s   11.11.190.174   worker1   <none>           <none>
nginx       1/1     Running   0          3m55s   11.11.190.173   worker1   <none>           <none>
nginx-txt   1/1     Running   0          3m38s   11.11.190.177   worker1   <none>           <none>
```

6- At the moment there is nothing to connect the services with the deployed pods. The only way to access the web servers deployed on pods is to connect to the pod's ip.
```
student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://11.11.190.174

<html><body><h1>It works!</h1></body></html>

student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://11.11.190.173

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://11.11.190.177

Server address: 11.11.190.177:80
Server name: nginx-txt
Date: 04/Jun/2020:13:23:00 +0000
URI: /
Request ID: 8ca66d98a6c028dd9946536b4c081916
```

7- "Connect" the services to the pods. To do this, label the pods with the labels and values that the services are "looking for" through the label selector they have defined.
```
student@master:~$ kubectl label pod nginx app=service1 -n ch4

pod/nginx labeled

student@master:~$ kubectl label pod httpd app=service1 -n ch4

pod/httpd labeled

student@master:~$ kubectl label pod nginx-txt app=service2 -n ch4

pod/nginx-txt labeled
```

8- Check the labels applied to the pods, as well as the status of the endpoints after the labelling.
```
student@master:~$ kubectl get pod -l app=service1 -n ch4 -o wide

NAME    READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
httpd   1/1     Running   0          11m   11.11.190.174   worker1   <none>           <none>
nginx   1/1     Running   0          12m   11.11.190.173   worker1   <none>           <none>

student@master:~$ kubectl get pod -l app=service2 -n ch4 -o wide

NAME        READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
nginx-txt   1/1     Running   0          12m   11.11.190.177   worker1   <none>           <none>

student@master:~$ kubectl get ep -n ch4

NAME       ENDPOINTS                           AGE
service1   11.11.190.173:80,11.11.190.174:80   76m
service2   11.11.190.177:80                    76m
service3   <none>                              76m
```
9- Run several curls from the busybox pod to service1 and service2.
```
student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://service1.ch4.svc.cluster.local:8060

<html><body><h1>It works!</h1></body></html>

student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://service1.ch4.svc.cluster.local:8060

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://service2.ch4.svc.cluster.local:8070

Server address: 11.11.190.177:80
Server name: nginx-txt
Date: 04/Jun/2020:13:33:22 +0000
URI: /
Request ID: 92c793da59e1cc9fe76c6ff5524e0fcf
```

10- Re-label the httpd pod to put the correct label on it (app=service3).
```
student@master:~$ kubectl label pod httpd app=service3 --overwrite -n ch4

pod/httpd labeled
```

11- Check what has changed on the endpoints after the previous point.
```
student@master:~$ kubectl get ep -n ch4

NAME       ENDPOINTS          AGE
service1   11.11.190.173:80   82m
service2   11.11.190.177:80   82m
service3   11.11.190.174:80   82m
```

12- Run a curl from the busybox pod to the service3.
```
student@master:~$ kubectl exec -n ch4 -ti busybox -- curl http://service3.ch4.svc.cluster.local:8080

<html><body><h1>It works!</h1></body></html>
```

13- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch4

pod "busybox" deleted
pod "httpd" deleted
pod "nginx" deleted
pod "nginx-txt" deleted
service "service1" deleted
service "service2" deleted
service "service3" deleted
```

**NODE SELECTOR**

In the Node Selector laboratory we are going to use 2 deploys(deploy-nginx-txt y deploy-nginx-web) with which we are going to check how to do deployments on specific worker nodes, using labels put on the nodes and the Node Selector feature configured in the deploys.

1- Deploy 2 deployments using the yaml files provided. **deploy-nginx-txt.yaml** and **deploy-nginx-web.yaml**
```
student@master:~$ cat<<EOF> deploy-nginx-txt.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-txt
  name: nginx-txt
  namespace: ch4
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-txt
  strategy: {}
  template:
    metadata:
      labels:
        app: nginx-txt
    spec:
      containers:
      - image: nginxdemos/hello:plain-text
        name: hello
        resources: {}
      nodeSelector:
        app: nginx-txt
EOF
kubectl apply -f deploy-nginx-txt.yaml

deployment.apps/nginx-txt created

student@master:~$ cat<<EOF>deploy-nginx-web.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-web
  name: nginx-web
  namespace: ch4
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-web
  strategy: {}
  template:
    metadata:
      labels:
        app: nginx-web
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
      nodeSelector:
         app: nginx-web
EOF

kubectl apply -f deploy-nginx-web.yaml


deployment.apps/nginx-web created
```
2- Check what the deployment status is and the status of the pods.
```
student@master:~$ kubectl get deploy -n ch4 -o wide

NAME        READY   UP-TO-DATE   AVAILABLE   AGE    CONTAINERS   IMAGES                        SELECTOR
nginx-txt   0/1     1            0           2m4s   hello        nginxdemos/hello:plain-text   app=nginx-txt
nginx-web   0/1     1            0           2m1s   nginx        nginx                         app=nginx-web

student@master:~$ kubectl get pod -n ch4 -o wide

NAME                         READY   STATUS    RESTARTS   AGE     IP       NODE     NOMINATED NODE   READINESS GATES
nginx-txt-7d48849974-stp5s   0/1     Pending   0          2m55s   <none>   <none>   <none>           <none>
nginx-web-65867444f-c8cxw    0/1     Pending   0          2m52s   <none>   <none>   <none>           <none>
```

3- Check why the pods are not deploying.
See that the reason is that there is no node that matches the Node-Selector requirement that the pods have.
```
student@master:~$ kubectl describe pod nginx-txt-7d48849974-stp5s -n ch4

Name:           nginx-txt-7d48849974-stp5s
Namespace:      ch4
Priority:       0
Node:           <none>
Labels:         app=nginx-txt
                pod-template-hash=7d48849974
Annotations:    <none>
Status:         Pending
IP:             
IPs:            <none>
Controlled By:  ReplicaSet/nginx-txt-7d48849974
Containers:
  hello:
    Image:        nginxdemos/hello:plain-text
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-jzmb5 (ro)
Conditions:
  Type           Status
  PodScheduled   False 
Volumes:
  default-token-jzmb5:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-jzmb5
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  app=nginx-txt
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason            Age                  From               Message
  ----     ------            ----                 ----               -------
  Warning  FailedScheduling  48s (x4 over 3m41s)  default-scheduler  0/3 nodes are available: 3 node(s) didn't match node selector.
  
student@master:~$ kubectl describe pod nginx-web-65867444f-c8cxw -n ch4

Name:           nginx-web-65867444f-c8cxw
Namespace:      ch4
Priority:       0
Node:           <none>
Labels:         app=nginx-web
                pod-template-hash=65867444f
Annotations:    <none>
Status:         Pending
IP:             
IPs:            <none>
Controlled By:  ReplicaSet/nginx-web-65867444f
Containers:
  nginx:
    Image:        nginx
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-jzmb5 (ro)
Conditions:
  Type           Status
  PodScheduled   False 
Volumes:
  default-token-jzmb5:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-jzmb5
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  app=nginx-web
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason            Age                  From               Message
  ----     ------            ----                 ----               -------
  Warning  FailedScheduling  75s (x8 over 9m34s)  default-scheduler  0/3 nodes are available: 3 node(s) didn't match node selector.
```

4- Check if the nodes have the necessary labels(app=nginx-web y app=nginx-txt) so that the deployments can be deployed, and if not, label them so that the deployments can be done.
```
student@master:~$ kubectl get node --show-labels |grep app=nginx-

student@master:~$ kubectl label node worker1 app=nginx-web

node/worker1 labeled

student@master:~$ kubectl label node worker2 app=nginx-txt

node/worker2 labeled
```

5- Check that the nodes are correctly labeled and if so, check that the pods have been deployed in the corresponding worker node according to the label.
```
student@master:~$ kubectl get node --show-labels |grep app=nginx-

worker1   Ready    worker   99d   v1.17.6   app=nginx-web,beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=ubs1804-k8s117-worker1,kubernetes.io/os=linux,node-role.kubernetes.io/worker=worker

worker2   Ready    worker   99d   v1.17.6   app=nginx-txt,beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=ubs1804-k8s117-worker2,kubernetes.io/os=linux,node-role.kubernetes.io/worker=worker

student@master:~$ kubectl get pod -n ch4 -o wide

NAME                         READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
nginx-txt-7d48849974-stp5s   1/1     Running   0          27m   11.11.218.228   worker2   <none>           <none>
nginx-web-65867444f-c8cxw    1/1     Running   0          27m   11.11.190.180   worker1   <none>           <none>
```


6- Scale the deployments with 3 replicas each and check that the new pods are deployed on the same worker node as the existing one.
```
student@master:~$ kubectl scale --replicas=3 deploy/nginx-web -n ch4

deployment.apps/nginx-web scaled

student@master:~$ kubectl scale --replicas=3 deploy/nginx-txt -n ch4

deployment.apps/nginx-txt scaled

student@master:~$ kubectl get pod -n ch4 -o wide

NAME                         READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
nginx-txt-7d48849974-dzfjf   1/1     Running   0          46s   11.11.218.236   worker2   <none>           <none>
nginx-txt-7d48849974-sddm7   1/1     Running   0          46s   11.11.218.235   worker2   <none>           <none>
nginx-txt-7d48849974-stp5s   1/1     Running   0          31m   11.11.218.228   worker2   <none>           <none>
nginx-web-65867444f-c8cxw    1/1     Running   0          31m   11.11.190.180   worker1   <none>           <none>
nginx-web-65867444f-fpjmq    1/1     Running   0          50s   11.11.190.187   worker1   <none>           <none>
nginx-web-65867444f-v8qfh    1/1     Running   0          50s   11.11.190.183   worker1   <none>           <none>
```

7- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch4

pod "nginx-txt-7d48849974-dzfjf" deleted
pod "nginx-txt-7d48849974-sddm7" deleted
pod "nginx-txt-7d48849974-stp5s" deleted
pod "nginx-web-65867444f-c8cxw" deleted
pod "nginx-web-65867444f-fpjmq" deleted
pod "nginx-web-65867444f-v8qfh" deleted
deployment.apps "nginx-txt" deleted
deployment.apps "nginx-web" deleted
replicaset.apps "nginx-txt-7d48849974" deleted
replicaset.apps "nginx-web-65867444f" deleted
```

---


## LAB2 - NODE AFFINITY

In this lab we will "play" with the Node Affinty property to display pods on nodes with certain tags. We will also have to take into account the available resources.



1- Create labels on the nodes with the type of disk and the speed of the disk.
```
student@master:~$ kubectl label node <worker1> disktype=hdd diskspeed1=hdd10000rpm diskspeed3=hdd5400rpm
student@master:~$ kubectl label node <worker2> disktype=ssd diskspeed2=hdd7200rpm 
```
2- Check that the labels have been generated correctly.
```
student@master:~$ kubectl get node --show-labels|grep disk
```

3- We created 2 deploy. 
deploy-nginx-hdd10000.yaml whose Node Affinity rules would be:
Imperative: To deploy on nodes with label disktype=sdd or disktype=hdd
Preference: To deploy on nodes with label diskspeed1=hdd10000rpm
It also has certain requirements and resource limits.

deploy-nginx-hdd7200.yaml whose Node Affinity rules would be:
Imperative: Deploy on nodes with label disktype=sdd or disktype=hdd
Preference1: Deploy to nodes with label diskspeed2=hdd72000rpm
Preference2: Deploy to nodes with label diskspeed3=hdd54000rpm

```
student@master:~$ cat<<EOF>deploy-nginx-hdd10000.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-hdd10000
  name: nginx-hdd10000
  namespace: ch4
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-hdd10000
  strategy: {}
  template:
    metadata:
      labels:
        app: nginx-hdd10000
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: disktype
                operator: In
                values:
                - ssd
                - hdd
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            preference:
              matchExpressions:
              - key: diskspeed1
                operator: In
                values:
                - hdd10000rpm
      containers:
      - image: nginxdemos/hello:plain-text
        name: hello
        resources:
          limits:
            cpu: 200m
            memory: 2.5Gi
          requests:
            cpu: 100m
            memory: 2.5Gi
EOF

kubectl apply -f deploy-nginx-hdd10000.yaml

student@master:~$ cat<<EOF>deploy-nginx-hdd7200.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-hdd7200
  name: nginx-hdd7200
  namespace: ch4
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-hdd7200
  strategy: {}
  template:
    metadata:
      labels:
        app: nginx-hdd7200
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: disktype
                operator: In
                values:
                - ssd
                - hdd
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            preference:
              matchExpressions:
              - key: diskspeed2
                operator: In
                values:
                - hdd7200rpm
          - weight: 1
            preference:
              matchExpressions:
              - key: diskspeed3
                operator: In
                values:
                - hdd5400rpm
      containers:
      - image: nginxdemos/hello:plain-text
        name: hello
        resources: {}
EOF

kubectl apply -f deploy-nginx-hdd7200.yaml
```
		
4- Check the location of the pods according to the defined Affinity Nodes.
```
student@master:~$ kubectl get pod -n ch4 -o wide
```

According to the configuration made in each Node Affinity deploy, the pod nginx-hdd10000 should be deployed in worker1 and the pod nginx-hdd7200 should be deployed in worker2.

5- Scaled the nginx-hdd7200 deploy to have 10 replicas.
```
student@master:~$ kubectl scale --replicas=10 deploy nginx-hdd7200 -n ch4
```

6- Check the creation, status and location of the 10 replicas.
```
student@master:~$ kubectl get pod -n ch4 -o wide
```

All or most of the pods have been deployed, according to the Node Affinity rules, configured on the worker2. Note that the weights set in the Node Affinity rules also influence where the pods are scheduled.


7- Scale the nginx-hd10000 deploy to have 3 replicas.
```
student@master:~$ kubectl scale --replicas=3 deploy nginx-hdd10000 -n ch4
```

8- Check the creation, status and location of the 3 replicas.
```
student@master:~$ kubectl get pod -n ch4 -o wide
```

Note that of the 2 new replicas that have been scheduled, one is deployed on the worker2 and the other is in a Pending state.
As the resources available in worker1 are not enough, the second replica has been scheduled on worker2, as the Node Affinity rules do allow scheduling on that node.
However, the third replica cannot be scheduled on any of the nodes because the available resources do not match the requirements indicated in the definition of the pod.

To deploy more than 2 replicas of the deploy-nginx10000 we should increase the nodes' resources or modify the resource limits defined in the deployment.

9- Scale the deploy deploy-nginx10000 to 2 replicas and the deploy deploy-nginx7200 to 1 replica.
```
k scale --replicas=2 deploy nginx-hdd10000 -n ch4

k scale --replicas=1 deploy nginx-hdd7200 -n ch4
```
10- Scale the deploy deploy-nginx7200 again so that it has 10 replicas.
```
k scale --replicas=10 deploy nginx-hdd7200 -n ch4
```

11- Check the creation, status and location of the 3 replicas.
```
student@master:~$ kubectl get pod -n ch4 -o wide
```

Note that the replicas have now been divided between the two workers, unlike the one we saw in point 6. This is because the resources available now are not the same (they are less) that we had in point 6 (the deploy deploy-nginx10000 only had one replica that was in the worker1).
The programming takes into account on one hand the imperative rule of Node Affinity, and on the other hand the available resources in the worker nodes.

12- Removes all elements deployed in the ch4 namespace
```
student@master:~$ kubectl label node <worker1> disktype- diskspeed1- diskspeed3-

student@master:~$ kubectl label node <worker2> disktype- diskspeed2-

student@master:~$ kubectl delete all --all -n ch4
```
---

## LAB3 - TAINTS&TOLERATIONS

In this lab we will be working with Taint and Tolerations.
We will configure Taints in the worker nodes and we will see how it affects the scheduling of the deployments.
We'll also use Tolerations in 
 the definition of pods to perform pod scheduling on nodes with taints.
 
 
1- Checks if the nodes of the cluster have defined taints.
```
student@master:~$ kubectl describe node <nombre worker1>

student@master:~$ kubectl describe node <nombre worker2>

student@master:~$ kubectl describe node <nombre master1>
```


2- Deploy the following 2 pods.
```
student@master:~$ cat<<EOF>pod-nginx.yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-nginx
  namespace: ch4
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
EOF

kubectl apply -f pod-nginx.yaml

student@master:~$ cat<<EOF>pod-nginx-tolerations.yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-env-noexecute
  namespace: ch4
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
  tolerations:
  - key: "env"
    operator: "Exists"
    effect: "NoExecute"
EOF

kubectl apply -f pod-nginx-tolerations.yaml
```

3- Check the deployment, status and location of the pods.
```
student@master:~$ kubectl get pod -n ch4 -o wide

AME                  READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
nginx-env-noexecute   1/1     Running   0          9s    11.11.190.130   worker1   <none>           <none>
pod-nginx             1/1     Running   0          15s   11.11.190.190   worker1   <none>           <none>
```

4- Configure the following taint in the worker node(s) where the pods are running.
```
student@master:~$ kubectl taint node <woker node> env=production:NoExecute
```

Note that the pod nginx-pod has been evicted.
Do you know why?
And why the pod nginx-env-noexecute has not been evicted?

5- Removes from the worker nodes the taint defined in point 4 and the pod nginx-env-noexecute.
```
student@master:~$ kubectl taint node <woker node> env=production:NoExecute-

student@master:~$ kubectl delete pod nginx-env-noexecute -n ch4
```

6- Create the following taints on the worker nodes. One taint in each node.
```
student@master:~$ kubectl taint node <name worker1> env=production:NoSchedule

student@master:~$ kubectl taint node <name worker2> env=develop:NoSchedule
```

7- Check the correct creation of the taint.
```
student@master:~$ kubectl describe node <nombre worker1>|grep -i taints
Taints:             env=production:NoExecute

student@master:~$ kubectl describe node <nombre worker2>|grep -i taints
Taints:             env=develop:NoSchedule
```

4- Deploy the pod-nginx.yaml in the cluster.
```
student@master:~$ cat<<EOF>pod-nginx.yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-nginx
  namespace: ch4
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
EOF

kubectl apply -f pod-nginx.yaml
```

5- Check the deployment status.
```
student@master:~$ kubectl get pod -n ch4 -o wide
```

6- As we see the state of the pod is Pending. Check out the reason.
```
student@master:~$ kubectl describe pod nginx -n ch4
```

In the section Events you can see the reason.
```
Events:
  Type     Reason            Age                From               Message
  ----     ------            ----               ----               -------
  Warning  FailedScheduling  26s (x2 over 89s)  default-scheduler  0/3 nodes are available: 3 node(s) had taints that the pod didn't tolerate.
```

7- Make a copy of the pod-nginx.yaml definition file.
```
student@master:~$ cp pod-nginx.yaml pod.nginx.yam.orig
```

8- Modify the pod-nginx.yaml definition file so that when it is deployed the pod is scheduled on the worker2 node.
```
tolerations:
  - key: ""
    operator: ""
    value: ""
    effect: ""
```

9- Once the pod-nginx.yaml file is modified, deploy it and check the status of the deployment and if the pod is running in the correct worker node.
```
student@master:~$ kubectl apply -f pod-nginx.yaml

student@master:~$ kubectl get pod -n ch4 -o wide
NAME        READY   STATUS    RESTARTS   AGE     IP              NODE                     NOMINATED NODE   READINESS GATES
pod-nginx   1/1     Running   0          8m42s   11.11.218.206   worker2   <none>           <none>
```
10- Modify the definition file pod-nginx-tolerations.yaml so that when deploying it the pod is deployed on the worker1.
```
student@master:~$ kubectl apply -f pod-nginx-toleration.yaml
```

11- Check if the change has been made correctly. If so, the pod should be running on the worker1 node.
```
student@master:~$ kubectl get pod -n ch4 -o wide

NAME                  READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
nginx-env-noexecute   1/1     Running   0          55s   11.11.190.129   worker1   <none>           <none>
pod-nginx             1/1     Running   0          14m   11.11.218.206   worker2   <none>           <none>
```

12- Delete the elements created in this lab.
```
student@master:~$ kubectl delete all --all -n ch4

student@master:~$ kubectl taint node <worker1> env=production:NoExecute-

student@master:~$ kubectl taint node <worker2> env=develop:NoSchedule-
```

## LAB4 - JOBS AND CRONJOBS

In this lab we will be working with **jobs** and **cronjobs**.
We're going to create a job that will run a countdown.
We will also create a cronjob that will simulate the planning of a DB backup execution.  
For it we will use the following files yaml of definition: ***job.yaml*** and ***cronjob.yaml***.

1- Take a look at the contents of the job.yaml file to understand the job definition.
```
student@master:~$ cat job.yaml
```

2- Using the file job.yaml we are going to deploy a job called countdown-10s, and as its name indicates it is going to be executed and make a 10 seconds countdown.
```
student@master:~$ cat<<EOF>job.yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: countdown-10s
  namespace: ch4
spec:
  completions: 1
  template:
    metadata:
      name: countdown-10s
    spec:
      containers:
      - name: countdown-10s
        image: busybox
        command: ["/bin/sh","-c"]
        args: ["echo -e \"Countdown 10 secs:\n\";for i in 10 9 8 7 6 5 4 3 2 1; do echo \$i; sleep 1 ; done; echo -e \"\nBOOM!!!\n\""]
        #args: ["echo 'consuming a message'; sleep 10"]
      restartPolicy: OnFailure
EOF

kubectl apply -f job.yaml

job.batch/countdown-10s created

student@master:~$ kubectl get job -n ch4

NAME            COMPLETIONS   DURATION   AGE
countdown-10s   1/1           15s        29s

student@master:~$ kubectl get pod -n ch4

NAME                  READY   STATUS      RESTARTS   AGE
countdown-10s-fgfql   0/1     Completed   0          46s

student@master:~$ kubectl logs <pod-name> -n ch4

Countdown 10 secs:

10
9
8
7
6
5
4
3
2
1

BOOM!!!

```

3- Take a look at the contents of the cronjob.yaml file to understand the job definition.
```
student@master:~$ cat cronjob.yaml
```

4- Deploy the cronjob using the yaml definition file.
```
student@master:~$ cat<<EOF>cronjob.yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: backup-bbdd
  namespace: ch4
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            args:
            - /bin/sh
            - -c
            - "echo -e \"\nStarting to backup the database at: `date`.\n\";sleep 5;echo -e \"Backup of the DB successfully completed.\n\"; echo -e \"Backup done at: `date`\n\""  
          restartPolicy: OnFailure
EOF

kubectl apply -f cronjob.yaml

cronjob.batch/backup-bbdd created
```

5- Check that the cronjob, job and pod are deployed correctly.
```
student@master:~$ kubectl get cronjob -n ch4

NAME          SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
backup-bbdd   */1 * * * *   False     1        2s              18s

student@master:~$ kubectl get job -n ch4

NAME                     COMPLETIONS   DURATION   AGE
backup-bbdd-1591533600   1/1           9s         23s

student@master:~$ kubectl get pod -n ch4

NAME                           READY   STATUS              RESTARTS   AGE
backup-bbdd-1591533600-mb5jl   0/1     Completed           0          64s
backup-bbdd-1591533660-2gm94   0/1     ContainerCreating   0          4s
```

6- By executing a cronjob description we will be able to observe characteristics or cofigurations that apply by default to the cronjobs as they are:
Successful Job History Limit: 3
Failed Job History Limit: 1

As well as other configuration options that we have not established:
Starting Deadline Seconds: <unset>
Selector: <unset>
Parallelism: <unset>
Completions: <unset>
```
student@master:~$ kubectl describe cronjob backup-bbdd -nch4

Name:                          backup-bbdd
Namespace:                     ch4
Labels:                        <none>
Annotations:                   kubectl.kubernetes.io/last-applied-configuration:
                                 {"apiVersion":"batch/v1beta1","kind":"CronJob","metadata":{"annotations":{},"name":"backup-bbdd","namespace":"ch4"},"spec":{"jobTemplate":...
Schedule:                      */1 * * * *
Concurrency Policy:            Allow
Suspend:                       False
Successful Job History Limit:  3
Failed Job History Limit:      1
Starting Deadline Seconds:     <unset>
Selector:                      <unset>
Parallelism:                   <unset>
Completions:                   <unset>
Pod Template:
  Labels:  <none>
  Containers:
   hello:
    Image:      busybox
    Port:       <none>
    Host Port:  <none>
    Args:
      /bin/sh
      -c
      echo -e "
      Starting to backup the database at: `date`.
      ";sleep 5;echo -e "Backup of the DB successfully completed.
      "; echo -e "Backup done at: `date`
      "
    Environment:     <none>
    Mounts:          <none>
  Volumes:           <none>
Last Schedule Time:  Sun, 07 Jun 2020 14:42:00 +0200
Active Jobs:         <none>
Events:
  Type    Reason            Age    From                Message
  ----    ------            ----   ----                -------
  Normal  SuccessfulCreate  2m37s  cronjob-controller  Created job backup-bbdd-1591533600
  Normal  SawCompletedJob   2m27s  cronjob-controller  Saw completed job: backup-bbdd-1591533600, status: Complete
  Normal  SuccessfulCreate  97s    cronjob-controller  Created job backup-bbdd-1591533660
  Normal  SawCompletedJob   87s    cronjob-controller  Saw completed job: backup-bbdd-1591533660, status: Complete
  Normal  SuccessfulCreate  37s    cronjob-controller  Created job backup-bbdd-1591533720
  Normal  SawCompletedJob   27s    cronjob-controller  Saw completed job: backup-bbdd-1591533720, status: Complete
```
7- Finally check the logs of some of the completed pods to verify the execution they have done.
```
student@master:~$ kubectl logs <pod-name> -n ch4

Starting to backup the database at: Sun Jun  7 12:40:04 UTC 2020.

Backup of the DB successfully completed.

Backup done at: Sun Jun  7 12:40:09 UTC 2020

```

8- Remove the elements created in this lab.
```
student@master:~$ kubectl delete all --all -n ch4
```
