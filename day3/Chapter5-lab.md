# Chapter 5
In this Lab we will cover following concepts
- We will learn how to manage data using different Kubernetes features such us __volumes__, __configurations__, __secrets__ and __persitent volumes__.

- We will use different volumes to deploy a dummy-webserver changing content using deployed volumes.

- And lastly we will learn and configure Storage class, in this case a nfs strage type.

---


## Lab1 - Using Kubernetes volumes

In this lab we will learn basic Volumes
### __Using Volumes inside Pods.__

1 - First we will create a pod with two containers and 3 different volumes:

- test-container1 --> host-volume on /data

- test-container2 --> emtpy-volume on /data and tmpfs volume on /onmemory

> NOTE:
> We have added an specific nodeName key to execute this pod on this node.

What we want to check is that our deployment is not going to be **Running** until a specific volume exits, in this case the host-volume.

First we create a namespace for the whole chapters lab:
```
student@master:~$ kubectl create namespace ch5
```

Create the explained multipod with the volunes:

#### [test-pod.pod.yml](./test-pod.pod.yml)

```
student@master:~$ cat<<EOF> test-pod.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod
  namespace: ch5
spec:
  nodeName: worker1
  containers:
  - image: hopla/test:1.0
    name: test-container1
    volumeMounts:
    - mountPath: /data
      name: host-volume
  - image: hopla/test:1.0
    name: test-container2
    volumeMounts:
    - mountPath: /data
      name: empty-volume
    - mountPath: /onmemory
      name: memory-volume
  volumes:
  - name: host-volume
    hostPath:
      path: /home/vagrant/data
      type: Directory
  - name: empty-volume
    emptyDir: {}
  - name: memory-volume
    emptyDir:
      medium: Memory
EOF

# Create de resource
student@master:~$ kubectl create -f test-pod.pod.yml
pod/test-pod created
```
Let's check the state of the pod:
```

student@master:~$ kubectl get pods -n ch5
NAME                      READY   STATUS              RESTARTS   AGE
test-pod                  0/2     ContainerCreating   0          26s

```
We can look at the pod info in order to know why is failing:
```
student@master:~$ kubectl describe pod test-pod -n ch5
[OMITED]
Events:
  Type     Reason       Age               From            Message
  ----     ------       ----              ----            -------
  Warning  FailedMount  3s (x6 over 19s)  kubelet, worker1  MountVolume.SetUp failed for volume "host-volume" : hostPath type check failed: /home/vagrant/data is not a directory

```

Pod will remain in ContainerCreating until _/home/vagrant/data_ is present on node _worker1_ as it is not able to find it as there is no directory with this name.

We will just create this directory in worker1 and verify again.

```
vagrant@worker1:$ mkdir /home/vagrant/data


student@master:~$ kubectl get pods -n ch5
NAME                      READY   STATUS    RESTARTS   AGE
test-pod                  2/2     Running   0          40s
```
As the mounted directory use in our pod alredy exist the cluster is able to deploy the pod.

2 - Then we will  create an empty file on node _worker1_ and verify it is inside _test-container1_.
```
vagrant@worker1:$ touch /home/vagrant/data/testfile

student@master:~$ kubectl exec -ti test-pod -n ch5 -c test-container1 -- ls /data
testfile
```
container2 has de 'same' /data path, let's check that the testfile is there.
```

student@master:~$ kubectl exec -ti test-pod -n ch5  -c test-container2 -- ls /data

```
Well testfile is NOT  going to be located there as this volume is configure as EmptyDir and not as a hostPath in /home/vagrant/data in worker1.

---

## Lab2 - Using Kubernetes configmap and secret
### __Using configMap__.

1 - Let's move now to configMap definitions creating one to configure our test application. 

We are going to configure a Configmap with all the data for our app, as if it was a file need fot our pod configuration. The Configmap is going to be use as a volume (remember configmaps can also be used as env variables).

We define a configmap with our configuration data:

#### [app-config.configmap.yml](./app-config.configmap.yml)

```
student@master:~$ cat <<-EOF >app-config.configmap.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
  namespace: ch5
data:
  APP_CONFIGS: |
    APP_PORT=8080
    APP_LISTENIP=0.0.0.0
EOF

student@master:~$ kubectl create -f app-config.configmap.yml
configmap/app-config created

# Check configmap
student@master:~$ kubectl get configmap -n ch5
NAME         DATA   AGE
app-config   1      4m49s

```

You can also visualize the ConfigMap info: 
```
student@master:~$ kubectl describe configmap app-config -n ch5
Name:         app-config
Namespace:    ch5
Labels:       <none>
Annotations:  <none>

Data
====
APP_CONFIGS:
----
APP_PORT=8080
APP_LISTENIP=0.0.0.0

Events:  <none>

```

We will now create a dummy application Pod which uses this ConfigMap to understand how this will work:

#### [app-pod.pod.yml](./app-pod.pod.yml)
```
student@master:~$ cat <<-EOF >app-pod.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: app-pod
  namespace: ch5
spec:
  containers:
  - image: hopla/test:1.0
    name: app-container
    volumeMounts:
    - mountPath: /etc/app
      name: test-app-config
  volumes:
  - name: test-app-config
    configMap:
      name: app-config
      items:
      - key: APP_CONFIGS
        path: app.conf
EOF

student@master:~$ kubectl create -f app-pod.pod.yml
pod/app-pod created
student@master:~$ kubectl get pod -n ch5
NAME       READY   STATUS    RESTARTS   AGE
app-pod    1/1     Running   0          3m29s


```
2 - So this pod mounts APP_CONFIGS, which has APP_PORT and APP_LISTENIP as data, in the path /etc/app as full path /etc/app/app.conf . Let's check it:

```
student@master:~$ kubectl exec -ti app-pod -n ch5 -- sh -c "ls /etc/app/"
app.conf

student@master:~$ kubectl exec -ti app-pod -n ch5 -- cat /etc/app/app.conf
APP_PORT=8080
APP_LISTENIP=0.0.0.0
```


### __Creating Secrets__.

3 - We will now review how secrets will help us to secure our application's credentials.

Create a secret with the commandline which has username and password, neede for our app to properly deploy.
```
student@master:~$ kubectl create -n ch5 secret generic appadmin \
--from-literal=username=admin \
--from-literal=password='supersecretpassword'
```

Check that secret was created and its info:
```
student@master:~$ kubectl get secret -n ch5
NAME                  TYPE                                  DATA   AGE
appadmin              Opaque                                2      2s

student@master:~$kubectl describe secret appadmin -n ch5
Name:         appadmin
Namespace:    ch5
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  19 bytes
username:  5 bytes

```

Then we use it within a pod as a volumes (also could be able to use env variables):

#### [test-pod-admin.pod.yml](./test-pod-admin.pod.yml)

```
student@master:~$ cat<<EOF> test-pod-admin.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod-admin
  namespace: ch5
spec:
  containers:
  - image: hopla/test:1.0
    name: test-container
    volumeMounts:
    - mountPath: /etc/app
      name: test-appadmin
  volumes:
  - name: test-appadmin
    secret:
      secretName: appadmin
EOF

student@master:~$ kubectl create -f test-pod-admin.pod.yml
pod/test-pod-admin created
```
Check our pod was deployed correctly:
```
student@master:~$ kubectl get pod -n ch5
NAME             READY   STATUS    RESTARTS   AGE
test-pod-admin   1/1     Running   0          44s
```

4 - If we now take a look at defined username and password we get the values configured using secret resource.
```
student@master:~$ kubectl exec -ti test-pod-admin -- cat /etc/app/username
admin

student@master:~$ kubectl exec -ti test-pod-admin -- cat /etc/app/password
supersecretpassword
```


---




## Lab3 - Creating and using Secrets, configMaps, Volumes and PersistentVolume/PersistentVolumeClaim.

In this lab we will deploy a simple webserver using different volumes. We will use webserver.deployment.yaml.

We have prepared following volumes:

- **congigMap** - config-volume (with "/etc/nginx/conf.d/default.conf" - configuration file)
- **emptyDir** - empty-volume for Nginx logs "/var/log/nginx".
- **secret** - secret-volume to specify some variables to compose index.html page.
- **persistentVolumeClaim** - data-volume binded to hostPath's persistentVolume using host's "/mnt". 

---

>NOTE: Need ch5 namespace for this lab. 


1 - First we create "webserver-test-config" configMap, this is goint to be use by our nginx pod:

#### [nginx.config.yml](./nginx.config.yml)

```
student@master:~$ cat<<EOF> nginx.config.yml
apiVersion: v1
data:
  default.conf: |+
        server {
            listen       80;
            server_name  test;

            location / {
                root   /wwwroot;
                index  index.html index.htm;
            }

            error_page   500 502 503 504  /50x.html;
            location = /50x.html {
                root   /usr/share/nginx/html;
            }

        }

kind: ConfigMap
metadata:
  creationTimestamp: null
  name: webserver-test-config
  namespace: ch5
EOF

student@master:~$ kubectl create -f nginx.config.yml

```

We can check created "webserver-test-config" and its data:
```
student@master:~$ kubectl get configmap -n ch5
NAME                    DATA   AGE
webserver-test-config   1      9s

$ kubectl describe configmap -n ch5 webserver-test-config 
Name:         webserver-test-config
Namespace:    ch5
Labels:       <none>
Annotations:  kubectl.kubernetes.io/last-applied-configuration:
                {"apiVersion":"v1","data":{"default.conf":"server {\n    listen       80;\n    server_name  test;\n\n    location / {\n        root   /www...

Data
====
default.conf:
----
server {
    listen       80;
    server_name  test;

    location / {
        root   /wwwroot;
        index  index.html index.htm;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

}


Events:  <none>

```


2 - Create secret which is going to be use in order to give values to our index.html which is going to serve our nginx pod.

#### [title.secret.yml](./title.secret.yml)

```
student@master:~$ cat<<EOF> title.secret.yml
apiVersion: v1
data:
  PAGEBODY: SGVsbG9fV29ybGRfZnJvbV9TZWNyZXQ=
  PAGETITLE: SE9QTEEgS1VCRVJORVRFUyBUUkFJTklORyE=
kind: Secret
metadata:
  creationTimestamp: null
  name: webserver-secret
  namespace: ch5

EOF

# Create secret
student@master:~$ kubectl create -f title.secret.yml
```

>NOTE: We can create this Secret also using imperative format with kubectl command-line:
>```
>kubectl create secret generic webserver-secret -n ch5 \
>--from-literal=PAGETITLE="HOPLA KUBERNETES TRAINING!" \
>--from-literal=PAGEBODY="Hello_World"
>```

We created the new "webserver-secret" secret, we can check its creation and data:
```
student@master:~$ kubectl get secret -n ch5
 webserver-secret      Opaque                                2      12s
student@master:~$ kubectl describe secret -n ch5 webserver-secret
Name:         webserver-secret
Namespace:    ch5
Labels:       <none>
Annotations:  
Type:         Opaque

Data
====
PAGEBODY:   23 bytes
PAGETITLE:  26 bytes

```

3 - Create Persistence Volumen and Persistence Volume claim
#### [hostpath.pv.yml](./hostpath.pv.yml)

```
student@master:~$ cat<<EOF> hostpath.pv.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: webserver-pv
  labels:
    type: local
spec:
  storageClassName: manual 
  capacity:
    storage: 500Mi
  accessModes:
    - ReadWriteOnce 
  persistentVolumeReclaimPolicy: Retain
  hostPath:
    path: "/mnt" 

EOF

# create pv
student@master:~$ kubectl create -f hostpath.pv.yml

```
Check persistentVolume and be aware of the state:

>NOTE:  pv are object which are non-namespaced

```
student@master:~$ kubectl get pv
webserver-pv                               500Mi      RWO            Retain           Available                                                   manual                  21s


student@master:~$ kubectl describe pv webserver-pv
Name:            webserver-pv
Labels:          type=local
Annotations:     kubectl.kubernetes.io/last-applied-configuration:
                   {"apiVersion":"v1","kind":"PersistentVolume","metadata":{"annotations":{},"labels":{"type":"local"},"name":"webserver-pv"},"spec":{"access...
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:    manual
Status:          Available
Claim:           
Reclaim Policy:  Retain
Access Modes:    RWO
VolumeMode:      Filesystem
Capacity:        500Mi
Node Affinity:   <none>
Message:         
Source:
    Type:          HostPath (bare host directory volume)
    Path:          /mnt
    HostPathType:  
Events:            <none>
```
The state of the PersistentVolume is Available as there is no PersistentVolumeClaim which has asked for this storage section. Therefore let's create the pvc in order to use this pv.

And now create its PersistentVolumeClaim.
#### [hostpath.pvc.yml](./hostpath.pvc.yml)

```
student@master:~$ cat<<EOF> hostpath.pvc.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: werbserver-pvc
  namespace: ch5
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
  storageClassName: manual
EOF

# create pvc
student@master:~$ kubectl create -f hostpath.pvc.yml
```

Check pvc and persistentVolume state:

>NOTE:  pvc are object which are namespaced not like pv

```
student@master:~$ kubectl get pvc -n ch5
NAME             STATUS   VOLUME         CAPACITY   ACCESS MODES   STORAGECLASS   AGE
werbserver-pvc   Bound    webserver-pv   500Mi      RWO            manual         16s
student@master:~$ kubectl get pv
NAME           CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                STORAGECLASS   REASON   AGE
webserver-pv   500Mi      RWO            Retain           Bound    ch5/werbserver-pvc   manual                  3m57s

```
No our pv and pvc are in Bound state as pvc has requested that storage becaus they pv fulfills pvc storage petitions.

4 - Now we deploy the whole application with in order to use all the resources configured:

- **configMap** - config-volume (with "/etc/nginx/conf.d/default.conf" - configuration file) created on the first step

- **secret** - secret-volume to specify some variables to compose index.html page created on the second step.

- **persistentVolumeClaim** - data-volume binded to hostPath's persistentVolume using host's "/mnt" which was created on the third step

- **emptyDir** - empty-volume for Nginx logs "/var/log/nginx" which is going to be added now
#### [webserver.deploy.yml](./webserver.deploy.yml)

```
student@master:~$ cat<<EOF> webserver.deploy.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: webserver
  name: webserver
  namespace: ch5
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webserver
  strategy: {}
  template:
    metadata:
      labels:
        app: webserver
    spec:
      nodeName: worker1
      containers:
      - image: codegazers/nginx-lab:1.2
        env:
        - name: HTMLDIR
          value: /wwwroot
        - name: PAGETITLE
          valueFrom:
            secretKeyRef:
              name: webserver-secret
              key: PAGETITLE
        - name: PAGEBODY
          valueFrom:
            secretKeyRef:
              name: webserver-secret
              key: PAGEBODY
        name: nginx
        resources: {}
        volumeMounts:
        - name: config-volume
          mountPath: /etc/nginx/conf.d/
        - mountPath: /var/log/nginx
          name: empty-volume
          readOnly: false
        - mountPath: /wwwroot
          name: data-volume          
      volumes:
      - name: config-volume
        configMap:
          name: webserver-test-config
      - name: empty-volume
        emptyDir: {}
      - name: data-volume
        persistentVolumeClaim:
          claimName: werbserver-pvc
EOF

# Create deployment
student@master:~$ kubectl create -f webserver.deploy.yml

```

```
student@master:~$ kubectl get all -n ch5 -o wide
NAME                             READY   STATUS    RESTARTS   AGE   IP       NODE               NOMINATED NODE   READINESS GATES
pod/webserver-64c474cbcb-5mrxb   1/1     Running   0          66s   10.244.104.11   node2   <none>           <none>


```

5 - Create "webserver" Service in order to have access to our deploy html web and see the values of our application: 
#### [webserver.service.yml](./webserver.service.yml)

```
student@master:~$ cat<<EOF> webserver.service.yml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: webserver
  name: webserver-svc
  namespace: ch5
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
    nodePort: 32080
  selector:
    app: webserver
  type: NodePort
EOF

# Create service
student@master:~$ kubectl create -f webserver.service.yml
```



6 - Now we test the web server.

```
student@master:~$ curl IP-MASTER:32080
<!DOCTYPE html>
<html>
<head>
<title>DEFAULT_TITLE</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>DEFAULT_BODY</h1>
</body>
</html>
```

First Pod execution will create default "/wwwroot/index.html" inside it. This is mounted inside "worker1" node's filesystem inside "/mount" directory. Therefore after this first execution, we can find that "/mnt/index.html" was created. This file was published and we get it when we executed curl 0.0.0.0:32080. File would change until index exits so firs shown data is not goin to have the wished data

Our application is quite simple but it is prepared to modify the content of the "index.html" file. As mentioned before, default title and body will be changed with the values defined in the Secret resource. This will happen on container creation if "index.html" file already exist. 


7 - We delete "webserver" Deployment's Pod. 
```
student@master:~$ kubectl delete pod/webserver-64c474cbcb-5mrxb  -n ch5
```

A new Pod is deployed to keep Deployment state and now our data is populated

8 - We access again and we notice that page was changed.
```
student@master:~$ curl IP-MASTER:32080
<!DOCTYPE html>
<html>
<head>
<title>HOPLA KUBERNETES TRAINING!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Hello_World_from_Secret</h1>
</body>
</html>

```

In this lab we have used different storage resources to manage different kind of data and how this data influences our deployment.

---
