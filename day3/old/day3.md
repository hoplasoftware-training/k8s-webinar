# In this Lab:
- We will learn how to manage data using different Kubernetes features such us __volumes__, __configurations__, __secrets__ and __persitent volumes__.

- We will also talked about networking on this third webinar, hence we will have a lab showing many of the Kubernetes described during the session.

---



# Data Management Labs Section
This section will guide usto basic volumes, configMaps and secrets.


---
## volumes
1 - First we will create a pod with two containers and 3 different volumes:

- test-container1 --> host-volume on /data

- test-container2 --> emtpy-volume on /data and tmpfs volume on /onmemory

> NOTE:
> We have added an specific nodeName key to execute this pod on this node.


### [test-pod.pod.yml](./test-pod.pod.yml)

```
$ cat<<EOF> test-pod.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod
spec:
  nodeName: knode3
  containers:
  - image: hopla/test:1.0
    name: test-container1
    volumeMounts:
    - mountPath: /data
      name: host-volume
  - image: hopla/test:1.0
    name: test-container2
    volumeMounts:
    - mountPath: /data
      name: empty-volume
    - mountPath: /onmemory
      name: memory-volume
  volumes:
  - name: host-volume
    hostPath:
      path: /home/vagrant/data
      type: Directory
  - name: empty-volume
    emptyDir: {}
  - name: memory-volume
    emptyDir:
      medium: Memory
EOF

$ kubectl create -f test-pod.pod.yml
pod/test-pod created


$ kubectl get pods
NAME                      READY   STATUS              RESTARTS   AGE
test-pod                  0/2     ContainerCreating   0          26s

```

Pod will remain in ContainerCreating until _/home/vagrant/data_ is present on node _knode3_.

We will just create this directory and verify again

```
(vagrant@knode3)$ mkdir /home/vagrant/data


$ kubectl get pods
NAME                      READY   STATUS    RESTARTS   AGE
test-pod                  2/2     Running   0          40s


```


2 - Then we will  create an empty file on node _knode3_ and verify it is inside _test-container1_.
```
(vagrant@knode3)$ touch /home/vagrant/data/testfile

$ kubectl exec -ti test-pod -c test-container1 -- ls /data
testfile

$ kubectl exec -ti test-pod -c test-container2 -- ls /data

```
---

## configMap
1 - Let's move now to configMap definitions creating one to configure our test application:

### [app-config.configmap.yml](./app-config.configmap.yml)

```
$ cat <<-EOF >app-config.configmap.yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
data:
  APP_CONFIGS: |
    APP_PORT=8080
    APP_LISTENIP=0.0.0.0

EOF

$ kubectl create -f app-config.configmap.yml
configmap/app-config created
```

We will now create a dummy application Pod to understand how this will work:

```
$ cat <<-EOF >app-pod.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: app-pod
spec:
  containers:
  - image: hopla/test:1.0
    name: app-container
    volumeMounts:
    - mountPath: /etc/app
      name: test-app-config
  volumes:
  - name: test-app-config
    configMap:
      name: app-config
      items:
      - key: APP_CONFIGS
        path: app.conf
EOF

$ kubectl create -f app-pod.pod.yml
pod/app-pod created

```
Notice the paths and keys used to configure in this case application's port and listenip.


```
$ kubectl exec -ti app-pod -- sh -c "ls /etc/app/"
app.conf

$ kubectl exec -ti app-pod -- cat /etc/app/app.conf
APP_PORT=8080
APP_LISTENIP=0.0.0.0

```

---
## secrets
1 - We will now review how secrets will help us to secure our application's credentials.
```
$ kubectl create secret generic appadmin \
--from-literal=username=admin \
--from-literal=password='supersecretpassword'
```

Then we use it within a pod:
### [test-po-admin.pod.yml](./test-po-admin.pod.yml)
```
$ cat<<EOF> test-po-admin.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod-admin
spec:
  containers:
  - image: hopla/test:1.0
    name: test-container
    volumeMounts:
    - mountPath: /etc/app
      name: test-appadmin
  volumes:
  - name: test-appadmin
    secret:
      secretName: appadmin
EOF

$ kubectl create -f test-po-admin.pod.yml
pod/test-pod-admin created

```

2 - If we now take a look at defined username and password we get the values configured using secret resource.
```
$ kubectl exec -ti test-pod-admin -- cat /etc/app/username
admin

$ kubectl exec -ti test-pod-admin -- cat /etc/app/password
supersecretpassword
```
If we inspect this values they are not visible.

---

# Network Labs Section
This section will show us various Kubernetes features with easy labs.

---
## Network Resolution

1 - We will start reviewing default dns resolution checking _www.google.com_ FQDN.
```
$ kubectl run --generator=run-pod/v1 nettools --rm --image=frjaraur/nettools:minimal --restart=Never --attach --quiet -- host www.google.com
```

2 - We deploy a simple webserver using small Alpine Nginx image.
```
$ kubectl create deployment --image=nginx:alpine webserver
```

3 - Now we will expose port 80 to be reachable.
```
$ kubectl expose deployment webserver --port 80 --target-port 80
```
This service is only exposed internally within Kubernetes cluster.


4 - We will now test resolution of defined service, created using __kubectl expose__ command.
```
$ kubectl run --generator=run-pod/v1 nettools --rm --image=frjaraur/nettools:minimal --restart=Never --attach --quiet -- host webserver
```

---


## Ingress Controllers

1 - We will first deploy Kubernetes' Nginx Controller:

```
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml


$ kubectl get deployments -A
NAMESPACE       NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
default         blue                       3/3     3            3           13m
default         red                        2/2     2            2           13m
ingress-nginx   nginx-ingress-controller   1/1     1            1           58m
kube-system     calico-kube-controllers    1/1     1            1           77m
kube-system     coredns                    2/2     2            2           77m
```

2 - Now we publish Ingress Controller's service as NodePort.
```
$ kubectl expose deployment nginx-ingress-controller -n ingress-nginx --type=NodePort --port=80 --target-port=80
service/nginx-ingress-controller exposed

$ kubectl get svc -n ingress-nginx
NAME                       TYPE       CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
nginx-ingress-controller   NodePort   10.99.77.35   <none>        80:30043/TCP   16m
```

3 - Let's deploy two simple applications showing colors.
 - red - 2 instances
 - blue - 3 instances

---
__Schema:__
![Ingress Controller](https://gitlab.com/hoplasoftware-training/k8s-webinar/-/raw/master/images/IC_lab.jpg)

---
### Red Deployment

### [red.deployment.yml](./red.deployment.yml)

```
cat <<-EOF >red.deployment.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: red
    webinar: day3
  name: red
spec:
  replicas: 2
  selector:
    matchLabels:
      app: red
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: red
        webinar: day3
    spec:
      containers:
      - image: codegazers/colors:1.16
        name: red-container
        imagePullPolicy: Always
        resources: {}
        env:
        - name: COLOR
          value: red
status: {}
EOF
```

### Blue Deployment

### [blue.deployment.yml](./blue.deployment.yml)

```
cat <<-EOF >blue.deployment.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: blue
    webinar: day3
  name: blue
spec:
  replicas: 3
  selector:
    matchLabels:
      app: blue
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: blue
        webinar: day3
    spec:
      containers:
      - image: codegazers/colors:1.16
        name: blue-container
        imagePullPolicy: Always
        resources: {}
        env:
        - name: COLOR
          value: blue
status: {}
EOF

```

We will just deploy these __Deployment__ definitions and __expose internally__ their ports.
```
$ kubectl create -f red.deployment.yml
deployment.apps/red created

$ kubectl expose deployment red --name=red-svc --labels webinar=lab3 --port=3000 --target-port=3000
service/red exposed


$ kubectl create -f blue.deployment.yml
deployment.apps/blue created

$ kubectl expose deployment blue --name=blue-svc --labels webinar=lab3 --port=3000 --target-port=3000
service/blue exposed


```

4 - We list all applications' components:

```
$ kubectl get all -l webinar --show-labels
NAME                        READY   STATUS    RESTARTS   AGE     LABELS
pod/blue-54bc8d95b9-ddzrq   1/1     Running   0          3m51s   app=blue,pod-template-hash=54bc8d95b9,webinar=day3
pod/blue-54bc8d95b9-p5vv7   1/1     Running   0          3m54s   app=blue,pod-template-hash=54bc8d95b9,webinar=day3
pod/blue-54bc8d95b9-z6s2t   1/1     Running   0          3m57s   app=blue,pod-template-hash=54bc8d95b9,webinar=day3
pod/red-74f5dbcc7d-k4wk8    1/1     Running   0          3m59s   app=red,pod-template-hash=74f5dbcc7d,webinar=day3
pod/red-74f5dbcc7d-w7sjv    1/1     Running   0          4m3s    app=red,pod-template-hash=74f5dbcc7d,webinar=day3

NAME           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE   LABELS
service/blue   ClusterIP   10.102.231.100   <none>        3000/TCP   54s   webinar=lab3
service/red    ClusterIP   10.111.145.245   <none>        3000/TCP   66s   webinar=lab3

NAME                   READY   UP-TO-DATE   AVAILABLE   AGE     LABELS
deployment.apps/blue   3/3     3            3           5m14s   app=blue,webinar=day3
deployment.apps/red    2/2     2            2           5m8s    app=red,webinar=day3

NAME                              DESIRED   CURRENT   READY   AGE     LABELS
replicaset.apps/blue-54bc8d95b9   3         3         3       3m57s   app=blue,pod-template-hash=54bc8d95b9,webinar=day3
replicaset.apps/red-74f5dbcc7d    2         2         2       4m3s    app=red,pod-template-hash=74f5dbcc7d,webinar=day3
```
5 - We are now ready for __internal testing__.
we can use Ingress Controller's internal service's IP address and some of our applications' host headers.

```
$ curl -H "host: blue.example.com" -L 10.99.77.35
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.17.8</center>
</body>
</html>
```
We get an error because we haven't defined any __Ingress__ resource yet.

6 - Let's deploy our applications' ingress definitions:

### [colors.ingress.yml](./colors.ingress.yml)
```
$ cat <<-EOF >colors.ingress.yml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: colors-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      - path: /red/(.*)
        backend:
          serviceName: red-svc
          servicePort: 3000
      - backend:
          serviceName: blue-svc
          servicePort: 3000


  - host: red.enterprise.es
    http:
      paths:
      - backend:
          serviceName: red-svc
          servicePort: 3000

EOF



$ kubectl create -f colors.ingress.yml
ingress.extensions/colors-ingress created
```
We review now defined resources:

```
$ kubectl get ingress
NAME             HOSTS                                ADDRESS   PORTS   AGE
colors-ingress   blue.example.com,red.enterprise.es             80      45s

```
7 - We now can test our definitions.

We defined __"ed.enterprise.es"__ host header for application __red__. Therefore we can use Ingress Controller's internal service's IP address and red-defined host header.
```
$ curl -H "host: red.enterprise.es" 10.99.77.35/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-w7sjv
CONTAINER_IP:  10.244.69.206
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux
```

We can apply same concepts with  __"blue.example.com"__ host header for application __blue__. Therefore we can also use Ingress Controller's internal service's IP address and blue-defined host header.

```

$ curl -H "host: blue.example.com" -L 10.99.77.35/text
<html>
<head>
    <title>blue</title>
    <meta charset="utf-8" />
    <style>
        body {
            background-color: blue;
        }
        .center {
            padding: 70px 0;
        }
        h2 {
            color: grey;
            text-align: left;
            font-family: "Sans-serif", Arial;
            font-style: oblique;
            font-size: 14px;
        }
        table {
            border-collapse: collapse;
            margin-left:auto;
            margin-right:auto;
            background-color: #F0F8FF;
        }

        table, th, td {
            border: 1px solid black;

        }
        tr:hover {background-color: #DCDCDC}

        p.padding {
            padding-top: 2cm;
        }
    </style>
</head>
<body>
<div class="center">
    <table style="float:center" >
        <tr><td>Container IP:</td><td> 10.244.69.208</td></tr>
        <tr><td>Client IP:</td><td>undefined</td></tr>
        <tr><td>Container Name:</td><td>blue-54bc8d95b9-ddzrq</td></tr>
        <tr><td>Color:</td><td>blue</td></tr>
        <tr><td>Application Version:</td><td>1.20</td></tr>
    </table>
</div>
</body>

</html>
</body>
</html>
```


8 - But we also defined __/red__ path to be forwarded to red service. Therefore, we can reach __red__ service using __"blue.example.com"__.
```
$ curl -H "host: blue.example.com" -L 10.99.77.35/red/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-k4wk8
CONTAINER_IP:  10.244.176.205
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux
```

>NOTE:
> We need to use URL rewriting but this will be applied to all the hosts defined within the Ingress resource. To avoid this situation, we have just split them in two Ingress resource files. This is described in 9th step.



9 - We will create two different Ingress resources to avoid URL rewriting errors:

### [colors-simple.ingress.yml](./colors-simple.ingress.yml)

```
$ cat <<-EOF >colors-simple.ingress.yml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: colors-ingress
  annotations:
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      - backend:
          serviceName: blue-svc
          servicePort: 3000


  - host: red.enterprise.es
    http:
      paths:
      - backend:
          serviceName: red-svc
          servicePort: 3000

EOF


$ kubectl replace -f colors-simple.ingress.yml
ingress.extensions/colors-ingress replaced
```

### [colors-rewrite.ingress.yml](./colors-rewrite.ingress.yml)
```
$ cat <<-EOF >colors-rewrite.ingress.yml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: colors-ingress-rewrite
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      #- path: /red(/|$)(.*)
      - path: /red/(.*)
        backend:
          serviceName: red-svc
          servicePort: 3000
EOF


$ kubectl create -f ingress-rewrite.ingress.yml
ingress.extensions/colors-ingress-rewrite created


```
10 - Now we can test again __blue__ and __red__ services acceses again, even using __"blue.example.com"__ to reach the __red__ one.
```
$ curl -H "host: blue.example.com" -L 10.99.77.35/text
APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: blue-54bc8d95b9-z6s2t
CONTAINER_IP:  10.244.176.206
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux


$ curl -H "host: blue.example.com" -L 10.99.77.35/red/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-w7sjv
CONTAINER_IP:  10.244.69.206
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux


$ curl -H "host: red.enterprise.es" 10.99.77.35/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-k4wk8
CONTAINER_IP:  10.244.176.205
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux
```