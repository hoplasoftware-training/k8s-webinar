





---------------------------------------


# Context
## See actual context and resouces
```
kubectl get config
```
## Curl another cluster
```
 curl https://192.168.2.220:6443/api/v1/pods --key admin.key --cert admin.crt --cacert ca.crt

```
## Configure new context

```
kubectl config  set-cluster office --server=https://192.168.2.220:6443 --certificate-authority=/home/vagrant/certs-office/ca.crt

kubectl config  set-credentials user-office --client-certificate=/home/vagrant/certs-office/admin.crt --client-key=/home/vagrant/certs-office/admin.key

kubectl config set-context kube-office --cluster=office --namespace=kube-system --user=user-office

kubectl config set-context all-office --cluster=office --user=user-office
 
``` 
## Using new context
```
kubectl get nodes

kubectl config  use-context kube-office
kubectl get pods --all-namespaces
kubectl get pods 
kubectl get pods -n default

kubectl config  use-context all-office
kubectl get pods --all-namespaces
```
```
kubectl config set-context dev-local --cluster=kubernetes --user=kubernetes-admin --namespace=dev
 kubectl config  use-context dev-local
```
## Delete information in kubeconfig
```
kubectl config delete-context default-office 
kubectl config  delete-cluster office
kubectl config  unset users.user-office
```



# RBAC

# Create context for namespace dev in local
```
sudo kubectl config set-context dev-local --cluster=kubernetes --user=kubernetes-admin --namespace=dev
sudo  kubectl config  use-context dev-local
```

# Create Deployment
```
apiVersion: v1
kind: Namespace
metadata:
  name: dev
---
apiVersion: apps/v1
kind: Deployment
metadata:
 name: myapp
 labels:
   app: myapp
   type: web
 namespace: dev
spec:
  replicas: 3
  selector:
    matchLabels:
      type: web
  template:
    metadata:
      name: colors-pod
      labels:
        type: web
    spec:
      containers:
      - name: colors-pod
        image: codegazers/colors:1.5
        ports:
        - name: web
          containerPort: 3000

```

```
kubectl create deployment --image nginx my-nginx -n default
```

## Role view and edit, for namespace dev
```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-view
  namespace: dev
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  creationTimestamp: null
  name: role-view-b
  namespace: dev
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: role-view
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: alba


```

```
kubectl create -f <yaml>

kubectl get pods --as=alba
kubectl get pods -n default --as=alba
kubectl get all --as=alba
kubectl set image deployment.apps/myapp colors-pod=codegazers/colors:1.5 --as alba --record
```


```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-developer
  namespace: dev
rules:
- apiGroups: ["", "apps", "extensions"]
  resources: ["pods", "deployments"]
  verbs:
  - get
  - list
  - watch
  - create
  - delete
  - update
  - edit
  - patch
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - get
  - list
  - watch

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  creationTimestamp: null
  name: role-developer-b
  namespace: dev
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: role-developer
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: jaime
  
```


```
kubectl get all --as=jaime
kubectl get pods -n default --as=jaime
kubectl set image deployment.apps/myapp colors-pod=codegazers/colors:1.10 --as jaime --record
```


## Cluster role edit
```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  creationTimestamp: null
  name: role-creator
  #namespace: dev
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
  - create
  - delete
- apiGroups:
  - ""
  resources:
  - secrest
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  creationTimestamp: null
  name: role-creator-b
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: role-creator
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: javi


```


```
kubectl get all --all-namespaces --as=javi
```

# Security Context
## Create pod with no security context
```
apiVersion: v1
kind: Pod
metadata:
  name: busybox0
  labels:
    app: busybox0
spec:
  containers:
  - image: busybox
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox

```

```
kubectl exec -ti busybox0 -- ls -l
kubectl exec -ti busybox0 -- chown nobody /var

```
## Add security context
```
apiVersion: v1
kind: Pod
metadata:
  name: busybox1
  labels:
    app: busybox1
spec:
  containers:
  - image: busybox
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox
    securityContext:
      capabilities:
        drop:
          - ALL
```

```
kubectl exec -ti busybox1 -- ls -l
kubectl exec -ti busybox1 -- chown nobody /var

```

```
apiVersion: v1
kind: Pod
metadata:
  name: busybox2
  labels:
    app: busybox2
spec:
  containers:
  - image: busybox
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox
    securityContext:
      capabilities:
        drop:
          - ALL
        add:
          - CHOWN
```

```
kubectl exec -ti busybox2 -- ls -l
kubectl exec -ti busybox2 -- chown nobody /var

```

# Image Security

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-private
  labels:
    app: nginx-private
spec:
  containers:
  - image: hoplasoftware/private-nginx:1.0
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: nginx
```


```
kubectl create secret docker-registry regcred --docker-server=docker.io/Account/private-nginx --docker-username=<nombre-usuario> --docker-password=holacaracola --docker-email=holacaracola@hoplasoftware.com
```

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-private
  labels:
    app: nginx-private
spec:
  containers:
  - image: hoplasoftware/private-nginx:1.0
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: nginx
  imagePullSecrets:
  - name: regcred
```

































































