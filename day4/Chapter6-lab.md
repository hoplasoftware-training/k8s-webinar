# Chapter 6
In these labs we will cover all the topics learned on Chapter6.

- We will review Kubernetes network model concepts.

- We will learn how to use Kubernetes internal DNS.

- We will have some labs that will help us to understand how Network Policies work.

- We will use Ingress Controllers to deploy applications in some labs.

---

## Lab1 - Review Kubernetes Network Model

In this lab we will learn how Kubernetes Network Model is implemented in a cluster.

>NOTE: We need a running Kubernetes cluster. We don't care about which CNI is already running because Kubernetes Network Model should work on any CNI implmentation.

1 -  Connect to the cluster and review provided CNI.


...
...

We will create a namespace to deploy chapter 6 labs.
```
student@master:~$ kubectl create namespace ch6
namespace/ch6 created
```

### __Containers within a Pod.__
2 - Review the first Kubernetes networking premise: ___Containers witihn a Pod share the same IP___.
In this step we will create a Pod with two containers. We will deploy a simple Pod with two busybox components.
Let's create a simple Pod definition resource file, [simple-multi-container.pod.yaml](./simple-multi-container.pod.yaml), with the following content:
```
student@master:~$ cat <<EOF>simple-multi-container.pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: multi-container
  namespace: ch6
spec:
  containers:
  - name: container1
    image: busybox
    command: ["/bin/sh"]
    args: ["-c","sleep","300"]
  - name: container2
    image: busybox
    command: ["/bin/sh"]
    args: ["-c","sleep","300"]
EOF
```

3 - Then we deploy this resource.
```
student@student-master:~$ kubectl create -f simple-multi-container.pod.yaml 
pod/multi-container created
```

4 - Let's review each container IP address. 
```
student@student-master:~$ kubectl exec -n ch6 multi-container -c container1 -- ip address show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: tunl0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1
    link/ipip 0.0.0.0 brd 0.0.0.0
4: eth0@if7: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1440 qdisc noqueue 
    link/ether b6:a4:54:94:03:e6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.193/32 scope global eth0
       valid_lft forever preferred_lft forever

student@student-master:~$ kubectl exec -n ch6 multi-container -c container2 -- ip address show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: tunl0@NONE: <NOARP> mtu 1480 qdisc noop qlen 1
    link/ipip 0.0.0.0 brd 0.0.0.0
4: eth0@if7: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1440 qdisc noqueue 
    link/ether b6:a4:54:94:03:e6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.193/32 scope global eth0
       valid_lft forever preferred_lft forever
```

As expected both containers, running inside "multi-container" Pod, have the same IP because they share the same network namespace.
```
student@student-master:~$ kubectl delete -n ch6 pod/multi-container
pod "multi-container" deleted
```

>### QUESTION: Why do we can't execute two Nginx web servers inside the same Pod?.


### __Pod to Pod Communications.__
3 - Review the first Kubernetes networking premise: ___Pods on a node can communicate with all pods on local node without NAT___.

```
student@student-master:~$ kubectl run -n ch6 --generator=run-pod/v1 --image=busybox p1 --restart=Always -- sleep 300
Flag --generator has been deprecated, has no effect and will be removed in the future.
pod/p1 created
```

Let's review pod's IP address.
```
student@master:~$ kubectl exec -n ch6 -ti p1 -- ip address show eth0
4: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1440 qdisc noqueue 
    link/ether a2:73:62:a5:81:b7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.162.2/32 scope global eth0
       valid_lft forever preferred_lft forever
```

And now we create a new pod that just pings the first one. Open another 
session to your master node and execute __watch kubectl get -n ch6 pods -o wide__
```
Every 2.0s: kubectl get -n ch6 pods -o wide        local-master: Wed May 27 13:48:45 2020

NAME   READY   STATUS    RESTARTS   AGE     IP              NODE            NOMINATED NODE   READINESS GATES
p1     1/1     Running   0          3m14s   192.168.162.2   local-worker2   <none>           <none>
```

Now create a new pod and review results in the other session.
```
student@master:~$ kubectl run -n ch6 p2 --image=busybox  --restart=Never --attach=true -i -- ping -c 50  192.168.162.8
If you don't see a command prompt, try pressing enter.
64 bytes from 192.168.162.2: seq=1 ttl=62 time=0.410 ms
64 bytes from 192.168.162.2: seq=2 ttl=62 time=0.489 ms
64 bytes from 192.168.162.2: seq=3 ttl=62 time=0.450 ms
64 bytes from 192.168.162.2: seq=4 ttl=62 time=0.460 ms
64 bytes from 192.168.162.2: seq=5 ttl=62 time=0.428 ms
```

Ping works from one pod to another but we notice that they are running on different hosts.

```
Every 2.0s: kubectl get -n ch6 pods -o wide                                                   local-master: Wed May 27 13:51:09 2020

NAME   READY   STATUS    RESTARTS   AGE     IP              NODE            NOMINATED NODE   READINESS GATES
p1     1/1     Running   1          5m39s   192.168.162.2   local-worker2   <none>           <none>
p2     1/1     Running   0          6s      192.168.0.198   local-worker1   <none>           <none>
```

We can terminate p2 Pod output issuing Ctrl+C.


This is not what we wanted to test because we have gone too fast. Pods are running on different hosts because scheduler knows resources available on each node and tries to maintain load balancing between nodes.

We have probed the statement that ___Pods on a node can communicate with all pods on all nodes without NAT___.


4 - Let's execute some pods on the same host.
```
student@master:~$ kubectl delete -n ch6 pod/p1 pod/p2
pod "p1" deleted
pod "p2" deleted
```


Let's ensure both pods run in the same host adding node affintiy.
```
student@master:~$ kubectl run -n ch6 --generator=run-pod/v1 \
 --image=busybox p1 --dry-run \
 -o yaml -- sleep 300 >p1.pod.yaml
```

Edit "p1.pod.yaml" file with your favorite editor.
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: p1
  name: p1
  namespace: ch6
spec:
  nodeName: student-worker1 ### Added to ensure execution node.
  containers:
  - args:
    - sleep
    - "300"
    image: busybox
    name: p1
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}

```

>NOTE: Don't worry about p1 Pod. It will restart every 300s 

```
student@master:~$ kubectl create -f p1.pod.yaml
pod/p1 created

student@master:~$ kubectl get pods -n ch6 -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP              NODE            NOMINATED NODE   READINESS GATES
p1     1/1     Running   0          10s   192.168.0.195   local-worker1   <none>           <none>
```

>NOTE: We can get all pods' IP addreses using jsonpath:
>```
>kubectl get pods -n ch6 -o jsonpath='{.items[*].status.podIP}'
>```

We will create p2 resource using p1 Pod's IP address:
```
student@master:~$ kubectl run -n ch6 p2 --image=busybox --dry-run -o yaml --restart=Never -- ping -c 50  $(kubectl get pod p1 -o jsonpath='{.status.podIP}')>p2.pod.yaml
```

And then we will modify p2.pod.yaml to deploy this workload on the same node:
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: p2
  name: p2
  namespace: ch6
spec:
  nodeName: student-worker1
  containers:
  - args:
    - ping
    - -c
    - "50"
    - 192.168.0.195
    image: busybox
    name: p2
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}
```

```
student@master:~$ kubectl create -f p2.pod.yaml 
pod/p2 created


student@master:~$ kubectl get pods -n ch6 -o wide 
NAME   READY   STATUS    RESTARTS   AGE   IP              NODE            NOMINATED NODE   READINESS GATES
p1     1/1     Running   6          39m   192.168.0.195   local-worker1   <none>           <none>
p2     1/1     Running   0          20s   192.168.0.196   local-worker1   <none>           <none>
student@master:~$ kubectl logs -n ch6 p2
PING 192.168.0.195 (192.168.0.195): 56 data bytes
64 bytes from 192.168.0.195: seq=0 ttl=63 time=0.106 ms
64 bytes from 192.168.0.195: seq=1 ttl=63 time=0.106 ms
64 bytes from 192.168.0.195: seq=2 ttl=63 time=0.107 ms
64 bytes from 192.168.0.195: seq=3 ttl=63 time=0.096 ms
64 bytes from 192.168.0.195: seq=4 ttl=63 time=0.097 ms
64 bytes from 192.168.0.195: seq=5 ttl=63 time=0.098 ms
64 bytes from 192.168.0.195: seq=6 ttl=63 time=0.081 ms
64 bytes from 192.168.0.195: seq=7 ttl=63 time=0.096 ms
64 bytes from 192.168.0.195: seq=8 ttl=63 time=0.090 ms
64 bytes from 192.168.0.195: seq=9 ttl=63 time=0.130 ms
64 bytes from 192.168.0.195: seq=10 ttl=63 time=0.098 ms
64 bytes from 192.168.0.195: seq=11 ttl=63 time=0.113 ms
64 bytes from 192.168.0.195: seq=12 ttl=63 time=0.105 ms
64 bytes from 192.168.0.195: seq=13 ttl=63 time=0.103 ms
64 bytes from 192.168.0.195: seq=14 ttl=63 time=0.107 ms
64 bytes from 192.168.0.195: seq=15 ttl=63 time=0.091 ms
64 bytes from 192.168.0.195: seq=16 ttl=63 time=0.094 ms
64 bytes from 192.168.0.195: seq=17 ttl=63 time=0.094 ms
64 bytes from 192.168.0.195: seq=18 ttl=63 time=0.089 ms
64 bytes from 192.168.0.195: seq=19 ttl=63 time=0.092 ms
64 bytes from 192.168.0.195: seq=20 ttl=63 time=0.109 ms
64 bytes from 192.168.0.195: seq=21 ttl=63 time=0.105 ms
64 bytes from 192.168.0.195: seq=22 ttl=63 time=0.108 ms
64 bytes from 192.168.0.195: seq=23 ttl=63 time=0.114 ms
64 bytes from 192.168.0.195: seq=24 ttl=63 time=0.093 ms
64 bytes from 192.168.0.195: seq=25 ttl=63 time=0.100 ms
```

This process probes local node's Pod's communications.

Remove p1 and p2 Pods.
```
student@master:~$ kubectl delete -n ch6 pod/p1 pod/p2
pod "p1" deleted
pod "p2" deleted
```

Let's go a little bit further.

### __Hosts' processes to Pod/Services Communications.__

5 - Let's review communications between host and pods/services.
In this lab we will deploy a couple of web servers to test host-to-Pod and host-to-Service communications.
```
student@master:~$ kubectl create deploy -n ch6 webserver1 --image httpd:alpine 
deployment.apps/webserver1 created

student@master:~$ kubectl create deploy -n ch6 webserver2 --image nginx:alpine 
deployment.apps/webserver2 created
```

We can now review deployments states:
```
student@master:~$ kubectl get deploy -n ch6 
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
webserver1   1/1     1            1           35s
webserver2   1/1     1            1           117s

student@master:~$  kubectl get pods -n ch6 -o wide
NAME                          READY   STATUS    RESTARTS   AGE   IP               NODE            NOMINATED NODE   READINESS GATES
webserver1-b6b6d6dcb-dpdvt    1/1     Running   0          40s   192.168.89.193   local-worker2   <none>           <none>
webserver2-5896b78565-5xcs2   1/1     Running   0          33s   192.168.64.193   local-worker1   <none>           <none>

```

We can test access to web servers Pods from master node.
```
student@master:~$ curl 192.168.89.193
<html><body><h1>It works!</h1></body></html>  
student@master:~$ curl 192.168.64.193
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```

This means that we can access from any host to Pods' processes. Let's now create a couple of services.

We will use ___kubectl expose___  to create a Service associated to webserver1 Deployment.
```
student@master:~$ kubectl expose -n ch6 deployment webserver1 --port 1080 --target-port 80
service/webserver1 exposed
```

Let's review created Service:
```
student@master:~$ kubectl get svc -n ch6 
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP    3h33m
webserver1   ClusterIP   10.110.181.253   <none>        1080/TCP   2m22s
```

For webserver2 we will use declarative format. First we review webserver2 Deployment's Pods labels:
```
student@master:~$ kubectl get deploy -n ch6 webserver2 --show-labels
NAME         READY   UP-TO-DATE   AVAILABLE   AGE   LABELS
webserver2   1/1     1            1           16m   app=webserver2

student@master:~$ kubectl get pods -n ch6 -l app=webserver2 --show-labels
NAME                          READY   STATUS    RESTARTS   AGE   LABELS
webserver2-5896b78565-5xcs2   1/1     Running   0          17m   app=webserver2,pod-template-hash=5896b78565
```

Now, we can create a service preparing a Service resource definition.
```
student@master:~$ kubectl create service clusterip webserver2 --dry-run --tcp=1080 -o yaml -n ch6 >webserver2.svc.yaml
```

Let's edit "webserver2.svc.yaml" file with our favorite editor and change Service's port, and verify Service's selector:
```
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: webserver2
  name: webserver2
  namespace: ch6
spec:
  ports:
  - name: "1080"
    port: 1080
    protocol: TCP
    targetPort: 80 # Our Nginx Pod listen on port 80, hence we changed it
  selector:
    app: webserver2 # We verify that app label is searching for "webserver2" value. It is fine.
  type: ClusterIP
status:
  loadBalancer: {}
```

Then we create this new service:
```
student@master:~$ kubectl create -f webserver2.svc.yaml
service/webserver2 created
```

Once created, we can verify both services:
```
student@master:~$ kubectl get svc -n ch6
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
webserver1   ClusterIP   10.107.186.22   <none>        1080/TCP   27m
webserver2   ClusterIP   10.102.13.56    <none>        1080/TCP   5s
```

Let's verify access to web servers from any node:
```
student@master:~$ curl 10.107.186.22:1080
<html><body><h1>It works!</h1></body></html>

student@master:~$ curl 10.102.13.56:1080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Let's verify access from a new Pod.
```
student@master:~$ kubectl run -n ch6 \
--rm test --image=frjaraur/nettools:minimal --restart=Never \
--attach=true -ti -- sh

If you don't see a command prompt, try pressing enter.
/ # curl 10.107.186.22:1080
<html><body><h1>It works!</h1></body></html>
/ # 
/ # 
/ # curl 10.102.13.56:1080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
/ # 
/ # exit
pod "test" deleted
```

6 - Let's delete "webserver1" and "webserver2" resources.
```
student@master:~$ kubectl get svc -n ch6
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
webserver1   ClusterIP   10.107.186.22   <none>        1080/TCP   22h
webserver2   ClusterIP   10.102.13.56    <none>        1080/TCP   22h

student@master:~$ kubectl get deploy -n ch6
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
webserver1   3/3     3            3           22h
webserver2   1/1     1            1           22h
```

Then
```
student@master:~$ kubectl delete deploy -n ch6 webserver1 webserver2
deployment.apps "webserver1" deleted
deployment.apps "webserver2" deleted

student@master:~$ kubectl delete svc -n ch6 webserver1 webserver2
service "webserver1" deleted
service "webserver2" deleted
```

Therefore, hosts and other Pods deployed within this cluster can access our Services' ports. ___Agents on a node can reach all Pods in that node and other nodes___.

Finally we will try host's network namespace.

### __Pods sharing host's networkk namespace can access Pods and Services from other hosts.__

7 - Create a Pod within host's network namespace.
In this case we will create a simple Pod using alpine image. We will use declarative format. First we will create a template using ___kubectl run --dry-run -o yaml___:
```
student@master:~$ kubectl run --generator=run-pod/v1 \
hostnetwork -n ch6 --dry-run -o yaml \
--image=frjaraur/nettools:minimal -- sleep 300 > hostnetwork.pod.yaml
```

Now, with our favorite editor we just add "hostNetwork: true" for our Pod's containers:
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: hostnetwork
  name: hostnetwork
spec:
  hostNetwork: true
  containers:
  - args:
    - sleep
    - "300"
    image: frjaraur/nettools:minimal
    name: hostnetwork
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Then we create this new resource.
```
student@master:~$ kubectl create -f hostnetwork.pod.yaml -n ch6
pod/hostnetwork created
```

Pod has now the IP address of our Kubernetes worker node (You will have a different IP address):
```
student@master:~$ kubectl get pods -o wide -n ch6
NAME                          READY   STATUS    RESTARTS   AGE   IP               NODE            NOMINATED NODE   READINESS GATES
hostnetwork                   1/1     Running   0          89s   10.10.10.13      local-worker2   <none>           <none>
test                          1/1     Running   0          33m   192.168.64.195   local-worker1   <none>           <none>
webserver1-b6b6d6dcb-dpdvt    1/1     Running   0          90m   192.168.89.193   local-worker2   <none>           <none>
webserver2-5896b78565-5xcs2   1/1     Running   0          90m   192.168.64.193   local-worker1   <none>           <none>
```

Then we just execute again some tests within deployed Pod. We test "webserver1" Pod and its associated Service.
```
student@master:~$ kubectl exec -ti \
-n ch6 test -- curl 192.168.89.193:80
<html><body><h1>It works!</h1></body></html>


student@master:~$ kubectl exec -ti \
-n ch6 test -- curl 10.107.186.22:1080
<html><body><h1>It works!</h1></body></html>
```

Therefore, Pods sharing nodes' network namespace are also able to access deployed Services and Pods.

We can remove "hostnetwork" Pod.
```
student@master:~$ kubectl delete -n ch6 pod/hostnetwork
pod "hostnetwork" deleted
```

---
In this lab we have reviewed the most important concepts from Kubernetes Networking Model:
- Containers within a Pod have the same IP address.
- Pods have unique IPs.
- Pods-to-Pods communication works on distributed hosts without NAT.
- Agents or any process running on a node can reach all Pods and Services deployed on that node or any other node within the cluster.
- Pods in host network can communicate with all pods on all nodes without NAT.
- Third-Party Solutions provide this Model (Calico in this lab).


---

## Lab2 - Verify Kubernetes DNS

In this lab we are going to learn the most important Kubernetes' intenral DNS most important features.

1 - Services deployed within Kubernetes cluster are published in internal DNS automatically. Service's DNS-restriged IP address is Service's clusterIP. 
We deploy a test pod using 
```
student@master:~$ kubectl run -n ch6 --rm test \
--image=frjaraur/nettools:minimal --restart=Never \
--attach=true -ti -- sh
If you don't see a command prompt, try pressing enter.
/ # host webserver1
webserver1.ch6.svc.cluster.local has address 10.107.186.22
/ # host webserver2
webserver2.ch6.svc.cluster.local has address 10.102.13.56
/ # exit
```

We get the IP addesses of "webserver1" and "webserver2" Services.

Notice that internal FQDN is SERVICE_NAME.NAMESPACE.svc.CLUSTER_NAME.local

2 - Now we will increase the number of replicas and review changes.

We will increase the number of replicas of "webserver1" changing running resource's properties. We will use ___kubectl edit deployment___ to change Deployment's instances.
```
student@master:~$ kubectl edit deploy -n ch6 webserver1
```

Resource file will appear for us and we can change replicas' value from "1" to "3":
```
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: "2020-05-27T14:57:01Z"
  generation: 2
  labels:
    app: webserver1
  name: webserver1
  namespace: ch6
  resourceVersion: "22677"
  selfLink: /apis/apps/v1/namespaces/ch6/deployments/webserver1
  uid: 104b18a6-9f57-4173-a406-a69498dfc115
spec:
  progressDeadlineSeconds: 600
  replicas: 3
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: webserver1
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
"/tmp/kubectl-edit-jzl7n.yaml" 67L, 1870C 
```

Once changes are made and we exit the file, changes are committed.
```
deployment.apps/webserver1 edited
```

We can review again "webserver1" Service's IP address.
```
student@master:~$ kubectl run -n ch6 --rm test --image=frjaraur/nettools:minimal --restart=Never --attach=true -ti -- host webserver1
webserver1.ch6.svc.cluster.local has address 10.107.186.22
pod "test" deleted
```

Nothing has changed. Service's IP address is kept during its lifetime.

3 - Let's verify DNS Pods within the cluster:
```
student@master:~$ kubectl get pods -n kube-system
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-7489ff5b7c-rcdqx   1/1     Running   0          166m
calico-node-hhjws                          1/1     Running   0          159m
calico-node-p5mb4                          1/1     Running   0          166m
calico-node-ps6z8                          1/1     Running   0          152m
coredns-6955765f44-fq29q                   1/1     Running   0          166m
coredns-6955765f44-nvmzw                   1/1     Running   0          166m
etcd-local-master                          1/1     Running   0          166m
kube-apiserver-local-master                1/1     Running   0          166m
kube-controller-manager-local-master       1/1     Running   1          166m
kube-proxy-llsl9                           1/1     Running   0          152m
kube-proxy-mxr8h                           1/1     Running   0          166m
kube-proxy-t8292                           1/1     Running   0          159m
kube-scheduler-local-master                1/1     Running   1          166m
```

We can also review DNS associated Service:
```
student@master:~$ kubectl get services -n kube-system
NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
kube-dns   ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   167m
```

Therefore Pods are using this IP address for services resolution.
```
student@master:~$ kubectl run -n ch6 --rm test --image=frjaraur/nettools:minimal --restart=Never --attach=true -ti -- cat /etc/resolv.conf
nameserver 10.96.0.10
search ch6.svc.cluster.local svc.cluster.local cluster.local
options ndots:5
pod "test" deleted
```

We can take a quick look at "coredns" Deployment:
```
student@master:~$ kubectl get deploy coredns -n kube-system -o yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: "2020-05-27T14:34:59Z"
  generation: 1
  labels:
    k8s-app: kube-dns
  name: coredns
  namespace: kube-system
...
...
spec:
  progressDeadlineSeconds: 600
  replicas: 2
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: kube-dns
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        k8s-app: kube-dns
    spec:
      containers:
      - args:
        - -conf
        - /etc/coredns/Corefile
        image: k8s.gcr.io/coredns:1.6.5
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 5
          httpGet:
            path: /health
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 60
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 5
        name: coredns
        ports:
        - containerPort: 53
          name: dns
          protocol: UDP
        - containerPort: 53
          name: dns-tcp
          protocol: TCP
        - containerPort: 9153
          name: metrics
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /ready
            port: 8181
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        resources:
          limits:
            memory: 170Mi
          requests:
            cpu: 100m
            memory: 70Mi
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            add:
            - NET_BIND_SERVICE
            drop:
            - all
          readOnlyRootFilesystem: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /etc/coredns
          name: config-volume
          readOnly: true
      dnsPolicy: Default
      nodeSelector:
        beta.kubernetes.io/os: linux
      priorityClassName: system-cluster-critical
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      serviceAccount: coredns
      serviceAccountName: coredns
      terminationGracePeriodSeconds: 30
      tolerations:
      - key: CriticalAddonsOnly
        operator: Exists
      - effect: NoSchedule
        key: node-role.kubernetes.io/master
      volumes:
      - configMap:
          defaultMode: 420
          items:
          - key: Corefile
            path: Corefile
          name: coredns
        name: config-volume
status:
...
...
```

Notice that "coredns" ConfigMap is used to configure "coredns" Pods:
```
student@master:~$ kubectl get configmap -n kube-system coredns -o yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        prometheus :9153
        forward . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
...
...
```

DNS will forward all external queries to CoreDNS' Pod's resolver, configured in Pod's "/etc/resolv.conf" file (this points to your host's resolv.conf configured resolvers).

>NOTE: By default, Pods resolution is disabled ("pods insecure") in ConfigMap. We can change this behavior to use "192-168-89-193.ch6.pods.cluster.local", although it is not very useful.

Kubelet configures CoreDNS's Service's IP address. Remember that Kubelet is a systemd-service in out environment. Using systemctl status kubelet, we can observe that "/etc/systemd/system/kubelet.service.d/10-kubeadm.conf" file guides us to "/var/lib/kubelet/config.yaml" file, which is the Kubelet's configuration file.
```
root@local-master:/etc/kubernetes# cat /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
...
...
clusterDNS:
- 10.96.0.10
clusterDomain: cluster.local
...
...
```

We notice that Service's IP address is defined here with its own DNS' prefix.

---
In this lab we have learned how Kubernetes implments its own internal DNS to manage Service's interactions and external resolution (forwarding). We also learned which parameters are needed and where are the files required to change DNS's behavior.

---

## Lab 3 - Using Kubernetes Nginx Ingress Controller

In this lab we will deploy Kubernetes Nginx Ingress Controller to publish two simple applications with redirectios between them.

1 - We will first deploy Kubernetes' Nginx Controller:

```
student@master:~$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml


student@master:~$ kubectl get deployments -A
NAMESPACE       NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
default         blue                       3/3     3            3           13m
default         red                        2/2     2            2           13m
ingress-nginx   nginx-ingress-controller   1/1     1            1           58m
kube-system     calico-kube-controllers    1/1     1            1           77m
kube-system     coredns                    2/2     2            2           77m
```

2 - Now we publish Ingress Controller's service as NodePort.
```
student@master:~$ kubectl expose deployment nginx-ingress-controller -n ingress-nginx --type=NodePort --port=80 --target-port=80
service/nginx-ingress-controller exposed

student@master:~$ kubectl get svc -n ingress-nginx
NAME                       TYPE       CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
nginx-ingress-controller   NodePort   10.99.77.35   <none>        80:30043/TCP   16m
```

3 - Let's deploy two simple applications showing colors.
 - red - 2 instances
 - blue - 3 instances

 - COLORS application's containers expose port 3000. 

#### __Lab Schema:__
![Ingress Controller Red-Blue Deployment](../Images/IC_lab.jpg)


#### Red Deployment

We first create [red.deployment.yml](./red.deployment.yml) for "red" application:

```
student@master:~$ cat <<-EOF >red.deployment.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: red
  namespace: ch6
  name: red
spec:
  replicas: 2
  selector:
    matchLabels:
      app: red
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: red
        webinar: day3
    spec:
      containers:
      - image: codegazers/colors:1.16
        name: red-container
        imagePullPolicy: Always
        resources: {}
        env:
        - name: COLOR
          value: red
EOF
```

#### Blue Deployment

And then we deploy the "blue" application, [blue.deployment.yml](./blue.deployment.yml):

```
student@master:~$ cat <<-EOF >blue.deployment.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: blue
  namespace: ch6
  name: blue
spec:
  replicas: 3
  selector:
    matchLabels:
      app: blue
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: blue
        webinar: day3
    spec:
      containers:
      - image: codegazers/colors:1.16
        name: blue-container
        imagePullPolicy: Always
        resources: {}
        env:
        - name: COLOR
          value: blue
EOF

```

We will just deploy these __Deployment__ definitions and __expose internally__ their ports.
```
student@master:~$ kubectl create -f red.deployment.yml
deployment.apps/red created

student@master:~$ kubectl expose deployment red -n ch6 --name=red-svc --labels webinar=lab3 --port=3000 --target-port=3000
service/red exposed


student@master:~$ kubectl create -f blue.deployment.yml
deployment.apps/blue created

student@master:~$ kubectl expose deployment blue -n ch6 --name=blue-svc --labels webinar=lab3 --port=3000 --target-port=3000
service/blue exposed

```

4 - We list all applications' components:

```
student@master:~$ kubectl get all  -n ch6 --show-labels
NAME                        READY   STATUS    RESTARTS   AGE     LABELS
pod/blue-54bc8d95b9-ddzrq   1/1     Running   0          3m51s   app=blue,pod-template-hash=54bc8d95b9
pod/blue-54bc8d95b9-p5vv7   1/1     Running   0          3m54s   app=blue,pod-template-hash=54bc8d95b9
pod/blue-54bc8d95b9-z6s2t   1/1     Running   0          3m57s   app=blue,pod-template-hash=54bc8d95b9
pod/red-74f5dbcc7d-k4wk8    1/1     Running   0          3m59s   app=red,pod-template-hash=74f5dbcc7d
pod/red-74f5dbcc7d-w7sjv    1/1     Running   0          4m3s    app=red,pod-template-hash=74f5dbcc7d

NAME           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE   LABELS
service/blue   ClusterIP   10.102.231.100   <none>        3000/TCP   54s   webinar=lab3
service/red    ClusterIP   10.111.145.245   <none>        3000/TCP   66s   webinar=lab3

NAME                   READY   UP-TO-DATE   AVAILABLE   AGE     LABELS
deployment.apps/blue   3/3     3            3           5m14s   app=blue
deployment.apps/red    2/2     2            2           5m8s    app=red

NAME                              DESIRED   CURRENT   READY   AGE     LABELS
replicaset.apps/blue-54bc8d95b9   3         3         3       3m57s   app=blue,pod-template-hash=54bc8d95b9
replicaset.apps/red-74f5dbcc7d    2         2         2       4m3s    app=red,pod-template-hash=74f5dbcc7d
```

5 - We are now ready for __internal testing__.
We can use Ingress Controller's internal service's IP address and some of our applications' host headers.

```
student@master:~$ curl -H "host: blue.example.com" -L 10.99.77.35
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.17.8</center>
</body>
</html>
```

We get an error because we haven't defined any __Ingress__ resource yet.

6 - Let's deploy our applications' ingress definitions. We create [colors.ingress.yml](./colors.ingress.yml):
```
student@master:~$ cat <<-EOF >colors.ingress.yml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: colors-ingress
  namespace: ch6
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /\$1
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      - path: /red/(.*)
        backend:
          serviceName: red-svc
          servicePort: 3000
      - backend:
          serviceName: blue-svc
          servicePort: 3000


  - host: red.enterprise.es
    http:
      paths:
      - backend:
          serviceName: red-svc
          servicePort: 3000
EOF
```

> NOTE: Notice that we used "/\$1" instead of "$1" this is due to re use of cat to create the new file. If we  use "$1", generated output will be blank. 

Now, we deploy this ___Ingress___ resource.
```
student@master:~$ kubectl create -f colors.ingress.yml
ingress.extensions/colors-ingress created
```

We reviewvdefined resources:
```
student@master:~$ kubectl get ingress
NAME             HOSTS                                ADDRESS   PORTS   AGE
colors-ingress   blue.example.com,red.enterprise.es             80      45s
```

7 - We now can test our definitions.

We defined "red.enterprise.es" host header for "red" application. Therefore we can use Ingress Controller's internal service's IP address and red-defined host header.
```
student@master:~$ curl -H "host: red.enterprise.es" 10.99.77.35/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-w7sjv
CONTAINER_IP:  10.244.69.206
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux
```

We can apply same concepts with "blue.example.com" host's header for blue application. Therefore we can also use Ingress Controller's internal service's IP address and blue-defined host's header.

```
student@master:~$ curl -H "host: blue.example.com" -L 10.99.77.35/text
<html>
<head>
    <title>blue</title>
    <meta charset="utf-8" />
    <style>
        body {
            background-color: blue;
        }
        .center {
            padding: 70px 0;
        }
        h2 {
            color: grey;
            text-align: left;
            font-family: "Sans-serif", Arial;
            font-style: oblique;
            font-size: 14px;
        }
        table {
            border-collapse: collapse;
            margin-left:auto;
            margin-right:auto;
            background-color: #F0F8FF;
        }

        table, th, td {
            border: 1px solid black;

        }
        tr:hover {background-color: #DCDCDC}

        p.padding {
            padding-top: 2cm;
        }
    </style>
</head>
<body>
<div class="center">
    <table style="float:center" >
        <tr><td>Container IP:</td><td> 10.244.69.208</td></tr>
        <tr><td>Client IP:</td><td>undefined</td></tr>
        <tr><td>Container Name:</td><td>blue-54bc8d95b9-ddzrq</td></tr>
        <tr><td>Color:</td><td>blue</td></tr>
        <tr><td>Application Version:</td><td>1.20</td></tr>
    </table>
</div>
</body>

</html>
</body>
</html>
```


8 - But we also defined __/red__ path to be forwarded to red service. Therefore, we can reach "red" service using "blue.example.com".
```
student@master:~$ curl -H "host: blue.example.com" -L 10.99.77.35/red/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-k4wk8
CONTAINER_IP:  10.244.176.205
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux
```

>NOTE:
> We need to use URL rewriting but this will be applied to all the hosts defined within the Ingress resource. To avoid this situation, we have just split them in two Ingress resource files. This is described in following step.



9 - We will create two different Ingress resources to avoid URL rewriting errors.
First, we create [colors-simple.ingress.yml](./colors-simple.ingress.yml) file.
```
student@master:~$ cat <<-EOF >colors-simple.ingress.yml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: colors-ingress
  annotations:
  namespace: ch6
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      - backend:
          serviceName: blue-svc
          servicePort: 3000


  - host: red.enterprise.es
    http:
      paths:
      - backend:
          serviceName: red-svc
          servicePort: 3000
EOF


student@master:~$ kubectl replace -f colors-simple.ingress.yml
ingress.extensions/colors-ingress replaced
```

Then we create [colors-rewrite.ingress.yml](./colors-rewrite.ingress.yml) file:
```
student@master:~$ cat <<-EOF >colors-rewrite.ingress.yml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: colors-ingress-rewrite
  namespace: ch6
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /\$1
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      #- path: /red(/|$)(.*)
      - path: /red/(.*)
        backend:
          serviceName: red-svc
          servicePort: 3000
EOF


student@master:~$ kubectl create -f ingress-rewrite.ingress.yml
ingress.extensions/colors-ingress-rewrite created
```

> NOTE: Notice that we used "/\$1" instead of "$1" this is due to re use of cat to create the new file. If we  use "$1", generated output will be blank. 

10 - Now we can test "blue" and "red" services acceses again, even using "blue.example.com" to reach the "red" application's backends.
```
student@master:~$ curl -H "host: blue.example.com" -L 10.99.77.35/text
APP_VERSION: 1.20
COLOR: blue
CONTAINER_NAME: blue-54bc8d95b9-z6s2t
CONTAINER_IP:  10.244.176.206
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux


student@master:~$ curl -H "host: blue.example.com" -L 10.99.77.35/red/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-w7sjv
CONTAINER_IP:  10.244.69.206
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux


student@master:~$ curl -H "host: red.enterprise.es" 10.99.77.35/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: red-74f5dbcc7d-k4wk8
CONTAINER_IP:  10.244.176.205
CLIENT_IP: 10.244.195.128
CONTAINER_ARCH: linux
```

In this lab we learned how to implement simple redirections between services using ___Ingress___ resources.

---

## Lab 4 - Deploying Ingress Resources with TLS/SSL.

In this lab we will deploy a couple of webservers. They will be deployed without TLS/SSL security but we can improve their security encrypting communications from users to the deployed Ingress Controller. We will deploy certificates for each service's backend hence security will be provided at Ingress Controller level.

1 - Deploy first web server, "webserver1". In this case we create [webserver1.full.yml](./webserver1.full.yml):
```
student@master:~$ cat <<-EOF >webserver1.full.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: webserver1
  name: webserver1
  namespace: ch6  
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webserver1
  template:
    metadata:
      labels:
        app: webserver1
    spec:
      containers:
      - image: nginx:alpine
        name: nginx
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: webserver1
  name: webserver1
  namespace: ch6  
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: webserver1

EOF
```

We create "webserver1" resources (Deployment and Service):
```
student@master:~$ kubectl create -f webserver1.full.yml             
deployment.apps/webserver1 created
service/webserver1 created
```

2 - Now we create a second web server, "webserver2". In this case we create [webserver2.full.yml](./webserver2.full.yml):
```
student@master:~$ cat <<-EOF >webserver2.full.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: webserver2
  name: webserver2
  namespace: ch6
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webserver2
  template:
    metadata:
      labels:
        app: webserver2
    spec:
      containers:
      - image: httpd:alpine
        name: httpd
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: webserver2
  name: webserver2
  namespace: ch6  
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: webserver2
EOF
```

And then we create "webserver2" resources (Deployment and Service):
```
student@master:~$ kubectl create -f webserver2.full.yml             
deployment.apps/webserver2 created
service/webserver2 created
```

3 - Once we have deployed "webserver1" and "webserver2" applications, we can create their certificates. We are going to use them in our Ingress Controller, instead of having to modify our applications' backends. We are using auto-signed certificates for both web servers.
 ```
student@master:~$ openssl req -x509 -nodes -days 30 -newkey rsa:2048 -keyout webserver1.key -out webserver1.crt -subj "/CN=webserver1.local.lab"
Generating a 2048 bit RSA private key
............................+++++
.+++++
writing new private key to 'webserver1.key'
-----

student@master:~$ openssl req -x509 -nodes -days 30 -newkey rsa:2048 -keyout webserver2.key -out webserver2.crt -subj "/CN=webserver2.local.lab"
Generating a 2048 bit RSA private key
................................................................................................+++++
......................+++++
writing new private key to 'webserver2.key'
-----
```

We have just used "webserver1.local.lab" and "webserver2.local.lab" as Common Names for internal FQDN names.

4 - We will create Secret resources for Ingress resources:
```
student@master:~$ kubectl create secret -n ch6 tls webserver1-certificates --key webserver1.key  --cert webserver1.crt
secret/webserver1-certificates created

student@master:~$ kubectl create secret -n ch6 tls webserver2-certificates --key webserver2.key  --cert webserver2.crt
secret/webserver2-certificates created

student@master:~$ kubectl get secret -n ch6
NAME                      TYPE                                  DATA   AGE
default-token-fwmpl       kubernetes.io/service-account-token   3      42h
webserver1-certificates   kubernetes.io/tls                     2      19h
webserver2-certificates   kubernetes.io/tls                     2      19h
```

5 - Now we create Ingress resources for both web servers using  [webservers.ingress.yml](./webservers.ingress.yaml):
```
student@master:~$ cat <<-EOF >webservers.ingress.yml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: webservers
  namespace: ch6
spec:
  tls:
  - hosts:
    - webserver1.local.lab
    secretName: webserver1-certificates
  - hosts:
    - webserver2.local.lab
    secretName: webserver2-certificates
  rules:
  - host: webserver1.local.lab
    http:
      paths:
      - backend:
          serviceName: webserver1
          servicePort: 80
        path: /
  - host: webserver2.local.lab
    http:
      paths:
      - backend:
          serviceName: webserver2
          servicePort: 80
        path: /
EOF
```

And then, we deploy this Ingress resource:
```
student@master:~$ kubectl create -f webservers.ingress.yml
ingress.networking.k8s.io/webservers created
```

6 - Now, we review deployed Ingress resource and Ingress Controller deployed service port.
```
student@master:~$ kubectl get ingress -n ch6
NAME         HOSTS                                       ADDRESS   PORTS     AGE
webservers   webserver1.local.lab,webserver2.local.lab             80, 443   59s
```

7 - If you don't exposed HTTPS port on your Ingress Conroller is time to create a new service or modify already created one. In this lab we will just create a new one:
```
student@master:~$ kubectl expose deployment nginx-ingress-controller -n ingress-nginx --type=NodePort --port=443 --target-port=443 --name nginx-ingress-controller-https
service/nginx-ingress-controller-https exposed
```

We review nginx-ingress-controller Service's port (we used NodePort type service):
```
student@master:~$ kubectl get -o jsonpath="{.spec.ports[0].nodePort}" service nginx-ingress-controller-https -n ingress-nginx
30957
```

or
```
student@master:~$ kubectl get svc -n ingress-nginx
NAME                       TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
nginx-ingress-controller   NodePort   10.98.130.26   <none>        80:32289/TCP   15s
nginx-ingress-controller-https   NodePort   10.111.161.117   <none>        443:30957/TCP   106s

```

7 - Finally we verify web server's accesses. First we just review HTTPS default access.
```
student@master:~$ curl -k https://0.0.0.0:30957
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.17.8</center>
</body>
</html>
```

Then we use specific host's header. First we test "webserver1.local.lab":
```
student@master:~$ curl -H "host: webserver.local.lab" -k https://0.0.0.0:30957
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Same with "webserver2.local.lab":
```
student@master:~$ curl -H "host: webserver2.local.lab" -k https://0.0.0.0:30957
<html><body><h1>It works!</h1></body></html>
```

It works as expected. Let's verify received certificate:
```
student@master:~$ openssl s_client -servername webserver2.local.lab -connect 0.0.0.0:30957 -showcerts </dev/null 2>/dev/null | openssl x509 -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            8c:19:3d:d5:57:9a:e1:88
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN = webserver2.local.lab
        Validity
            Not Before: May 28 14:10:14 2020 GMT
            Not After : Jun 27 14:10:14 2020 GMT
        Subject: CN = webserver2.local.lab
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:fb:45:b0:0f:9d:9b:98:77:7d:4c:82:fd:ae:d3:
                    65:90:ec:a9:07:6b:b0:6f:59:c7:c8:7b:dc:68:67:
                    9d:4d:c8:25:a2:37:1c:cd:0c:74:65:c9:24:b1:68:
                    4e:b3:41:3e:b3:40:82:a1:ed:d3:6d:ee:ba:25:d1:
                    f8:3a:06:7c:d9:c5:38:2f:1c:a7:27:da:c2:0e:fe:
                    c8:ae:43:22:f6:d1:53:2c:73:23:5f:fe:f4:79:cd:
                    a5:a9:69:5f:5d:5c:47:bf:29:5f:87:a7:12:f8:6a:
                    58:6a:6c:de:ef:85:a4:eb:1f:2f:f9:ea:81:21:bb:
                    de:66:00:71:3f:4d:3e:93:59:71:53:5d:3a:54:9d:
                    2a:fa:d8:07:a9:61:56:e7:0f:00:ac:1e:78:bd:ab:
                    cd:cc:5c:01:2f:d0:1f:30:ab:e3:9a:39:68:cd:2c:
                    f4:36:e9:b5:a6:30:26:ee:6c:62:37:1a:78:a2:98:
                    23:85:8c:4b:47:b3:4e:f8:3a:eb:52:fc:92:3d:09:
                    52:f9:52:fa:29:38:64:a8:a5:ed:c0:c1:47:61:85:
                    9f:ad:73:6f:c4:4d:a8:48:3f:87:f7:c8:4d:ad:08:
                    c4:d7:7a:6a:55:88:d9:b8:89:7d:a7:e8:01:6b:68:
                    ca:5f:d1:06:81:68:a3:50:6f:2b:e5:fe:c5:6a:71:
                    17:65
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                EC:AF:A4:66:31:D9:EE:7A:0D:BB:54:34:30:0D:B8:9E:7B:01:07:6C
            X509v3 Authority Key Identifier: 
                keyid:EC:AF:A4:66:31:D9:EE:7A:0D:BB:54:34:30:0D:B8:9E:7B:01:07:6C

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         af:37:a4:11:7a:3e:1e:f2:75:de:f4:ca:ff:bc:db:fe:88:ef:
         c2:2b:5e:4a:00:cc:71:44:88:45:a4:0d:f4:ba:54:16:e4:dc:
         3d:bd:6c:d2:93:14:39:17:af:57:f3:e2:11:45:5c:ec:f3:c8:
         6d:2a:69:ce:b8:5c:18:82:ac:e3:bc:2f:43:e8:53:da:24:9f:
         0d:b7:01:17:2d:31:45:28:ef:a4:ba:89:d8:c3:0e:77:1d:36:
         d5:86:0c:ea:2d:c6:68:43:16:34:c4:e9:15:43:45:e4:62:d6:
         e3:e9:58:56:a3:3e:f9:d5:ab:1b:62:3f:3e:87:b1:13:5c:dc:
         7d:cc:28:7d:cf:0f:e0:3f:07:32:6b:23:fd:0b:9b:ef:a3:81:
         87:b7:ba:ed:fc:34:70:0c:11:a4:fb:9b:1c:f0:af:73:77:96:
         46:67:ae:3f:fb:7b:7e:e7:a9:8f:08:1f:6e:ca:d0:c0:7e:83:
         9f:f1:22:57:e4:7c:76:85:7f:83:b2:27:ed:8a:4a:86:3b:6e:
         1c:09:68:9a:f2:d6:b6:62:8b:45:ab:ac:7b:ce:e0:29:4d:3a:
         3d:54:80:4d:c0:5c:a5:70:d3:87:45:ca:90:ea:81:ec:60:aa:
         31:43:bd:e0:29:02:19:1e:91:1a:ff:b3:a6:d9:d6:bb:dc:57:
         03:09:81:32
-----BEGIN CERTIFICATE-----
MIIDFDCCAfygAwIBAgIJAIwZPdVXmuGIMA0GCSqGSIb3DQEBCwUAMB8xHTAbBgNV
BAMMFHdlYnNlcnZlcjIubG9jYWwubGFiMB4XDTIwMDUyODE0MTAxNFoXDTIwMDYy
NzE0MTAxNFowHzEdMBsGA1UEAwwUd2Vic2VydmVyMi5sb2NhbC5sYWIwggEiMA0G
CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQD7RbAPnZuYd31Mgv2u02WQ7KkHa7Bv
WcfIe9xoZ51NyCWiNxzNDHRlySSxaE6zQT6zQIKh7dNt7rol0fg6BnzZxTgvHKcn
2sIO/siuQyL20VMscyNf/vR5zaWpaV9dXEe/KV+HpxL4alhqbN7vhaTrHy/56oEh
u95mAHE/TT6TWXFTXTpUnSr62AepYVbnDwCsHni9q83MXAEv0B8wq+OaOWjNLPQ2
6bWmMCbubGI3GniimCOFjEtHs074OutS/JI9CVL5UvopOGSope3AwUdhhZ+tc2/E
TahIP4f3yE2tCMTXempViNm4iX2n6AFraMpf0QaBaKNQbyvl/sVqcRdlAgMBAAGj
UzBRMB0GA1UdDgQWBBTsr6RmMdnueg27VDQwDbieewEHbDAfBgNVHSMEGDAWgBTs
r6RmMdnueg27VDQwDbieewEHbDAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEB
CwUAA4IBAQCvN6QRej4e8nXe9Mr/vNv+iO/CK15KAMxxRIhFpA30ulQW5Nw9vWzS
kxQ5F69X8+IRRVzs88htKmnOuFwYgqzjvC9D6FPaJJ8NtwEXLTFFKO+kuonYww53
HTbVhgzqLcZoQxY0xOkVQ0XkYtbj6VhWoz751asbYj8+h7ETXNx9zCh9zw/gPwcy
ayP9C5vvo4GHt7rt/DRwDBGk+5sc8K9zd5ZGZ64/+3t+56mPCB9uytDAfoOf8SJX
5Hx2hX+DsiftikqGO24cCWia8ta2YotFq6x7zuApTTo9VIBNwFylcNOHRcqQ6oHs
YKoxQ73gKQIZHpEa/7Om2da73FcDCYEy
-----END CERTIFICATE-----
```

We review "Issuer: CN = webserver2.local.lab" and "Subject: CN = webserver2.local.lab". We reached the right backend and get the right certificate.


8 - Finally, we can verify HTTP port instead of HTTPS and review results:
```
student@master:~$ export HTTP_PORT=$(kubectl get -o jsonpath="{.spec.ports[0].nodePort}" service nginx-ingress-controller -n ingress-nginx)

student@master:~$ curl -H "host: webserver1.local.lab" http://0.0.0.0:${HTTP_PORT}
<html>
<head><title>308 Permanent Redirect</title></head>
<body>
<center><h1>308 Permanent Redirect</h1></center>
<hr><center>nginx/1.17.8</center>
</body>
</html>


student@master:~$ curl -H "host: webserver2.local.lab" http://0.0.0.0:${HTTP_PORT}
<html>
<head><title>308 Permanent Redirect</title></head>
<body>
<center><h1>308 Permanent Redirect</h1></center>
<hr><center>nginx/1.17.8</center>
</body>
</html>
```

Notice that we used __kubectl get -o jsonpath="{.spec.ports[0].nodePort}" service nginx-ingress-controller -n ingress-nginx__ to use obtained value using __jsonpath__.

>NOTE: If we try to follow redirection, we will get an error:
>```
>student@master:~$ curl -L -H "host: webserver2.local.lab" http://0.0.0.0:${HTTP_PORT}
>curl: (7) Failed to connect to webserver2.local.lab port 443: No route to host
>```
>This is due to the fact that we are modifying host's headers in curl, but real DNS entries don't exist and we can not reach redirected host because it is unknown in local DNS.
>

In this lab we learned how to implement TLS/SSL communications even if out application's backends are using HTTP instead of HTTPS. TLS tunnles are managed by Ingress Controller.

---

## Lab 5 - Multiple Ingress Controllers using ingressClass resources.

In this lab we will deploy a second __Ingress Controller__ using __Helm__ and redeploy "webserver2" __Deployment__ using this new __Ingress Controller__.

1 - First we add "https://kubernetes.github.io/ingress-nginx" as a new repo for __Helm's Charts__:
```
student@master:~$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

student@master:~$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ingress-nginx" chart repository
...Successfully got an update from the "stable" chart repository
Update Complete. ⎈ Happy Helming!⎈ 
```

2 - Then we download all configurable values for "ingress-nginx/ingress-nginx" __Chart__. This will help us to configure a new __ingressClass__ for the second __Ingress Controller__.
```
student@master:~$ helm show values ingress-nginx/ingress-nginx > ingress-nginx.values.yaml
```

Then we edit created file, "ingress-nginx.values.yaml", with our favorite editor and locate ___class___ key.
```
 60 
 61   ## Name of the ingress class to route through this controller
 62   ##
 63   ingressClass: nginx
 64 
 65   # labels to add to the pod container metadata
```

We change its value to "ch6".
```
 60 
 61   ## Name of the ingress class to route through this controller
 62   ##
 63   ingressClass: ch6
 64 
 65   # labels to add to the pod container metadata
```

3 - We also locate __Service__'s type configuration to enable __NodePort__.
```
283     ports:
284       http: 80
285       https: 443
286 
287     targetPorts:
288       http: http
289       https: https
290 
291     type: LoadBalancer
292 
293     # type: NodePort
294     # nodePorts:
```

We change it to __NodePort__:
```
283     ports:
284       http: 80
285       https: 443
286 
287     targetPorts:
288       http: http
289       https: https
290 
291     type: NodePort
292 
293     # type: NodePort
294     # nodePorts:
```

3 - Then we install this new __Ingress Controller__ in "ch6" __Namespace__, using custom values.
```
student@master:~$ helm install custom-ingress -n ch6 -f ingress-nginx.values.yaml ingress-nginx/ingress-nginx
NAME: custom-ingress
LAST DEPLOYED: Fri May 29 14:04:30 2020
NAMESPACE: ch6
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
Get the application URL by running these commands:
  export HTTP_NODE_PORT=$(kubectl --namespace ch6 get services -o jsonpath="{.spec.ports[0].nodePort}" custom-ingress-ingress-nginx-controller)
  export HTTPS_NODE_PORT=$(kubectl --namespace ch6 get services -o jsonpath="{.spec.ports[1].nodePort}" custom-ingress-ingress-nginx-controller)
  export NODE_IP=$(kubectl --namespace ch6 get nodes -o jsonpath="{.items[0].status.addresses[1].address}")

  echo "Visit http://$NODE_IP:$HTTP_NODE_PORT to access your application via HTTP."
  echo "Visit https://$NODE_IP:$HTTPS_NODE_PORT to access your application via HTTPS."

An example Ingress that makes use of the controller:

  apiVersion: networking.k8s.io/v1beta1
  kind: Ingress
  metadata:
    annotations:
      kubernetes.io/ingress.class: ch6
    name: example
    namespace: foo
  spec:
    rules:
      - host: www.example.com
        http:
          paths:
            - backend:
                serviceName: exampleService
                servicePort: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
        - hosts:
            - www.example.com
          secretName: example-tls

If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls
```

4 - Now we remove previously created "webservers" __Ingress__ resource.
```
student@master:~$ kubectl get ingress -n ch6
NAME         HOSTS                                       ADDRESS   PORTS     AGE

webservers   webserver1.local.lab,webserver2.local.lab             80, 443   148m
student@master:~$ kubectl delete ingress -n ch6 webservers 
ingress.extensions "webservers" deleted
```

5 - We deploy two different __Ingress__ resources. First one will include "webserver1.local.lab" host's header. We create [webserver1.ingress.yaml](./webserver1.ingress.yaml) with the following content:
```
student@master:~$ cat<<EOF> webserver1.ingress.yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: webserver1
  namespace: ch6
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  tls:
  - hosts:
    - webserver1.local.lab
    secretName: webserver1-certificates
  rules:
  - host: webserver1.local.lab
    http:
      paths:
      - backend:
          serviceName: webserver1
          servicePort: 80
        path: /
EOF
```

And we deploy this __Ingress__ resource.
```
student@master:~$ kubectl create -f webserver1.ingress.yaml
ingress.networking.k8s.io/webserver1 created
```

We review and test this new resource:
```
student@master:~$ kubectl get ingress -n ch6
NAME         HOSTS                  ADDRESS   PORTS     AGE
webserver1   webserver1.local.lab             80, 443   8s

student@master:~$ curl -H "host: webserver1.local.lab" http://0.0.0.0:${HTTP_PORT}
<html>
<head><title>308 Permanent Redirect</title></head>
<body>
<center><h1>308 Permanent Redirect</h1></center>
<hr><center>nginx/1.17.8</center>
</body>
</html>
```

And going a bit further:
```
student@master:~$ export HTTPS_PORT=$(kubectl get -o jsonpath="{.spec.ports[0].nodePort}" service nginx-ingress-controller-https -n ingress-nginx)  

student@master:~$ curl -H "host: webserver1.local.lab" -k https://0.0.0.0:${HTTPS_PORT}
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```



We have the same results seen in previous lab. I works as expected.

6 - Let's create and deploy "webserver2" __Ingress__ resource.  In this case we create [webserver2.ingress.yaml](./webserver2.ingress.yaml) with the following content:
```
student@master:~$ cat<<EOF>webserver2.ingress.yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: webserver2
  namespace: ch6
  annotations:
    kubernetes.io/ingress.class: "ch6"
spec:
  rules:
  - host: webserver2.local.lab
    http:
      paths:
      - backend:
          serviceName: webserver2
          servicePort: 80
        path: /
EOF
```

Then we deploy this __Ingress__ resource:
```
student@master:~$ kubectl create -f webserver2.ingress.yaml
ingress.networking.k8s.io/webserver2 created
```

7 - We deployed the new __Ingress Controller__ using __NodePort__ and "ch6" __Namespace__. Then we review its published ports:
```
student@master:~$ kubectl get svc -n ch6
NAME                                                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
custom-ingress-ingress-nginx-controller             NodePort    10.105.108.41    <none>        80:31050/TCP,443:32742/TCP   58m
custom-ingress-ingress-nginx-controller-admission   ClusterIP   10.96.134.131    <none>        443/TCP                      58m
webserver1                                          ClusterIP   10.99.229.48     <none>        80/TCP                       23h
webserver2                                          ClusterIP   10.111.208.161   <none>        80/TCP                       22h
```

8 - We deployed in this lab "webserver2" __Ingress__ resource without TLS. Therefore we can test it in port 31050 (this may change in your environment), which is the __NodePort__ published port for "custom-ingress-ingress-nginx-controller" __Service__'s 80 port. This is the name acquired by the __Service__ associated with the __Ingress Controller__ customized and deployed using __Helm__.
```
student@master:~$ export CUSTOM_IC_HTTP_PORT=$(kubectl get -o jsonpath="{.spec.ports[0].nodePort}" service custom-ingress-ingress-nginx-controller -n ch6)

student@master:~$ echo $CUSTOM_IC_HTTP_PORT
31050

student@master:~$ curl -H "host: webserver2.local.lab" http://0.0.0.0:${CUSTOM_IC_HTTP_PORT}
<html><body><h1>It works!</h1></body></html>
```

This also works. Each webserver is published on its own __Ingress Controller__. In this lab we used:
- __Default Ingress Controller__ - Deployed using ___kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml___ on default "ingress-nginx"  __Namespace__.
- __Custom Ingress Contoller__ - Deployed on "ch6" __Namespace__ using ingress-nginx/ingress-nginx __Helm's Chart__ with custom "ch6" __IngressClass__.

We can remove everything created for this lab simply by removing "ch6" __Namespace__.

We learned in this lab that we can deploy multiple __Ingress Controllers__ usign __IngressClass__ resources. This is very useful and required in multi-tenant environments.

## Lab6 - Network Policies


In this lab we are going to have two different versions of our aplication.

Aplication version 1 has a management UI for the application, a client who access backend trhought facade. All these compoenents are distributed in 3 namespaces, management-ui, frontend and backend, distributed as follow:

![Image description](img/appsv1.png)

Our second version of the aplication includes a queue and payment deployments with their namespaces. These communications are ingres and egres communication. Whole aplication is shown in the follwing image:


![Image description](img/appsv2.png)



## APPLICATION VERSION 1


1 - Deployment of the APPLICATION V1

#### [namespaces.yml](./namespaces.yml)
```
# We deploy all neede ns for app v1 and app v2 
student@master:~$ cat<<EOF> namespaces.yml
apiVersion: v1
kind: Namespace
metadata:
  name: management-ui 
  labels:
    role: management-ui 
---
apiVersion: v1
kind: Namespace
metadata:
  name: frontend
  labels:
    requires: backend
---
apiVersion: v1
kind: Namespace
metadata:
  name: backend
  labels:
    requires: queue
---
apiVersion: v1
kind: Namespace
metadata:
  name: infrastructure
  labels:
    role: infrastructure
---
apiVersion: v1
kind: Namespace
metadata:
  name: pci
  labels:
    requires: queue
EOF

student@master:~$ kubectl create -f namespaces.yml
```
**MANAGEMENT UI**

#### [management.deploy.](./management.deploy.)
```
student@master:~$ cat<<EOF> management.deploy.yml
# management ui (deploy and service)
apiVersion: v1
kind: Service
metadata:
  name: management-ui 
  namespace: management-ui 
spec:
  type: NodePort
  ports:
  - port: 9001 
    targetPort: 9001
    nodePort: 32100
  selector:
    role: management-ui 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: management-ui 
  namespace: management-ui 
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web-ui
  template:
    metadata:
      labels:
        role: management-ui 
        app: web-ui
    spec:
      containers:
      - name: management-ui 
        image: dockersamples/star-collect:v1
        imagePullPolicy: Always
        ports:
        - containerPort: 9001
EOF

student@master:~$ kubectl create -f  management.deploy.yml

```

**CLIENT** 
#### [client.deploy.yml](./client.deploy.yml)
```
student@master:~$ cat<<EOF> client.deploy.yml
# deploy client (deploy and service)
apiVersion: v1
kind: Service
metadata:
  name: client
  namespace: frontend
spec:
  ports:
  - port: 9000 
    targetPort: 9000
  selector:
    app: client 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: client 
  namespace: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: client
  template:
    metadata:
      labels:
        app: client 
    spec:
      containers:
      - name: client 
        image: dockersamples/star-probe
        imagePullPolicy: Always
        command:
        - probe
        - --urls=http://facade.backend:80/status,http://payments.pci:6380/status,http://backend.backend:6379/status,http://client.frontend:9000/status,http://queue.infrastructure:5000/status
        ports:
        - containerPort: 9000 
EOF

student@master:~$ kubectl create -f client.deploy.yml
```
**FACADE**
#### [facade.deploy.yml](./facade.deploy.yml)
```
student@master:~$ cat<<EOF> facade.deploy.yml
# deploy facade (deploy and service)

apiVersion: v1
kind: Service
metadata:
  name: facade 
  namespace: backend
spec:
  ports:
  - port: 80 
    targetPort: 80 
  selector:
    app: facade
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: facade 
  namespace: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: facade
  template:
    metadata:
      labels:
        role: backend 
        app: facade
    spec:
      containers:
      - name: facade 
        image: dockersamples/star-probe
        imagePullPolicy: Always
        command:
        - probe
        - --http-port=80
        - --urls=http://facade.backend:80/status,http://payments.pci:6380/status,http://backend.backend:6379/status,http://client.frontend:9000/status,http://queue.infrastructure:5000/status
        ports:
        - containerPort: 80 
EOF

student@master:~$ kubeclt create -f facade.deploy.yml

```
**BACKEND**

#### [backend.yml](./backend.yml)
```
student@master:~$ cat<<EOF> backend.deploy.yml
# deploy backend (deploy and service)
apiVersion: v1
kind: Service
metadata:
  name: backend 
  namespace: backend
spec:
  ports:
  - port: 6379
    targetPort: 6379 
  selector:
    app: backend 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend 
  namespace: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        role: backend
        app: backend 
    spec:
      containers:
      - name: backend 
        image: dockersamples/star-probe
        imagePullPolicy: Always
        command:
        - probe
        - --http-port=6379
        - --urls=http://facade.backend:80/status,http://payments.pci:6380/status,http://backend.backend:6379/status,http://client.frontend:9000/status,http://queue.infrastructure:5000/status
        ports:
        - containerPort: 6379 
EOF

student@master:~$ kubectl create -f backend.deploy.yml

```

```
# on a browser

http://ip-node-from-cluster:32100
```
On kubernetes by defect all pod can talk with all pods.

![Image description](img/v1-1.png)


2 - Applying network policies

**Deny all communications between app namespaces**

#### [deny.np.yml](./deny.np.yml)
```
student@master:~$ cat<<EOF> deny.np.yml
# Deny rules for all namespaces for v1 and v2
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: frontend 
  name: default-deny-frontend
spec:
  podSelector:
    matchLabels: {}
  policyTypes:
  - Ingress
  ingress: []
---
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: backend 
  name: default-deny-backend
spec:
  podSelector:
    matchLabels: {}
  policyTypes:
  - Ingress
  ingress: []
EOF

student@master:~$ kubectl create -f deny.np.yml

```
```
# on a browser, you should see nothing as no communication is allow through our app

http://ip-node-from-cluster:32100
```

**Deploy the policy to allow the UI**

Management UI needs ingress access to all components of the application in order to have vision of these administrative tasks

#### [allow.np.yml](./allow.np.yml)
```
student@master:~$ cat<<EOF> allow.np.yml
# Done for all on V1 and V2
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: frontend
  name: allow-ui-frontend
spec:
  podSelector:
    matchLabels: {}
  policyTypes:
  - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              role: management-ui
---
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: backend
  name: allow-ui-backend
spec:
  podSelector:
    matchLabels: {}
  policyTypes:
  - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              role: management-ui
---
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: pci
  name: allow-ui-pci
spec:
  podSelector:
    matchLabels: {}
  policyTypes:
  - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              role: management-ui
---
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: infrastructure
  name: allow-ui-infrastructure
spec:
  podSelector:
    matchLabels: {}
  policyTypes:
  - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              role: management-ui
EOF

student@master:~$ kubectl create -f allow.np.yml
```

![Image description](img/v1-ui.png)


**Allow facade to speak to the backend**
Allow facada to speak to backend but only the needed port.
#### [facade-backend.np.yml](./facade-backend.np.yml)
```
student@master:~$ cat<<EOF> facade-backend.np.yml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: backend
  name: backend-policy
spec:
  podSelector:
    matchLabels:
      app: backend
  policyTypes:
  - Ingress
  ingress:
    - from:
        - podSelector:
            matchLabels:
              app: facade
      ports:
        - protocol: TCP
          port: 6379
EOF

student@master:~$ kubectl create -f facade-backend.np.yml
```

![Image description](img/v1-3.png)

**Allow client to access facade**
Allow client to speak to backend but only the needed port.

#### [client-facade.np.yml](./client-facade.np.yml)
```
student@master:~$ cat<<EOF> client-facade.np.yml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: backend
  name: facade-policy
spec:
  podSelector:
    matchLabels:
      app: facade
  policyTypes:
  - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              requires: backend
      ports:
        - protocol: TCP
          port: 80
EOF

student@master:~$ kubectl create -f client-facade.deploy.yml
```
![Image description](img/v1-4.png)


So now all componenents have the needed communications and our application set up is done as estipulated: managent ui to all componenetsm cliento to facade and facade to backend

![Image description](img/appsv1.png)

## APPLICATION VERSION 2



Now our aplication is added a payment service with two new deployments queue and payment, which would need to be configure in order to accept ingress an egress traffic.
If you remember for the previous exercise we have creates the new pci and infraestructure ns and apply restricted network policies to them.

![Image description](img/appsv2.png)

3 - Deployment of the full APPLICATION

Deploy the new components for the application a the new version of the management UI (namespaces where creates before).

**QUEUE**

#### [queue.deploy.yml](./queue.deploy.yml)
```
student@master:~$ cat<<EOF> queue.deploy.yml
apiVersion: v1
kind: Service
metadata:
  name: queue 
  namespace: infrastructure
spec:
  ports:
  - port: 5000
    targetPort: 5000 
  selector:
    app: queue 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: queue 
  namespace: infrastructure
spec:
  replicas: 1
  selector:
    matchLabels:
      app: queue
  template:
    metadata:
      labels:
        app: queue 
    spec:
      containers:
      - name: queue 
        image: dockersamples/star-probe
        imagePullPolicy: Always
        command:
        - probe
        - --http-port=5000
        - --urls=http://facade.backend:80/status,http://payments.pci:6380/status,http://backend.backend:6379/status,http://client.frontend:9000/status,http://queue.infrastructure:5000/status
        ports:
        - containerPort: 5000
EOF

student@master:~$ kubectl create -f queue.deploy.yml

```

**PAYMENT**
#### [payment.deploy.yml](./payment.deploy.yml)
```
student@master:~$ cat<<EOF> payment.deploy.yml
apiVersion: v1
kind: Service
metadata:
  name: payments 
  namespace: pci
spec:
  ports:
  - port: 6380
    targetPort: 6380 
  selector:
    app: payments 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: payments 
  namespace: pci
spec:
  replicas: 1
  selector:
    matchLabels:
      app: payments
  template:
    metadata:
      labels:
        app: payments 
    spec:
      containers:
      - name: payments 
        image: dockersamples/star-probe
        imagePullPolicy: Always
        command:
        - probe
        - --http-port=6380
        - --urls=http://facade.backend:80/status,http://payments.pci:6380/status,http://backend.backend:6379/status,http://client.frontend:9000/status,http://queue.infrastructure:5000/status
        ports:
        - containerPort: 6380
EOF

student@master:~$ kubectl create -f payment.deploy.yml
```
**Deploy v2 from management UI**
#### [managementv2.deploy.yml](./managementv2.deploy.yml)
```
First we must delete previous Deployment version.

student@master:~$ kubectl delete deploy management-ui -n management-ui
```

```
student@master:~$ cat<<EOF> managementv2.deploy.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: management-ui 
  namespace: management-ui 
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web-ui
  template:
    metadata:
      labels:
        role: management-ui 
        app: web-ui
    spec:
      containers:
      - name: management-ui 
        image: dockersamples/star-collect:v2
        imagePullPolicy: Always
        ports:
        - containerPort: 9001
EOF

student@master:~$ kubectl apply -f managementv2.deploy.yml
```
We have same communications as in V1 but with the new components, now we are going to need to apply the necessary network policies for payment and queue to allow egress and ingress communications. 

![Image description](img/v2-1.png)

### Appliying network policies

This policies is form facade-queue

#### [facade-queue.np.yml](./facade-queue.np.yml)
```
student@master:~$ cat<<EOF> facade-queue.np.yml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  namespace: pci
  name: default-deny-egress
spec:
  podSelector: {}
  policyTypes:
  - Egress
  egress:
  # allow DNS resolution
  - ports:
    - port: 53
      protocol: UDP
    - port: 53
      protocol: TCP
---
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  namespace: infrastructure
  name: queue-access-policy
spec:
  podSelector:
    matchLabels:
      app: queue
  policyTypes:
  - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              requires: queue
      ports:
        - protocol: TCP
          port: 5000
EOF

student@master:~$ kubectl create -f facade-queue.np.yml
```

![Image description](img/v2-2.png)

#### [pay-queue.np.yml](./pay-queue.np.yml)
```
student@master:~$ cat<<EOF> pay-queue.np.yml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  namespace: pci
  name: payments-policy
spec:
  podSelector:
    matchLabels:
      app: payments
  policyTypes:
  - Egress
  egress:
    - to:
        - namespaceSelector:
            matchLabels:
              role: infrastructure
EOF

student@master:~$ kubectl create -f pay-queue.np.yml
```
![Image description](img/v2-3.png)

Delete all the app namespaces:

```
student@master:~$ kubectl delete ns frontend
student@master:~$ kubectl delete ns backend
student@master:~$ kubectl delete ns pci
student@master:~$ kubectl delete ns infrastructure
student@master:~$ kubectl delete ns management-ui
```




---

### What we have learned in these labs?.
