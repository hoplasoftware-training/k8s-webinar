# Chapter 7
In these labs we will cover all the topics learned on Chapter7.
- We will cover the use of kubeconfig files and review concepts as clusters,users and context 

- We will review namespaces usage and resource quota of this namespaces.

- We will learn to configure roles. Rolesbindings as well as ClusterRoles and ClusterRoleBindings.

- We will have some labs impleneting diferent security context at pod deployment level as well as cluster-wide
- We will configure our deployments to pull an image from a private repository.

---

## Lab1 - Using __Kubeconfig__ to configure different Kubernetes __Context__.

 In this lab we are going to create an user in our cluster in order for our students to configure a new context a connecto to a different cluster.

1 - Create user in our cluster and distribute credentials. We use openssl to create a .crs and a .key for the "user-new" user.

```
student@master:~$ openssl req -new -newkey rsa:4096 -nodes -keyout user-new.key -out user-new.csr -subj "/CN=user-new"
```

With __CSR__ (Certificate Signing Request) data we create a __CertificateSigningRequest__ resource [csr.yml](./csr.yml) with the following content (we wrote ___cat___'s snippet to help you copy&paste quick editing):
```
student@master:~$ cat <<EOF>  csr.yml
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: user-new-access
spec:
  groups:
  - system:authenticated
  request: $(cat user-new.csr | base64 | tr -d '\n')
  usages:
  - client auth
EOF
```

Then we create this resource:
```
kubectl create -f csr.yml
```

>NOTE: We can review this resource using ___kubectl get csr___.

Notice that __CertificateSigningRequest__ resource is named "user-new-access". Now we need to approve this new user's certificate request:
```
student@master:~$ kubectl get csr
NAME              AGE   REQUESTOR                 CONDITION
csr-5d8d2         21m   system:bootstrap:ob8ak6   Approved,Issued
csr-dcrm9         15m   system:bootstrap:ob8ak6   Approved,Issued
user-new-access   13s   kubernetes-admin          Pending
```

>NOTE: Notice that requestor is "kubernetes-admin".

To accept this __CertificateSigningRequest__, we use ___kubectl certificate approve___:
```
student@master:~$ kubectl certificate approve user-new-access
certificatesigningrequest.certificates.k8s.io/user-new-access approved
```

2 - We are going to get the needed credentials and configure our new context using the new created certificate for "user-new-access" __CertificateSigningRequest__.

To get user's certificate we use kubectl get csr user-new-access command-line output:
```
student@master:~$ kubectl get csr user-new-access -o jsonpath='{.status.certificate}' | base64 --decode > user-new-access.crt
```

Now we have user's certificate.
```
student@master:~$ cat user-new-access.crt
-----BEGIN CERTIFICATE-----
MIID1zCCAr+gAwIBAgIQEfpIHxNOLlfyd2fTNusQdjANBgkqhkiG9w0BAQsFADAV
MRMwEQYDVQQDEwprdWJlcm5ldGVzMB4XDTIwMDYwNDEwMTYwMVoXDTIxMDYwNDEw
MTYwMVowEzERMA8GA1UEAxMIdXNlci1uZXcwggIiMA0GCSqGSIb3DQEBAQUAA4IC
DwAwggIKAoICAQCvC3uAp6enHqdBXtixka81hoObgyf/DGWt/JspgiMgh1MDtPy8
NtRcqaWd2/iFNyEK8C2IepueFSJvUk3YbHzwKVyv12T7gETUvLo6KrFyZsO4AQMm
4Esp+4wkHmZSYYeB5UGt7JxMX7iaNvTJev9nFYEfHfGYcF/VuBQYUdBAkEXjPdaa
fDi0Xs1Ew7dyGIBW4cUnFt6TDRc0yb25c1LV9F0AKqg5NZC56JGsNNM6IacLzKjq
AAx8RcKxNJSwH0GxJrEdWnrMNEyRfVOH3i26iVH65x9fIXUK93Gr4w0UBcevnyg6
Fp2lzb6lZXKQsQor0hsKMy6RzXhnvvAmsSv20SskpoUvBDRJyA4cbH0B5LuXIRbr
4Wvaxe1HuGtK3iBzcCDPoB6BrkbCTR6UZjHEgC6Ka+hqtyV/DVvV9f7xtXVT9tT7
J4aGE7NLLa3Q6c/uSU+Wevuf1eH4hwwTKR7r8+dz9z5kQI3QFMhM95JE61q9FdQl
uYOQhONB01QBslTpQIdVvxm2JaLbZBGp/PEan6qa6ViZ9iCAizNkWPBPEsf4ogVH
BgIdju56xzyS1esVwnJoSYGdWFZ3eD61xIFeIHb22ytEELInwIF7UXi/OcdkLwsO
sxwd1xl3WlZGdNSkVotaGU6vrj2Yad2wz7jjlNIXtBqSgnLum5qeFc4h3wIDAQAB
oyUwIzATBgNVHSUEDDAKBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMA0GCSqGSIb3
DQEBCwUAA4IBAQCJCXWhLkdXSHQjOSr3F86KgLipHCh2SMtUAVK3O5NSng3SSz/6
KLoTv23C2gO13X9nXLNj8NBeofC9ysOPwHAz3tflJdUxA+5vtdE5N6FSUjTcjshS
Oq6YPxB8GzbnojRKSMugIe+6CFejcx6tEW1O2TKDlXnhAsdHWUoIinrSKMOKEjjY
DpgiWr7AfmQ7/k/bpZvIjy3JEu/NbLms9caT10A8NVwFTtYK1XHYTr44MNIjOv/P
a59Hnz+dC+4WhCw7wmMu4UCDbSQn/6Kq1oRlGuDVEnHBeSa5po57bsEipw6mjn8Z
qnvXl64ULeeKQkic83Y8fQDLdjdjfU2vz9gD
-----END CERTIFICATE-----
```

We will pass this created certificate ("user-new-access.crt") and the key created using openssl ("user-new.key)" to our "new-user" user.

> NOTE: Make sure that kubernetes config directory has the same permissions as kubernetes config file

Now we prepare a new __Kubeconfig__ entry. We will use our master IP address for these labs. If we are using internal client node, we will use ___hostname -I___ to obtain valid IP addresses. __In our case, master IP address is 10.10.10.11. Ensure you use your master's IP address.__

First, we set a new cluster named "new-cluster" with "https://10.10.10.11:6443" as server.__In our case, master IP address is 10.10.10.11. Ensure you use your master's IP address.__
```
student@master:~$ kubectl config  set-cluster new-cluster --server=https://10.10.10.11:6443 --insecure-skip-tls-verify=true
Cluster "new-cluster" set.
```

Now we set new credentials for "user-new" using the created certificate and his key.
```
student@master:~$ kubectl config  set-credentials user-new --client-certificate=./user-new-access.crt --client-key=./user-new.key
User "user-new" set.
```

Finally we set "user-new" and "new-cluster" as the __User__ and __Cluster__ associated for "new-context" __Context__. Notice that we also specified an specific __Namespace__.
```
student@master:~$ kubectl config set-context new-context --cluster=new-cluster --namespace=ch7-dev --user=user-new
Context "new-context" created.
```

"ch7-dev" namespace isn't created yet. Therefore we create this new __Namespace__. Using [namespace-dev.yml](./namespace-dev.yml) (notice that we added __cat__ for easy creation):
```
student@master:~$ cat <<EOF> namespace-dev.yml
apiVersion: v1
kind: Namespace
metadata:
  name: ch7-dev
EOF
```

To create this __Namespace__ we will execute:
```
student@master:~$ kubectl create -f namespace-dev.yml
namespace/ch7-dev created
```

Now we are ready to use "new-context" new context.
```
student@master:~$ kubectl config get-contexts
CURRENT   NAME                          CLUSTER       AUTHINFO           NAMESPACE
*         kubernetes-admin@kubernetes   kubernetes    kubernetes-admin   
          new-context                   new-cluster   user-new           ch7-dev
```

To change to "new-context" we will use ___kubectl config  use-context___:
```
student@master:~$ kubectl config  use-context new-context 
Switched to context "new-context".
```

Now we list all __Pods__ with this user. 
```
student@master:~$ kubectl get pods
Error from server (Forbidden): pods is forbidden: User "user-new" cannot list resource "pods" in API group "" in the namespace "ch7-dev"
``` 

This is due to the fact that "user-new" has no __Role__ associated yet.

We go back to previous context ("kubernetes-admin@kubernetes").
```
student@master:~$ kubectl config  use-context kubernetes-admin@kubernetes
Switched to context "kubernetes-admin@kubernetes".
```
And now we can list __Pods__, event ther isn't any running on "default" __Namespace__:
```
student@master:~$ kubectl get pods
No resources found in default namespace.
```

This lab show us of to create a user to access our cluster and prepare the appropriate __Context__ using created credentials.

---

## Lab2 - Basic Namespaces

In this lab we are going to use and understand the basics of __Mamespace__ resources.


1 - First we will create two __Namespace__ resources "ch7-dev" (already created) and "ch7-prod".
```
student@master:~$ kubectl create namespace ch7-prod
namespace/ch7-prod created
```

Both __Namepsace__ resources are now created.
```
student@master:~$ kubectl get namespaces
NAME              STATUS   AGE
ch7-dev           Active   19m
ch7-prod          Active   12s
default           Active   83m
kube-node-lease   Active   83m
kube-public       Active   83m
kube-system       Active   83m
```

We will now create some __Deployment__ and __Service__ resources in order to make petitions between each other.

For "ch7-dev", we will create "firstdev" __Service__ and "firstdev" __Deployment__. Use your favorite editor to create [firstdev.full-deploy.yml](./firstdev.full-deploy.yml) with the following content (notice that we added ___cat___ to help you create the file):
```
student@master:~$ cat <<EOF> firstdev.full-deploy.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: firstdev
  name: firstdev
  namespace: ch7-dev
spec:
  ports:
  - name: "3000"
    port: 3000
    protocol: TCP
    targetPort: 3000
  selector:
    type: first
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: firstdev
  labels:
   app: firstdev
   type: first
  namespace: ch7-dev
spec:
  replicas: 2
  selector:
    matchLabels:
      type: first
  template:
    metadata:
      name: colors-pod
      labels:
        type: first
    spec:
      containers:
      - name: colors-pod
        image: codegazers/colors:1.16
        ports:
        - name: web
          containerPort: 3000
        resources:
          requests:
            memory: "50Mi"
            cpu: "0.1"
          limits: 
            memory: "80Mi"
            cpu: "0.5"
        env:
        - name: COLOR
          value: red
EOF
```

And we will just simply create "firstdev" __Service__ and "firstdev" __Deployment__ resources by executing ___kubectl create___:
```
student@master:~$ kubectl create -f firstdev.full-deploy.yml
service/firstdev created
deployment.apps/firstdev created
```

We will also create "seconddev" __Service__ and "seconddev" __Deployment__ on . Use your favorite editor to create [seconddev.full-deploy.yml](./seconddev.full-deploy.yml) with the following content (notice that we added ___cat___ to help you create the file):
```
student@master:~$ cat <<EOF> seconddev.full-deploy.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: seconddev
  name: seconddev
  namespace: ch7-dev
spec:
  ports:
  - name: "3000"
    port: 3000
    protocol: TCP
    targetPort: 3000
  selector:
    type: second
  type: ClusterIP

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: seconddev
  labels:
   app: seconddev
   type: second
  namespace: ch7-dev
spec:
  replicas: 1
  selector:
    matchLabels:
      type: second
  template:
    metadata:
      name: colors-pod
      labels:
        type: second
    spec:
      containers:
      - name: colors-pod
        image: codegazers/colors:1.16
        ports:
        - name: web
          containerPort: 3000
        resources:
          requests:
            memory: "50Mi"
            cpu: "0.1"
          limits: 
            memory: "80Mi"
            cpu: "1"
        env:
        - name: COLOR
          value: red
EOF
```

And we will just simply create "seconddev" __Service__ and "firstdev" __Deployment__ resources by executing ___kubectl create___:
```
student@master:~$ kubectl create -f seconddev.full-deploy.yml
service/seconddev created
deployment.apps/seconddev created
```

For "ch7-prod", we will create "prodonly" __Service__ and "prodonly" __Deployment__. Use your favorite editor to create [prodonly.full-deploy.yml](./prodonly.full-deploy.yml) with the following content (notice that we added ___cat___ to help you create the file):
```
student@master:~$ cat <<EOF> prodonly.full-deploy.yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: prodonly
  name: prodonly
  namespace: ch7-prod
spec:
  ports:
  - name: "3000"
    port: 3000
    protocol: TCP
    targetPort: 3000
  selector:
    type: only
  type: ClusterIP

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: prodonly
  labels:
    app: prodonly
    type: only
  namespace: ch7-prod
spec:
  replicas: 1
  selector:
    matchLabels:
      type: only
  template:
    metadata:
      name: colors-pod
      labels:
        type: only
    spec:
      containers:
      - name: colors-pod
        image: codegazers/colors:1.16
        ports:
        - name: web
          containerPort: 3000
        resources:
          requests:
            memory: "100Mi"
            cpu: "0.1"
          limits: 
            memory: "200Mi"
            cpu: "0.2"
        env:
        - name: COLOR
          value: white
EOF
```

And we will just simply create "prodonly" __Service__ and "prodonly" __Deployment__ resources by executing ___kubectl create___:
```
student@master:~$ kubectl create -f prodonly.full-deploy.yml
service/prodonly created
deployment.apps/prodonly created
```

2-  Check everything is on the desired placed and "Running".

On "ch7-dev" __Namespace__:
```
student@master:~$ kubectl get all -n ch7-dev
NAME                            READY   STATUS    RESTARTS   AGE
pod/firstdev-569f7fbf8f-6wpd8   1/1     Running   0          12m
pod/firstdev-569f7fbf8f-cvl8z   1/1     Running   0          12m
pod/seconddev-5d765f45b-f4lxx   1/1     Running   0          2m47s

NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/firstdev    ClusterIP   10.107.52.45    <none>        3000/TCP   12m
service/seconddev   ClusterIP   10.108.82.247   <none>        3000/TCP   2m48s

NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/firstdev    2/2     2            2           12m
deployment.apps/seconddev   1/1     1            1           2m48s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/firstdev-569f7fbf8f   2         2         2       12m
replicaset.apps/seconddev-5d765f45b   1         1         1       2m48s

student@master:~$ kubectl get endpoints firstdev -n ch7-dev
NAME       ENDPOINTS                                 AGE
firstdev   192.168.64.193:3000,192.168.89.193:3000   13m
```


On "ch7-prod" __Namespace__:
```
student@master:~$ kubectl get all -n ch7-prod
NAME                            READY   STATUS    RESTARTS   AGE
pod/prodonly-794f7885c8-9zjhb   1/1     Running   0          2m40s

NAME               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service/prodonly   ClusterIP   10.97.51.203   <none>        3000/TCP   2m40s

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/prodonly   1/1     1            1           2m40s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/prodonly-794f7885c8   1         1         1       2m40s


student@master:~$ kubectl get endpoints -n ch7-prod prodonly
NAME       ENDPOINTS             AGE
prodonly   192.168.64.194:3000   2m55s

```

3 - Now we will deploy a pod with ___curl___ command on "ch7-dev" __Namespace__ and try to access the services running on "ch7-dev" and "ch7-prod" namespaces.
We will use our favorite editor to create [curl.deploy.yml](./curl.deploy.yml) with the following content (notice that we added ___cat___ to help you create the file "on-fly"):
```
student@master:~$ cat <<EOF> curl.deploy.yml
apiVersion: v1
kind: Pod
metadata:
 name: curl-pod
 labels:
   name: curl
 namespace: ch7-dev
spec:
 containers:
 - name: curl-pod
   image:  yauritux/busybox-curl
   args:
    - sleep
    - "1000000"
EOF
```

Tehn we create "curl-pod" __Pod__:
```
student@master:~$ kubectl create -f curl.deploy.yml
pod/curl-pod created
```

Let's now execute some tests against services running on "ch7-dev"
```
student@master:~$ kubectl get svc -n ch7-dev
NAME        TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
firstdev    ClusterIP   10.107.52.45    <none>        3000/TCP   19m
seconddev   ClusterIP   10.108.82.247   <none>        3000/TCP   10m
```

Now we execute ___curl___ inside "curl-pod":
- Testing  "firstdev:3000/text" URL:
```
student@master:~$ kubectl exec -ti -n ch7-dev curl-pod curl firstdev:3000/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: firstdev-569f7fbf8f-cvl8z
CONTAINER_IP:  192.168.89.193
CLIENT_IP: ::ffff:192.168.64.195
CONTAINER_ARCH: linux
```

- Testing  "seconddev:3000/text" URL:
```
student@master:~$ kubectl exec -ti -n ch7-dev curl-pod curl  seconddev:3000/text
APP_VERSION: 1.20
COLOR: red
CONTAINER_NAME: seconddev-5d765f45b-f4lxx
CONTAINER_IP:  192.168.89.194
CLIENT_IP: ::ffff:192.168.64.195
CONTAINER_ARCH: linux
```

Now we execute the same tests against "ch7-prod" services:
```
student@master:~$ kubectl get svc -n ch7-prod
NAME       TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
prodonly   ClusterIP   10.97.51.203   <none>        3000/TCP   12m
```
- Testing  "prodonly:3000/text" URL:
```
student@master:~$ kubectl exec -ti -n ch7-dev curl-pod curl  prodonly:3000/text
curl: (6) Couldn't resolve host 'prodonly'
command terminated with exit code 6
```

__Resolution is internal within namespaces.__

We can test againg using full internal service's FQDN.
- Testing  "prodonly.ch7-prod.svc.cluster.local:3000/text" URL:
```
student@master:~$ kubectl exec -ti -n ch7-dev curl-pod curl  prodonly.ch7-prod.svc.cluster.local:3000/text
APP_VERSION: 1.20
COLOR: white
CONTAINER_NAME: prodonly-794f7885c8-9zjhb
CONTAINER_IP:  192.168.64.194
CLIENT_IP: ::ffff:192.168.64.195
CONTAINER_ARCH: linux
```

This works because all services's FQDN include __Namespace__ as DNS suffix. 


In this lab we learned how to isolate resource management between namespaces, but accesses to services and applications are permitted by default.

---

## Lab3 - Resource quotas applied to Namespaces 

In this lab we will create a resource quota on  previously created namespaces and understand the behaviour of this configurations.

1 -  Create [ch7-dev.resourcequota.yml](./ch7-dev.resourcequota.yml) ___ResourceQuota__ resource file using your favorite editor with the following content (notice that we added ___cat___ for easy deploying):
```
student@master:~$ cat <<EOF> ch7-dev.resourcequota.yml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: quota-dev
  namespace: ch7-dev
spec:
  hard:
    requests.cpu: "1"
    requests.memory: 1Gi
    limits.cpu: "4"
    limits.memory: 2Gi
    configmaps: "10"
    persistentvolumeclaims: "4"
    pods: "3"
    replicationcontrollers: "20"
    secrets: "10"
    services: "10" 
    services.loadbalancers: "2"
EOF
```

Then we just create ___ResourceQuota__ resource  using ____kubectl create___:
```
student@master:~$ kubectl create -f ch7-dev.resourcequota.yml
resourcequota/quota-dev created
```

We can review ___ResourceQuota__ resources now:
```
student@master:~$ kubectl get resourcequota --all-namespaces
NAMESPACE   NAME        CREATED AT
ch7-dev     quota-dev   2020-06-04T12:55:15Z
```

2 -  Undertand how this resource quotas works

If we take a look at actual resources in ch7-dev, we don't see any changes:
```
student@master:~$  kubectl get all -n ch7-dev
NAME                            READY   STATUS    RESTARTS   AGE
pod/curl-pod                    1/1     Running   0          76m
pod/firstdev-569f7fbf8f-6wpd8   1/1     Running   0          94m
pod/firstdev-569f7fbf8f-cvl8z   1/1     Running   0          94m
pod/seconddev-5d765f45b-f4lxx   1/1     Running   0          85m

NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/firstdev    ClusterIP   10.107.52.45    <none>        3000/TCP   94m
service/seconddev   ClusterIP   10.108.82.247   <none>        3000/TCP   85m

NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/firstdev    2/2     2            2           94m
deployment.apps/seconddev   1/1     1            1           85m

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/firstdev-569f7fbf8f   2         2         2       94m
replicaset.apps/seconddev-5d765f45b   1         1         1       85m
```

We can see that we applied a resource quota with a limit of 4 pods (max3), but all our pods are still up and running. This is due to the fact that __ResourcesQuota__ resources should be configured at the beginning, once we created the __Namespace__.  

__ResourcesQuota__ is not applied to already created resources.


Let's delete "firstdev" __Deployment__ and redeploy it again.

```
student@master:~$ kubectl delete -n ch7-dev deployment.apps/firstdev
deployment.apps "firstdev" deleted
```

>NOTE: We can always use ___kubectl delete -f <RESOURCE_YAML_FILE>___ to remove all the resources defined in the file, as we already did to create them using ___kubectl create -f <RESOURCE_YAML_FILE>___.

Let's create again "firstdev" __Deployment__ and __Service__.
```
student@master:~$ kubectl create -f firstdev.full-deploy.yml
deployment.apps/firstdev created
Error from server (AlreadyExists): error when creating "firstdev.full-deploy.yml": services "firstdev" already exists
```

We get this error because we just removed "firstdev" __Deployment__. "firstdev" __Service__ already existed. 

Let's review again all resources deployed on "ch7-dev" __Namespace__:
```
student@master:~$ kubectl get all -n ch7-dev
NAME                            READY   STATUS    RESTARTS   AGE
pod/curl-pod                    1/1     Running   0          89m
pod/firstdev-569f7fbf8f-6tz7d   1/1     Running   0          2m37s
pod/seconddev-5d765f45b-f4lxx   1/1     Running   0          98m

NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/firstdev    ClusterIP   10.107.52.45    <none>        3000/TCP   107m
service/seconddev   ClusterIP   10.108.82.247   <none>        3000/TCP   98m

NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/firstdev    1/2     1            1           2m37s
deployment.apps/seconddev   1/1     1            1           98m

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/firstdev-569f7fbf8f   2         1         1       2m37s
replicaset.apps/seconddev-5d765f45b   1         1         1       98m
student@master:~$ 
```

Notice that "deployment.apps/firstdev" only runs 1 instance although we defined 2 instances to be deployed.

Let's review "replicaset.apps/firstdev-569f7fbf8f" (this will change on your environment) to verify what is happening:
```
student@master:~$ kubectl describe -n ch7-dev replicaset.apps/firstdev-569f7fbf8f
Name:           firstdev-569f7fbf8f
Namespace:      ch7-dev
Selector:       pod-template-hash=569f7fbf8f,type=first
Labels:         pod-template-hash=569f7fbf8f
                type=first
Annotations:    deployment.kubernetes.io/desired-replicas: 2
                deployment.kubernetes.io/max-replicas: 3
                deployment.kubernetes.io/revision: 1
Controlled By:  Deployment/firstdev
Replicas:       1 current / 2 desired
Pods Status:    1 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  pod-template-hash=569f7fbf8f
           type=first
  Containers:
   colors-pod:
    Image:      codegazers/colors:1.16
    Port:       3000/TCP
    Host Port:  0/TCP
    Limits:
      cpu:     500m
      memory:  80Mi
    Requests:
      cpu:     100m
      memory:  50Mi
    Environment:
      COLOR:  red
    Mounts:   <none>
  Volumes:    <none>
Conditions:
  Type             Status  Reason
  ----             ------  ------
  ReplicaFailure   True    FailedCreate
Events:
  Type     Reason            Age                  From                   Message
  ----     ------            ----                 ----                   -------
  Normal   SuccessfulCreate  5m20s                replicaset-controller  Created pod: firstdev-569f7fbf8f-6tz7d
  Warning  FailedCreate      5m20s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-hwbm9" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m20s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-tcqgm" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m20s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-l674v" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m20s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-zdp2r" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m20s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-p8fvf" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m20s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-77w58" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m19s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-lrwzf" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m19s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-rw2j2" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      5m18s                replicaset-controller  Error creating: pods "firstdev-569f7fbf8f-5mtmc" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
  Warning  FailedCreate      25s (x8 over 5m18s)  replicaset-controller  (combined from similar events): Error creating: pods "firstdev-569f7fbf8f-gm47r" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
```

Last line shows following message:
```
Warning  FailedCreate      25s (x8 over 5m18s)  replicaset-controller  (combined from similar events): Error creating: pods "firstdev-569f7fbf8f-gm47r" is forbidden: exceeded quota: quota-dev, requested: pods=1, used: pods=3, limited: pods=3
```

Now we can review __Quota__ consumption:
```
student@master:~$ kubectl describe -n ch7-dev quota quota-dev
Name:                   quota-dev
Namespace:              ch7-dev
Resource                Used   Hard
--------                ----   ----
configmaps              0      10
limits.cpu              1500m  4
limits.memory           160Mi  2Gi
persistentvolumeclaims  0      4
pods                    3      3
replicationcontrollers  0      20
requests.cpu            200m   1
requests.memory         100Mi  1Gi
secrets                 1      10
services                2      10
services.loadbalancers  0      2
```

In this lab we learned how to implement quotas to prevent unnexpected consumptions.

---

## Lab4 - Roles and ClusterRoles within a namespace

In this lab we will learn how to use Kubernetes' RBAC to manage users' accesses.

1 - We will create __Role__ and __RoleBindig__ resources for a "user-new" __User__, created in the first lab. Remember that this user was unable to even list pods.

Let's create "role-view" __Role__ and "role-view-b" __RoleBinding__. Use your favorite editor to create [role-usernew.full-role.yml](./role-usernew.full-role.yml) with the following content (notice that we added ___cat___ for easy creation):
```
student@master:~$ cat <<EOF> role-usernew.full-role.yml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-view
  namespace: ch7-dev
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  creationTimestamp: null
  name: role-view-b
  namespace: ch7-dev
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: role-view
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: user-new
EOF
```

We create both resources using ___kubectl create___:
```
student@master:~$ kubectl create -f role-usernew.full-role.yml
role.rbac.authorization.k8s.io/role-view created
rolebinding.rbac.authorization.k8s.io/role-view-b created
```

Now we can verify acceses using ___kubectl get pods ---as___ to impersonate into another user.
```
student@master:~$ kubectl get pods -n ch7-dev --as=user-new
NAME                        READY   STATUS    RESTARTS   AGE
curl-pod                    1/1     Running   0          108m
firstdev-569f7fbf8f-6tz7d   1/1     Running   0          22m
seconddev-5d765f45b-f4lxx   1/1     Running   0          117m
```

Notice that __RoleBinding__ resource is applied for "ch7-dev" __Namespace__.
We can test this behavior requesting pods running on "ch7-prod".
```
student@master:~$ kubectl get pods -n ch7-prod --as=user-new
Error from server (Forbidden): pods is forbidden: User "user-new" cannot list resource "pods" in API group "" in the namespace "ch7-prod"
```

As expected, we get an error because "new-user" can only list pods on "ch7-dev" namespace.

Now we can access the cluster using "new-context" __Context__.
```
student@master:~$ kubectl config  use-context new-context 
Switched to context "new-context".
```

Let's verify now the pods running user's default __Namespace__, which is "ch7-dev" as we defined.
```
student@master:~$ kubectl get pods
NAME                        READY   STATUS    RESTARTS   AGE
curl-pod                    1/1     Running   0          114m
firstdev-569f7fbf8f-6tz7d   1/1     Running   0          27m
seconddev-5d765f45b-f4lxx   1/1     Running   0          123m
```

We can get back to previous context ("kubernetes-admin@kubernetes").
```
student@master:~$ kubectl config  use-context kubernetes-admin@kubernetes
Switched to context "kubernetes-admin@kubernetes".
```

We can retrieve __Role__ and __RoleBinding__ resources applied to "ch7-dev" __Namespace__:
```
student@master:~$ kubectl get -n ch7-dev  role,rolebinding
NAME                                       AGE
role.rbac.authorization.k8s.io/role-view   9m57s

NAME                                                AGE
rolebinding.rbac.authorization.k8s.io/role-view-b   9m57s
```

>NOTE: We can also apply this permissions using __ClusterRole__ and __ClusterRoleBinding__.
>```
>student@master:~$ cat <<EOF | kubectl apply -f -
>apiVersion: rbac.authorization.k8s.io/v1
>kind: ClusterRole
>metadata:
>  creationTimestamp: null
>  name: role-view
>rules:
>- apiGroups:
>  - ""
>  resources:
>  - pods
>  verbs:
>  - get
>  - list
>  - watch
>---
>apiVersion: rbac.authorization.k8s.io/v1beta1
>kind: RoleBinding
>metadata:
>  creationTimestamp: null
>  name: role-view-b
>  namespace: ch7-dev
>roleRef:
>  apiGroup: rbac.authorization.k8s.io
>  kind: ClusterRole
>  name: role-view
>subjects:
>- apiGroup: rbac.authorization.k8s.io
>  kind: User
>  name: user-new
>EOF
>```

In this lab we learned how to implement roles to users within namespaces.

---

## Lab5 - ClusterRoles  

In this lab we wil create role defined cluster-wide.

1 - Let's create "my-admin" __ClusterRole__ and "my-admin-binding" __ClusterRoleBinding__. Use your favorite editor to create [my-admin.full-clusterrole.yml](./my-admin.full-clusterrole.yml) with the following content (notice that we added ___cat___ for easy creation):
```
student@master:~$ cat <<EOF> my-admin.full-clusterrole.yml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  creationTimestamp: null
  name: my-admin
rules:
- apiGroups: ["", "apps", "extensions"]
  resources: ["pods"]
  verbs:
  - get
  - list
  - watch
  - create
  - delete
  - update
  - edit
  - patch
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  creationTimestamp: null
  name: my-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: my-admin
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: user-new
EOF
```

We apply this new resources file:
```
student@master:~$ kubectl create -f my-admin.full-clusterrole.yml
clusterrole.rbac.authorization.k8s.io/my-admin created
clusterrolebinding.rbac.authorization.k8s.io/my-admin created
```

Notice that this new __ClusterRole__ is applied at cluster-level. Therefore we should be abble to list pods in "ch7-prod".
```
student@master:~$ kubectl get pods -n ch7-prod --as=user-new
NAME                        READY   STATUS    RESTARTS   AGE
prodonly-794f7885c8-9zjhb   1/1     Running   0          140m
```

As expected, "user-new" user is now able to work with pods from all namespaces

---

## Lab6 - Using specific capabilities

We are going to use some capabilities in this lab in order to study how priviages should be granted on kubernetes.

1 - Let's create a __Pod__ resource, [root-busybox.deploy.yml](./root-busybox.deploy.yml), using your favorite editor, with the following content (notice that we added ___cat___ for easy creation):
```
student@master:~$ cat <<EOF> root-busybox.deploy.yml
apiVersion: v1
kind: Pod
metadata:
  name: busybox0
  labels:
    app: busybox0
  namespace: ch7-prod
spec:
  containers:
  - image: busybox
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox
EOF
```

We create this __Pod__:
```
student@master:~$ kubectl create -f root-busybox.deploy.yml
pod/busybox0 created
```

2 - Now we can test some commands:
First we can simply list __rootfs__:
```
student@master:~$ kubectl exec -ti busybox0  -n ch7-prod -- ls -l
total 36
drwxr-xr-x    2 root     root         12288 Jun  2 02:37 bin
drwxr-xr-x    5 root     root           360 Jun  4 14:03 dev
drwxr-xr-x    1 root     root          4096 Jun  4 14:03 etc
drwxr-xr-x    2 nobody   nogroup       4096 Jun  2 02:37 home
dr-xr-xr-x  137 root     root             0 Jun  4 14:03 proc
drwx------    2 root     root          4096 Jun  2 02:37 root
dr-xr-xr-x   13 root     root             0 Jun  4 14:03 sys
drwxrwxrwt    2 root     root          4096 Jun  2 02:37 tmp
drwxr-xr-x    3 root     root          4096 Jun  2 02:37 usr
drwxr-xr-x    1 root     root          4096 Jun  4 14:03 var
```

Now we can change __/var__ directory ownership:
```
student@master:~$ kubectl exec -ti busybox0 -n ch7-prod -- chown nobody /var
student@master:~$ kubectl exec -ti busybox0  -n ch7-prod -- ls -l
total 36
drwxr-xr-x    2 root     root         12288 Jun  2 02:37 bin
drwxr-xr-x    5 root     root           360 Jun  4 14:03 dev
drwxr-xr-x    1 root     root          4096 Jun  4 14:03 etc
drwxr-xr-x    2 nobody   nogroup       4096 Jun  2 02:37 home
dr-xr-xr-x  135 root     root             0 Jun  4 14:03 proc
drwx------    2 root     root          4096 Jun  2 02:37 root
dr-xr-xr-x   13 root     root             0 Jun  4 14:03 sys
drwxrwxrwt    2 root     root          4096 Jun  2 02:37 tmp
drwxr-xr-x    3 root     root          4096 Jun  2 02:37 usr
drwxr-xr-x    1 nobody   root          4096 Jun  4 14:03 var
```

Therefore we changed __/var__ filesystem.

3 - Let's create another __Pod__ resource. We create [dropcap-busybox.deploy.yml](./dropcap-busybox.deploy.yml) file using your favorite editor with the following content (notice that we added ___cat___ for easy creation):
```
student@master:~$ cat <<EOF> dropcap-busybox.deploy.yml
apiVersion: v1
kind: Pod
metadata:
  name: busybox1
  labels:
    app: busybox1
  namespace: ch7-prod
spec:
  containers:
  - image: busybox
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox
    securityContext:
      capabilities:
        drop:
          - ALL
EOF
```

We create this __Pod__:
```
student@master:~$ kubectl create -f dropcap-busybox.deploy.yml
pod/busybox1 created
```

Now we can test again:
```
student@master:~$ kubectl exec -ti busybox1 -n ch7-prod -- ls -l
total 36
drwxr-xr-x    2 root     root         12288 Jun  2 02:37 bin
drwxr-xr-x    5 root     root           360 Jun  4 14:13 dev
drwxr-xr-x    1 root     root          4096 Jun  4 14:13 etc
drwxr-xr-x    2 nobody   nogroup       4096 Jun  2 02:37 home
dr-xr-xr-x  136 root     root             0 Jun  4 14:13 proc
drwx------    2 root     root          4096 Jun  2 02:37 root
dr-xr-xr-x   13 root     root             0 Jun  4 14:13 sys
drwxrwxrwt    2 root     root          4096 Jun  2 02:37 tmp
drwxr-xr-x    3 root     root          4096 Jun  2 02:37 usr
drwxr-xr-x    1 root     root          4096 Jun  4 14:13 var
```

We will test again changing __/var__ ownership.
```
student@master:~$ kubectl exec -ti busybox1 -n ch7-prod -- chown nobody /var
chown: /var: Operation not permitted
command terminated with exit code 1
```

We get an error because all capabilities were dropped on "busybox1" __Pod__.

4 - Let's create now another __Pod__ adding __CHOWN__ capability. We will create [addcap-busybox.deploy.yml](./addcap-busybox.deploy.yml) file with the following content:
```
student@master:~$ cat <<EOF> addcap-busybox.deploy.yml
apiVersion: v1
kind: Pod
metadata:
  name: busybox2
  labels:
    app: busybox2
  namespace: ch7-prod
spec:
  containers:
  - image: busybox
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox
    securityContext:
      capabilities:
        drop:
          - ALL
        add:
          - CHOWN
EOF
```

We create this new __Pod__:
```
student@master:~$ kubectl create -f addcap-busybox.deploy.yml
pod/busybox2 created
```

We will test again changing __/var__ ownership.
```
student@master:~$ kubectl exec -ti busybox2 -n ch7-prod -- ls -l
total 36
drwxr-xr-x    2 root     root         12288 Jun  2 02:37 bin
drwxr-xr-x    5 root     root           360 Jun  4 14:18 dev
drwxr-xr-x    1 root     root          4096 Jun  4 14:18 etc
drwxr-xr-x    2 nobody   nogroup       4096 Jun  2 02:37 home
dr-xr-xr-x  141 root     root             0 Jun  4 14:18 proc
drwx------    2 root     root          4096 Jun  2 02:37 root
dr-xr-xr-x   13 root     root             0 Jun  4 14:18 sys
drwxrwxrwt    2 root     root          4096 Jun  2 02:37 tmp
drwxr-xr-x    3 root     root          4096 Jun  2 02:37 usr
drwxr-xr-x    1 root     root          4096 Jun  4 14:18 var
student@master:~$ kubectl exec -ti busybox2 -n ch7-prod -- chown nobody /var
student@master:~$ kubectl exec -ti busybox2 -n ch7-prod -- ls -l
total 36
drwxr-xr-x    2 root     root         12288 Jun  2 02:37 bin
drwxr-xr-x    5 root     root           360 Jun  4 14:18 dev
drwxr-xr-x    1 root     root          4096 Jun  4 14:18 etc
drwxr-xr-x    2 nobody   nogroup       4096 Jun  2 02:37 home
dr-xr-xr-x  141 root     root             0 Jun  4 14:18 proc
drwx------    2 root     root          4096 Jun  2 02:37 root
dr-xr-xr-x   13 root     root             0 Jun  4 14:18 sys
drwxrwxrwt    2 root     root          4096 Jun  2 02:37 tmp
drwxr-xr-x    3 root     root          4096 Jun  2 02:37 usr
drwxr-xr-x    1 nobody   root          4096 Jun  4 14:18 var
```

It worked as expected, adding __CHOWN__ capability.

In this lab we learned how to add and remove capabilities to __Pods__'s processs.

---

## Lab7 - PodSecurityPolicies

In this lab we will learn how to implement __PodSecurityPolicy__ resources.

1 - Let's create a __PodSecurityPolicy__ resource. We will create [mustrunasnonroot.yaml](./mustrunasnonroot.yml) using your favorite editor, with the following content (notice that we added ___cat___ for easy creation):
```
student@master:~$ cat <<-EOF> mustrunasnonroot.yml
apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:  
  name: psp-mustrunasnonroot
spec:
  allowPrivilegeEscalation: false
  allowedHostPaths:
  - pathPrefix: /dev/null
    readOnly: true
  fsGroup:
    rule: RunAsAny
  hostPorts:
  - max: 65535
    min: 0
  runAsUser:
    rule: 'MustRunAsNonRoot'
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  volumes:
  - '*'
EOF
```

We create this new resource:
```
student@master:~$  kubectl create -f mustrunasnonroot.yml  
podsecuritypolicy.policy/psp-mustrunasnonroot created
```

Now we can verify all PodSecurityPolicies applied in all namespaces:
```
student@master:~$ kubectl get PodSecurityPolicies --all-namespaces
NAME                   PRIV    CAPS   SELINUX    RUNASUSER          FSGROUP    SUPGROUP   READONLYROOTFS   VOLUMES
psp-mustrunasnonroot   false          RunAsAny   MustRunAsNonRoot   RunAsAny   RunAsAny   false            *
```

2 - We will add __PodSecurityPolicy__ in __--enable-admission-plugins__ line in __/etc/kubernetes/manifests/kube-apiserver.yaml__. Always make a backup and then modify this file and ensure __--enable-admission-plugins__  looks like this one:
```
    - --enable-admission-plugins=NodeRestriction,PodSecurityPolicy
```

We will wait until APIServer is restarted:
```
student@master:~$ docker ps |grep -i api
fc517ce2f2d5        c5c678ed2546              "kube-apiserver --ad…"   41 seconds ago      Up 40 seconds                           k8s_kube-apiserver_kube-apiserver-local-master_kube-system_5b45014978ce629cad55a13459929c0e_0
506ec379d8c1        k8s.gcr.io/pause:3.1      "/pause"                 42 seconds ago      Up 41 seconds                           k8s_POD_kube-apiserver-local-master_kube-system_5b45014978ce629cad55a13459929c0e_0
student@master:~$ docker ps |grep -i apiserver
fc517ce2f2d5        c5c678ed2546              "kube-apiserver --ad…"   45 seconds ago      Up 44 seconds                           k8s_kube-apiserver_kube-apiserver-local-master_kube-system_5b45014978ce629cad55a13459929c0e_0
506ec379d8c1        k8s.gcr.io/pause:3.1      "/pause"                 46 seconds ago      Up 45 seconds                           k8s_POD_kube-apiserver-local-master_kube-system_5b45014978ce629cad55a13459929c0e_0
```

3 - Now we can create a new __Pod__ with Nginx. We will use [root-nginx.pod.yml  ](./root-nginx.pod.yml) with the following content:
```
student@master:~$ cat <<EOF> root-nginx.pod.yml
apiVersion: v1
kind: Pod
metadata:
 name: nginx-as-root
 labels:
   lab: nginx-as-root
 namespace: ch7-prod
spec:
 containers:
 - name: nginx-as-root
   image: nginx:alpine
EOF
```

Let's see what happens now:

```
student@master:~$ kubectl create -f root-nginx.pod.yml  
pod/nginx-as-root created
```

But container can not be created.
```
student@master:~$ kubectl get pod -n ch7-prod
NAME                        READY   STATUS                       RESTARTS   AGE
busybox0                    1/1     Running                      0          57m
busybox1                    1/1     Running                      0          47m
busybox2                    1/1     Running                      0          42m
nginx-as-root               0/1     CreateContainerConfigError   0          22s
prodonly-794f7885c8-9zjhb   1/1     Running                      0          3h27m
```

Let's review what happened:
```
student@master:~$ kubectl describe pod -n ch7-prod nginx-as-root 
Name:         nginx-as-root
Namespace:    ch7-prod
Priority:     0
Node:         local-worker2/10.10.10.13
Start Time:   Thu, 04 Jun 2020 17:00:23 +0200
Labels:       lab=nginx-as-root
Annotations:  cni.projectcalico.org/podIP: 192.168.89.197/32
              kubernetes.io/psp: psp-mustrunasnonroot
Status:       Pending
IP:           192.168.89.197
IPs:
  IP:  192.168.89.197
Containers:
  nginx-as-root:
    Container ID:   
    Image:          nginx:alpine
    Image ID:       
    Port:           <none>
    Host Port:      <none>
    State:          Waiting
      Reason:       CreateContainerConfigError
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-ds7k9 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  default-token-ds7k9:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-ds7k9
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason     Age               From                    Message
  ----     ------     ----              ----                    -------
  Normal   Scheduled  <unknown>         default-scheduler       Successfully assigned ch7-prod/nginx-as-root to local-worker2
  Normal   Pulling    89s               kubelet, local-worker2  Pulling image "nginx:alpine"
  Normal   Pulled     80s               kubelet, local-worker2  Successfully pulled image "nginx:alpine"
  Warning  Failed     1s (x8 over 80s)  kubelet, local-worker2  Error: container has runAsNonRoot and image will run as root
  Normal   Pulled     1s (x7 over 79s)  kubelet, local-worker2  Container image "nginx:alpine" already present on machine
```

We get this message:
```
Warning  Failed     1s (x8 over 80s)  kubelet, local-worker2  Error: container has runAsNonRoot and image will run as root
```

Therefore __SecurityPodPolicy__ does not allow using root user inside __Pod__.

4 - Let's run a non-root Nginx. We will use frjaraur/non-root-nginx:1.2, which is an image that can run Nginx as "nginx" user on port 1080. We don't need to use "root" user in this case. 

We will create now [non-root-nginx.pod.yml](./non-root-nginx.pod.yml) resource file with following content:
```
student@master:~$ cat <<EOF> non-root-nginx.pod.yaml
apiVersion: v1
kind: Pod
metadata:
 name: nginx-as-nonroot
 labels:
   lab: nginx-as-root
spec:
 containers:
 - name: nginx-as-nonroot
   image: frjaraur/non-root-nginx:1.2
   imagePullPolicy: Always
 securityContext:
   runAsUser: 10001
EOF
```

Then we create this "nginx-as-nonroot" __Pod__:
```
student@master:~$ kubectl create -n ch7-prod -f non-root-nginx.pod.yaml
pod/nginx-as-nonroot created
```

Let's review the execution of this new __Pod__:
```
student@master:~$ kubectl get pod -n ch7-prod
NAME                        READY   STATUS                       RESTARTS   AGE
nginx-as-nonroot            1/1     Running                      0          20s
nginx-as-root               0/1     CreateContainerConfigError   0          31m
```

And now we can check port 1080:
```
student@master:~$ kubectl get pod nginx-as-nonroot -n ch7-prod -o wide
NAME               READY   STATUS    RESTARTS   AGE    IP               NODE            NOMINATED NODE   READINESS GATES
nginx-as-nonroot   1/1     Running   0          106s   192.168.64.198   local-worker1   <none>           <none>


student@master:~$ curl 192.168.64.198:1080
It worked!!
```
It works as expecting because it uses a port below 1024 and Nginx doesn't not require root user with the appropriate configuration.


>NOTE: Most Kubernetes pods are not created directly by users. Instead, they are typically created indirectly as part of a Deployment, ReplicaSet, or other templated controller via the controller manager. Granting the controller access to the policy would grant access for all pods created by that controller, so the preferred method for authorizing policies is to grant access to the pod’s service account.

Remove all your root and non-root Nginx __Pods__:
```
student@master:~$ kubectl -n ch7-prod  delete -f non-root-nginx.pod.yaml 
pod "nginx-as-nonroot" deleted

student@master:~$ kubectl -n ch7-prod delete -f root-nginx.pod.yaml 
pod "nginx-as-nonroot" deleted
```


5 - What would have happened if we use "user-new". Let's check if this user can run non-root __Pods__.

First we change our context to "new-context":
```
student@master:~$ kubectl config  get-contexts
CURRENT   NAME                          CLUSTER       AUTHINFO           NAMESPACE
*         kubernetes-admin@kubernetes   kubernetes    kubernetes-admin   
          new-context                   new-cluster   user-new           ch7-dev


student@master:~$ kubectl config  use-context new-context
Switched to context "new-context".
```

Now as "new-user", let's try to execute non-root Nginx __Pod__:
```
student@master:~$ kubectl create -f non-root-nginx.pod.yaml 
Error from server (Forbidden): error when creating "non-root-nginx.pod.yaml": pods "nginx-as-nonroot" is forbidden: unable to validate against any pod security policy: []
```

We get an error because this user has no access to __PodSecurityPolicies__.

To fix this situation we will create a __ClusterRole__ and __ClusterRoleBinding__ to allow  "user-new" to use __PodSecurityPolicies__ resources.


Use your favorite editor to create [role-non-root.yml](./role-non-root.yml) with the following content (we wrote cat's snippet to help you copy&paste quick editing):
```
student@master:~$ cat <<-EOF> role-non-root.yml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: role-mustrunasnonroot
rules:
- apiGroups:
  - policy
  resourceNames:
  - psp-mustrunasnonroot
  resources:
  - podsecuritypolicies
  verbs:
  - use
- apiGroups: [""]
  resources: ["pods"]
  verbs:
  - get
  - list
  - watch
  - create
  - delete
  - update
  - edit
  - patch
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: rb-mustrunasnonroot
  namespace: ch7-prod
roleRef:
  kind: ClusterRole
  name: role-mustrunasnonroot
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: User
  name: user-new
  #namespace: ch7-dev
EOF
```

Then we create these resources using "kubernetes-admin@kubernetes" __Context__:
```
student@master:~$ kubectl config  use-context kubernetes-admin@kubernetes
Switched to context "kubernetes-admin@kubernetes".

student@master:~$ kubectl create -f role-non-root.yml
clusterrole.rbac.authorization.k8s.io/role-mustrunasnonroot created
rolebinding.rbac.authorization.k8s.io/rb-mustrunasnonroot created
```


We check "user-new" access:
```
student@master:~$ kubectl auth can-i use podsecuritypolicy/psp-mustrunasnonroot --as user-new  -n ch7-prod
Warning: resource 'podsecuritypolicies' is not namespace scoped in group 'policy'
yes
```

Then we switch back to "new-context".
```
student@master:~$ kubectl config  use-context new-context
Switched to context "new-context".
```

And we try again:
```
student@master:~$ kubectl create -f non-root-nginx.pod.yaml -n ch7-prod
pod/nginx-as-nonroot created


student@master:~$ kubectl get pods -n ch7-prod
NAME                        READY   STATUS    RESTARTS   AGE
nginx-as-nonroot            1/1     Running   0          1m
```

6 - Rollback the changes we did in __/etc/kubernetes/manifests/kube-apiserver.yaml__ (remove __PodSecurityPolicy__ from the list of __admission-plugins__) and remove "psp-mustrunasnonroot" __PodSecurityPolicy__.

```
student@master:~$ kubectl config  use-context kubernetes-admin@kubernetes
Switched to context "kubernetes-admin@kubernetes".
student@master:~$ kubectl get psp
NAME                   PRIV    CAPS   SELINUX    RUNASUSER          FSGROUP    SUPGROUP   READONLYROOTFS   VOLUMES
psp-mustrunasnonroot   false          RunAsAny   MustRunAsNonRoot   RunAsAny   RunAsAny   false            *
student@master:~$ kubectl delete psp psp-mustrunasnonroot
podsecuritypolicy.policy "psp-mustrunasnonroot" deleted
```


This lab teach as how to implement __PodSecurityPolicy__ resources and integrate them to ensure security in our applications.


------


## Lab 8 - Using Registries for Image security

In this lab we will quickly review how to access image from privsate registries.

1 - Create a __Pod__ which uses a private repository. In this case we will create [private-image.pod.yml](./private-image.pod..yml) resource file.
```
student@master:~$ cat <<EOF> private-image.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-private
  labels:
    app: nginx-private
spec:
  containers:
  - image: hoplasoftware/private-nginx:1.0
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: nginx
EOF

student@master:~$ kubectl create -f private-image.pod.yml
```

Notice that we will use " image: hoplasoftware/private-nginx:1.0" image. This is a private image located in Docker.io registry.

2 - To access this image, we need to use registry's credentials. We will create a special secret of type __docker-registry__.
```
student@master:~$ kubectl create secret docker-registry regcred \
--docker-server=docker.io/Account/private-nginx \
--docker-username=<YOUR_USERNAME> \
--docker-password=<YOUR_SUPERSECRET_PASSWORD> --docker-email=<YOUR_USERNAME>@hoplasoftware.com
```

Now we can create "nginx-private" __Pod__ using define resource file.



```
student@master:~$ cat <<EOF> private-image.pod.yml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-private
  labels:
    app: nginx-private
spec:
  imagePullSecrets:
    name: regcred
  containers:
  - image: hoplasoftware/private-nginx:1.0
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: nginx
EOF

student@master:~$ kubectl create -f private-image.pod.yml
```


```
student@master:~$ kubectl create -f private-image.pod.yml
```
In this lab we learned how to use images from private registries using  __docker-registry__ secrets.

---

