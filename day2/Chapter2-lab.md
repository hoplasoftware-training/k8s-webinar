# Chapter 2
In these labs we will cover all the topics learned on Chapter2 and we also review following concepts:

- Kubernetes Components.

- Deep dive in Kubelet component executing some Pods.

- Kubernetes access using ___kubectl___.

- Get Kubernetes available API versions.

- Get resources information using ___kubectl explain___.

- Review which user is used to connect to your cluster.

---

## Lab1 - Reviewing Kubernetes Components

This lab will review the distribution of Kubernetes components within cluster nodes.

First we will connect to our master node. Each student has his own cluster, with one master and two wroker nodes. Master is tagged as master after student's name. For example, "stundet1" master node is "student1-master". Use SSH and the credentials ("hopla-learning.pem" and/or "hopla-learning.ppk")provided for "student" user.

1 - Once connected to master node, we will review cluster nodes:
```
vagrant@local-master:~$ kubectl get nodes
NAME            STATUS   ROLES    AGE   VERSION                                                                              
local-master    Ready    master   59m   v1.17.6                                                                              
local-worker1   Ready    <none>   52m   v1.17.6                                                                              
local-worker2   Ready    <none>   46m   v1.17.6                     
```

We can extend this information showing labels.
```
vagrant@local-master:~$ kubectl get nodes --show-labels
NAME            STATUS   ROLES    AGE   VERSION   LABELS
local-master    Ready    master   61m   v1.17.6   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=local-master,kubernetes.io/os=linux,node-role.kubernetes.io/master=
local-worker1   Ready    <none>   54m   v1.17.6   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=local-worker1,kubernetes.io/os=linux
local-worker2   Ready    <none>   48m   v1.17.6   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=local-worker2,kubernetes.io/os=linux
```

We notice that master has __node-role.kubernetes.io/master__ label. We can add role labels to nodes.
```
vagrant@local-master:~$ kubectl label node local-worker1 node-role.kubernetes.io/worker=
node/local-worker1 labeled

vagrant@local-master:~$ kubectl label node local-worker2 node-role.kubernetes.io/worker=
node/local-worker2 labeled

vagrant@local-master:~$ kubectl get nodes
NAME            STATUS   ROLES    AGE   VERSION
local-master    Ready    master   72m   v1.17.6
local-worker1   Ready    worker   65m   v1.17.6
local-worker2   Ready    worker   59m   v1.17.6
```

We learned how to add labels to nodes.

2 - Let's review master processes. Remember that we need to identify at least __Kube-APIServer__, __Kube-Scheduler__ and __Kube-Controller-Manager__.
```
vagrant@local-master:~$ ps -e|grep kube
 9200 ?        00:02:18 kube-apiserver
 9552 ?        00:01:46 kubelet
 9920 ?        00:00:01 kube-proxy
10654 ?        00:00:45 kube-controller
10691 ?        00:00:13 kube-scheduler
12348 ?        00:00:00 kube-controller
```

Now we can observe that all process will connect to __kube-apiserver__ using ___lsof -Pan -p <PID> -i___.
```
vagrant@local-master:~$ sudo lsof -Pan -p 9552 -i
COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
kubelet 9552 root   15u  IPv4  48714      0t0  TCP 127.0.0.1:34847 (LISTEN)
kubelet 9552 root   16u  IPv4  51359      0t0  TCP 10.10.10.11:46378->10.10.10.11:6443 (ESTABLISHED)
kubelet 9552 root   29u  IPv4  48774      0t0  TCP 127.0.0.1:10248 (LISTEN)
kubelet 9552 root   31u  IPv6  48779      0t0  TCP *:10250 (LISTEN)
vagrant@local-master:~$ sudo lsof -Pan -p 9920 -i
COMMAND    PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
kube-prox 9920 root    9u  IPv4  49583      0t0  TCP 10.10.10.11:46266->10.10.10.11:6443 (ESTABLISHED)
kube-prox 9920 root   12u  IPv4  49589      0t0  TCP 127.0.0.1:10249 (LISTEN)
kube-prox 9920 root   13u  IPv6  49596      0t0  TCP *:10256 (LISTEN)
vagrant@local-master:~$ sudo lsof -Pan -p 10654 -i
COMMAND     PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
kube-cont 10654 root    5u  IPv6  51861      0t0  TCP *:10252 (LISTEN)
kube-cont 10654 root    6u  IPv4  51867      0t0  TCP 127.0.0.1:10257 (LISTEN)
kube-cont 10654 root    7u  IPv4  52770      0t0  TCP 10.10.10.11:46402->10.10.10.11:6443 (ESTABLISHED)
kube-cont 10654 root    8u  IPv4  53165      0t0  TCP 10.10.10.11:46496->10.10.10.11:6443 (ESTABLISHED)
vagrant@local-master:~$ sudo lsof -Pan -p 10691 -i
COMMAND     PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
kube-sche 10691 root    5u  IPv6  52774      0t0  TCP *:10251 (LISTEN)
kube-sche 10691 root    6u  IPv4  52780      0t0  TCP 127.0.0.1:10259 (LISTEN)
kube-sche 10691 root    7u  IPv4  52781      0t0  TCP 10.10.10.11:46404->10.10.10.11:6443 (ESTABLISHED)
kube-sche 10691 root    8u  IPv4  52807      0t0  TCP 10.10.10.11:46408->10.10.10.11:6443 (ESTABLISHED)
vagrant@local-master:~$ sudo lsof -Pan -p 12348 -i
```

Notice that __master__ node has __kube-proxy__ and __kubelet__ processes running. This means that __master__ node is also available for scheduling. We will learn on "Scheduling" section why "master" node does not receive any workload.


---

## Lab2 - Revieweing ___kubectl___ command-line

In this lab we will understand how to use ___kubectl___ to interact with Kubernetes cluster.

1 - Review ___kubectl___ command-line help. ___Kubectl___ command should be in the same release of our cluster. Usually having newer version works, but is preferred to keep all components and client in the same release.
```
vagrant@local-master:~$ kubectl --help
kubectl controls the Kubernetes cluster manager.

 Find more information at: https://kubernetes.io/docs/reference/kubectl/overview/

Basic Commands (Beginner):
  create         Create a resource from a file or from stdin.
  expose         Take a replication controller, service, deployment or pod and expose it as a new Kubernetes Service
  run            Run a particular image on the cluster
  set            Set specific features on objects

Basic Commands (Intermediate):
  explain        Documentation of resources
  get            Display one or many resources
  edit           Edit a resource on the server
  delete         Delete resources by filenames, stdin, resources and names, or by resources and label selector

Deploy Commands:
  rollout        Manage the rollout of a resource
  scale          Set a new size for a Deployment, ReplicaSet or Replication Controller
  autoscale      Auto-scale a Deployment, ReplicaSet, or ReplicationController

Cluster Management Commands:
  certificate    Modify certificate resources.
  cluster-info   Display cluster info
  top            Display Resource (CPU/Memory/Storage) usage.
  cordon         Mark node as unschedulable
  uncordon       Mark node as schedulable
  drain          Drain node in preparation for maintenance
  taint          Update the taints on one or more nodes

Troubleshooting and Debugging Commands:
  describe       Show details of a specific resource or group of resources
  logs           Print the logs for a container in a pod
  attach         Attach to a running container
  exec           Execute a command in a container
  port-forward   Forward one or more local ports to a pod
  proxy          Run a proxy to the Kubernetes API server
  cp             Copy files and directories to and from containers.
  auth           Inspect authorization

Advanced Commands:
  diff           Diff live version against would-be applied version
  apply          Apply a configuration to a resource by filename or stdin
  patch          Update field(s) of a resource using strategic merge patch
  replace        Replace a resource by filename or stdin
  wait           Experimental: Wait for a specific condition on one or many resources.
  convert        Convert config files between different API versions
  kustomize      Build a kustomization target from a directory or a remote url.

Settings Commands:
  label          Update the labels on a resource
  annotate       Update the annotations on a resource
  completion     Output shell completion code for the specified shell (bash or zsh)

Other Commands:
  api-resources  Print the supported API resources on the server
  api-versions   Print the supported API versions on the server, in the form of "group/version"
  config         Modify kubeconfig files
  plugin         Provides utilities for interacting with plugins.
  version        Print the client and server version information

Usage:
  kubectl [flags] [options]

Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).
```

We can work with configurations using ___kubectl config___.

2 - First we will review which configuration we are using right now.
```
vagrant@local-master:~$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://10.10.10.11:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
```

__Kubectl__ interacts with Kube-APIServer and this validates and forward requests to internal APIs. Let's check deployed APIs using a __Pod__ in Lab3.


3 - In this step we will learn about ___kubectl explain___. This is a very important action for ___kubectl___ because it will help us to create any resource. Let's create a simple Pod using __Declarative mode__. In this case we need to create a file to create a simple Pod. We don't know even how to start.

We execute ___kubectl explain pod___ and review its output.
``` 
vagrant@local-master:~$ kubectl explain pod
KIND:     Pod
VERSION:  v1

DESCRIPTION:
     Pod is a collection of containers that can run on a host. This resource is
     created by clients and scheduled onto hosts.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     Standard object's metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     Specification of the desired behavior of the pod. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

   status       <Object>
     Most recently observed status of the pod. This data may not be up to date.
     Populated by the system. Read-only. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

```

This shows that our file should contain at first level the __apiVersion__, __kind__, __metadata__, __spec__ and __status__ (notice that this key is read-only and it is not defined by us).

Then we can create a simple file with these keys. Notice that __apiVersion__ and __kind__ are shown at the begining of command's output.

```
apiVersion: v1
kind: Pod
metadata:
spec:
``` 

Now we need to add __metadata__ and __spec__ data and both are defined as objects. Let's start with __metadata__. We can query again the Kube-APIServer using now __kubectl explain pod.metadata__:
```
vagrant@local-master:~$ kubectl explain pod.metadata
KIND:     Pod
VERSION:  v1

RESOURCE: metadata <Object>

DESCRIPTION:
     Standard object's metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

     ObjectMeta is metadata that all persisted resources must have, which
     includes all objects users must create.

FIELDS:
   annotations  <map[string]string>
     Annotations is an unstructured key value map stored with a resource that
     may be set by external tools to store and retrieve arbitrary metadata. They
     are not queryable and should be preserved when modifying objects. More
     info: http://kubernetes.io/docs/user-guide/annotations

   clusterName  <string>
     The name of the cluster which the object belongs to. This is used to
     distinguish resources with same name and namespace in different clusters.
     This field is not set anywhere right now and apiserver is going to ignore
     it if set in create or update request.

   creationTimestamp    <string>
     CreationTimestamp is a timestamp representing the server time when this
     object was created. It is not guaranteed to be set in happens-before order
     across separate operations. Clients may not set this value. It is
     represented in RFC3339 form and is in UTC. Populated by the system.
     Read-only. Null for lists. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   deletionGracePeriodSeconds   <integer>
     Number of seconds allowed for this object to gracefully terminate before it
     will be removed from the system. Only set when deletionTimestamp is also
     set. May only be shortened. Read-only.

   deletionTimestamp    <string>
     DeletionTimestamp is RFC 3339 date and time at which this resource will be
     deleted. This field is set by the server when a graceful deletion is
     requested by the user, and is not directly settable by a client. The
     resource is expected to be deleted (no longer visible from resource lists,
     and not reachable by name) after the time in this field, once the
     finalizers list is empty. As long as the finalizers list contains items,
     deletion is blocked. Once the deletionTimestamp is set, this value may not
     be unset or be set further into the future, although it may be shortened or
     the resource may be deleted prior to this time. For example, a user may
     request that a pod is deleted in 30 seconds. The Kubelet will react by
     sending a graceful termination signal to the containers in the pod. After
     that 30 seconds, the Kubelet will send a hard termination signal (SIGKILL)
     to the container and after cleanup, remove the pod from the API. In the
     presence of network partitions, this object may still exist after this
     timestamp, until an administrator or automated process can determine the
     resource is fully terminated. If not set, graceful deletion of the object
     has not been requested. Populated by the system when a graceful deletion is
     requested. Read-only. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   finalizers   <[]string>
     Must be empty before the object is deleted from the registry. Each entry is
     an identifier for the responsible component that will remove the entry from
     the list. If the deletionTimestamp of the object is non-nil, entries in
     this list can only be removed. Finalizers may be processed and removed in
     any order. Order is NOT enforced because it introduces significant risk of
     stuck finalizers. finalizers is a shared field, any actor with permission
     can reorder it. If the finalizer list is processed in order, then this can
     lead to a situation in which the component responsible for the first
     finalizer in the list is waiting for a signal (field value, external
     system, or other) produced by a component responsible for a finalizer later
     in the list, resulting in a deadlock. Without enforced ordering finalizers
     are free to order amongst themselves and are not vulnerable to ordering
     changes in the list.

   generateName <string>
     GenerateName is an optional prefix, used by the server, to generate a
     unique name ONLY IF the Name field has not been provided. If this field is
     used, the name returned to the client will be different than the name
     passed. This value will also be combined with a unique suffix. The provided
     value has the same validation rules as the Name field, and may be truncated
     by the length of the suffix required to make the value unique on the
     server. If this field is specified and the generated name exists, the
     server will NOT return a 409 - instead, it will either return 201 Created
     or 500 with Reason ServerTimeout indicating a unique name could not be
     found in the time allotted, and the client should retry (optionally after
     the time indicated in the Retry-After header). Applied only if Name is not
     specified. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#idempotency

   generation   <integer>
     A sequence number representing a specific generation of the desired state.
     Populated by the system. Read-only.

   labels       <map[string]string>
     Map of string keys and values that can be used to organize and categorize
     (scope and select) objects. May match selectors of replication controllers
     and services. More info: http://kubernetes.io/docs/user-guide/labels

   managedFields        <[]Object>
     ManagedFields maps workflow-id and version to the set of fields that are
     managed by that workflow. This is mostly for internal housekeeping, and
     users typically shouldn't need to set or understand this field. A workflow
     can be the user's name, a controller's name, or the name of a specific
     apply path like "ci-cd". The set of fields is always in the version that
     the workflow used when modifying the object.

   name <string>
     Name must be unique within a namespace. Is required when creating
     resources, although some resources may allow a client to request the
     generation of an appropriate name automatically. Name is primarily intended
     for creation idempotence and configuration definition. Cannot be updated.
     More info: http://kubernetes.io/docs/user-guide/identifiers#names

   namespace    <string>
     Namespace defines the space within each name must be unique. An empty
     namespace is equivalent to the "default" namespace, but "default" is the
     canonical representation. Not all objects are required to be scoped to a
     namespace - the value of this field for those objects will be empty. Must
     be a DNS_LABEL. Cannot be updated. More info:
     http://kubernetes.io/docs/user-guide/namespaces

   ownerReferences      <[]Object>
     List of objects depended by this object. If ALL objects in the list have
     been deleted, this object will be garbage collected. If this object is
     managed by a controller, then an entry in this list will point to this
     controller, with the controller field set to true. There cannot be more
     than one managing controller.

   resourceVersion      <string>
     An opaque value that represents the internal version of this object that
     can be used by clients to determine when objects have changed. May be used
     for optimistic concurrency, change detection, and the watch operation on a
     resource or set of resources. Clients must treat these values as opaque and
     passed unmodified back to the server. They may only be valid for a
     particular resource or set of resources. Populated by the system.
     Read-only. Value must be treated as opaque by clients and . More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#concurrency-control-and-consistency

   selfLink     <string>
     SelfLink is a URL representing this object. Populated by the system.
     Read-only. DEPRECATED Kubernetes will stop propagating this field in 1.20
     release and the field is planned to be removed in 1.21 release.

   uid  <string>
     UID is the unique in time and space value for this object. It is typically
     generated by the server on successful creation of a resource and is not
     allowed to change on PUT operations. Populated by the system. Read-only.
     More info: http://kubernetes.io/docs/user-guide/identifiers#uids
```

This shows all the options available for __metadata__ object. We will use __name__ only and we query again (although we read that it is just a "string").
```
vagrant@local-master:~$ kubectl explain pod.metadata.name
KIND:     Pod
VERSION:  v1

FIELD:    name <string>

DESCRIPTION:
     Name must be unique within a namespace. Is required when creating
     resources, although some resources may allow a client to request the
     generation of an appropriate name automatically. Name is primarily intended
     for creation idempotence and configuration definition. Cannot be updated.
     More info: http://kubernetes.io/docs/user-guide/identifiers#names
```

Let's do the same with __spec__. 
```
vagrant@local-master:~$ kubectl explain pod.spec
KIND:     Pod
VERSION:  v1

RESOURCE: spec <Object>

DESCRIPTION:
     Specification of the desired behavior of the pod. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

     PodSpec is a description of a pod.

FIELDS:
   activeDeadlineSeconds        <integer>
     Optional duration in seconds the pod may be active on the node relative to
     StartTime before the system will actively try to mark it failed and kill
     associated containers. Value must be a positive integer.

   affinity     <Object>
     If specified, the pod's scheduling constraints

   automountServiceAccountToken <boolean>
     AutomountServiceAccountToken indicates whether a service account token
     should be automatically mounted.

   containers   <[]Object> -required-
     List of containers belonging to the pod. Containers cannot currently be
     added or removed. There must be at least one container in a Pod. Cannot be
     updated.

   dnsConfig    <Object>
     Specifies the DNS parameters of a pod. Parameters specified here will be
     merged to the generated DNS configuration based on DNSPolicy.

   dnsPolicy    <string>
     Set DNS policy for the pod. Defaults to "ClusterFirst". Valid values are
     'ClusterFirstWithHostNet', 'ClusterFirst', 'Default' or 'None'. DNS
     parameters given in DNSConfig will be merged with the policy selected with
     DNSPolicy. To have DNS options set along with hostNetwork, you have to
     specify DNS policy explicitly to 'ClusterFirstWithHostNet'.

   enableServiceLinks   <boolean>
     EnableServiceLinks indicates whether information about services should be
     injected into pod's environment variables, matching the syntax of Docker
     links. Optional: Defaults to true.

   ephemeralContainers  <[]Object>
     List of ephemeral containers run in this pod. Ephemeral containers may be
     run in an existing pod to perform user-initiated actions such as debugging.
     This list cannot be specified when creating a pod, and it cannot be
     modified by updating the pod spec. In order to add an ephemeral container
     to an existing pod, use the pod's ephemeralcontainers subresource. This
     field is alpha-level and is only honored by servers that enable the
     EphemeralContainers feature.

   hostAliases  <[]Object>
     HostAliases is an optional list of hosts and IPs that will be injected into
     the pod's hosts file if specified. This is only valid for non-hostNetwork
     pods.

   hostIPC      <boolean>
     Use the host's ipc namespace. Optional: Default to false.

   hostNetwork  <boolean>
     Host networking requested for this pod. Use the host's network namespace.
     If this option is set, the ports that will be used must be specified.
     Default to false.

   hostPID      <boolean>
     Use the host's pid namespace. Optional: Default to false.

   hostname     <string>
     Specifies the hostname of the Pod If not specified, the pod's hostname will
     be set to a system-defined value.

   imagePullSecrets     <[]Object>
     ImagePullSecrets is an optional list of references to secrets in the same
     namespace to use for pulling any of the images used by this PodSpec. If
     specified, these secrets will be passed to individual puller
     implementations for them to use. For example, in the case of docker, only
     DockerConfig type secrets are honored. More info:
     https://kubernetes.io/docs/concepts/containers/images#specifying-imagepullsecrets-on-a-pod

   initContainers       <[]Object>
     List of initialization containers belonging to the pod. Init containers are
     executed in order prior to containers being started. If any init container
     fails, the pod is considered to have failed and is handled according to its
     restartPolicy. The name for an init container or normal container must be
     unique among all containers. Init containers may not have Lifecycle
     actions, Readiness probes, Liveness probes, or Startup probes. The
     resourceRequirements of an init container are taken into account during
     scheduling by finding the highest request/limit for each resource type, and
     then using the max of of that value or the sum of the normal containers.
     Limits are applied to init containers in a similar fashion. Init containers
     cannot currently be added or removed. Cannot be updated. More info:
     https://kubernetes.io/docs/concepts/workloads/pods/init-containers/

   nodeName     <string>
     NodeName is a request to schedule this pod onto a specific node. If it is
     non-empty, the scheduler simply schedules this pod onto that node, assuming
     that it fits resource requirements.

   nodeSelector <map[string]string>
     NodeSelector is a selector which must be true for the pod to fit on a node.
     Selector which must match a node's labels for the pod to be scheduled on
     that node. More info:
     https://kubernetes.io/docs/concepts/configuration/assign-pod-node/

   overhead     <map[string]string>
     Overhead represents the resource overhead associated with running a pod for
     a given RuntimeClass. This field will be autopopulated at admission time by
     the RuntimeClass admission controller. If the RuntimeClass admission
     controller is enabled, overhead must not be set in Pod create requests. The
     RuntimeClass admission controller will reject Pod create requests which
     have the overhead already set. If RuntimeClass is configured and selected
     in the PodSpec, Overhead will be set to the value defined in the
     corresponding RuntimeClass, otherwise it will remain unset and treated as
     zero. More info:
     https://git.k8s.io/enhancements/keps/sig-node/20190226-pod-overhead.md This
     field is alpha-level as of Kubernetes v1.16, and is only honored by servers
     that enable the PodOverhead feature.

   preemptionPolicy     <string>
     PreemptionPolicy is the Policy for preempting pods with lower priority. One
     of Never, PreemptLowerPriority. Defaults to PreemptLowerPriority if unset.
     This field is alpha-level and is only honored by servers that enable the
     NonPreemptingPriority feature.

   priority     <integer>
     The priority value. Various system components use this field to find the
     priority of the pod. When Priority Admission Controller is enabled, it
     prevents users from setting this field. The admission controller populates
     this field from PriorityClassName. The higher the value, the higher the
     priority.

   priorityClassName    <string>
     If specified, indicates the pod's priority. "system-node-critical" and
     "system-cluster-critical" are two special keywords which indicate the
     highest priorities with the former being the highest priority. Any other
     name must be defined by creating a PriorityClass object with that name. If
     not specified, the pod priority will be default or zero if there is no
     default.

   readinessGates       <[]Object>
     If specified, all readiness gates will be evaluated for pod readiness. A
     pod is ready when all its containers are ready AND all conditions specified
     in the readiness gates have status equal to "True" More info:
     https://git.k8s.io/enhancements/keps/sig-network/0007-pod-ready%2B%2B.md

   restartPolicy        <string>
     Restart policy for all containers within the pod. One of Always, OnFailure,
     Never. Default to Always. More info:
     https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#restart-policy

   runtimeClassName     <string>
     RuntimeClassName refers to a RuntimeClass object in the node.k8s.io group,
     which should be used to run this pod. If no RuntimeClass resource matches
     the named class, the pod will not be run. If unset or empty, the "legacy"
     RuntimeClass will be used, which is an implicit class with an empty
     definition that uses the default runtime handler. More info:
     https://git.k8s.io/enhancements/keps/sig-node/runtime-class.md This is a
     beta feature as of Kubernetes v1.14.

   schedulerName        <string>
     If specified, the pod will be dispatched by specified scheduler. If not
     specified, the pod will be dispatched by default scheduler.

   securityContext      <Object>
     SecurityContext holds pod-level security attributes and common container
     settings. Optional: Defaults to empty. See type description for default
     values of each field.

   serviceAccount       <string>
     DeprecatedServiceAccount is a depreciated alias for ServiceAccountName.
     Deprecated: Use serviceAccountName instead.

   serviceAccountName   <string>
     ServiceAccountName is the name of the ServiceAccount to use to run this
     pod. More info:
     https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/

   shareProcessNamespace        <boolean>
     Share a single process namespace between all of the containers in a pod.
     When this is set containers will be able to view and signal processes from
     other containers in the same pod, and the first process in each container
     will not be assigned PID 1. HostPID and ShareProcessNamespace cannot both
     be set. Optional: Default to false.

   subdomain    <string>
     If specified, the fully qualified Pod hostname will be
     "<hostname>.<subdomain>.<pod namespace>.svc.<cluster domain>". If not
     specified, the pod will not have a domainname at all.

   terminationGracePeriodSeconds        <integer>
     Optional duration in seconds the pod needs to terminate gracefully. May be
     decreased in delete request. Value must be non-negative integer. The value
     zero indicates delete immediately. If this value is nil, the default grace
     period will be used instead. The grace period is the duration in seconds
     after the processes running in the pod are sent a termination signal and
     the time when the processes are forcibly halted with a kill signal. Set
     this value longer than the expected cleanup time for your process. Defaults
     to 30 seconds.

   tolerations  <[]Object>
     If specified, the pod's tolerations.

   topologySpreadConstraints    <[]Object>
     TopologySpreadConstraints describes how a group of pods ought to spread
     across topology domains. Scheduler will schedule pods in a way which abides
     by the constraints. This field is alpha-level and is only honored by
     clusters that enables the EvenPodsSpread feature. All
     topologySpreadConstraints are ANDed.

   volumes      <[]Object>
     List of volumes that can be mounted by containers belonging to the pod.
     More info: https://kubernetes.io/docs/concepts/storage/volumes
```


```
vagrant@local-master:~$ kubectl explain pod.spec.containers.image
KIND:     Pod
VERSION:  v1

FIELD:    image <string>

DESCRIPTION:
     Docker image name. More info:
     https://kubernetes.io/docs/concepts/containers/images This field is
     optional to allow higher level config management to default or override
     container images in workload controllers like Deployments and StatefulSets.
```

Now we can prepare the simplest __Pod__ definition.
```
apiVersion: v1
kind: Pod
metadata:
  name: test
spec:
  containers:
    image: busybox
``` 

In this lab we learned a few about kubectl and how to obtain required information to create and modify Kubernetes resources.

---

## Lab3 - Accessing Kubernetes from internal resources

In this lab we will connect to the cluster using a service account's token associated with our user.

1 - First we create a namespace for these labs:
```
vagrant@local-master:~$ kubectl create namespace ch2
namespace/ch2 created
```

2 - We create a Pod with curl and some other resources:
```
vagrant@local-master:~$ kubectl run -n ch2 --rm test \
--image=frjaraur/nettools:minimal --restart=Never \
--attach=true -ti -- sh

If you don't see a command prompt, try pressing enter.
/ # cat /var/run/secrets/kubernetes.io/serviceaccount/token
eyJhbGciOiJSUzI1NiIsImtpZCI6IjVSSHphM2F6aTY5RWRHLXBJSDZQcGdkaG5kS3J4V01ERVp6cUhJZllCLUkifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJjaDIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlY3JldC5uYW1lIjoiZGVmYXVsdC10b2tlbi1yaG13ZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiNWI4Yjk1N2ItMjU5Ny00MzFlLWE4MWQtZmFkZjZjZGJlNGRlIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50OmNoMjpkZWZhdWx0In0.cNDZlKSnFG50r9IfNfbFgMmJrs1a148utkLmqXWC_apRdkPRof_grGoApZ0TimIyYh0uKlIlSL0hdipDKN_J7IDP35cI9H-YSFRCQ1barjKsNoSKYEzqUypWa9faFwcdggiJ2CCXeHlwQrs1eBmxLtgQMzYbZHAnDw8B7Z7xwdyK6AXwLKSSYTJ1bL7uEp9uvzUMxXbFcKciS_9gOlzJKrlxKk8A6pLOxkEqBD0phFifyZWqSIsnLg2zTTk8p4sSqHOQ_2BBEGy7kP0BzKcULdCBiGK_EtRtSEbZGZJ9LyG4dRS8s8LeZhD9cl5Bw6JS5UKKYVChlrUG03oqSz3a3A/ # 
```

All our __Pods__ are associated with our user and __/run/secrets/kubernetes.io/serviceaccount__ directory contains the used for our certifficate,__ca.crt__, the token created, __token__ and the namespace where the Pod is running, __namespace__:
```
 / #  ls -lart /var/run/secrets/kubernetes.io/serviceaccount/
total 4
lrwxrwxrwx    1 root     root            12 Jun  1 14:27 token -> ..data/token
lrwxrwxrwx    1 root     root            16 Jun  1 14:27 namespace -> ..data/namespace
lrwxrwxrwx    1 root     root            13 Jun  1 14:27 ca.crt -> ..data/ca.crt
lrwxrwxrwx    1 root     root            31 Jun  1 14:27 ..data -> ..2020_06_01_14_27_31.875567510
drwxr-xr-x    2 root     root           100 Jun  1 14:27 ..2020_06_01_14_27_31.875567510
drwxrwxrwt    3 root     root           140 Jun  1 14:27 .
drwxr-xr-x    3 root     root          4096 Jun  1 14:27 ..
```

---

## Lab4 - Viewing api-resources
In this lab we will review resources available and API versions in our Kubernetes cluster using __kubectl api-resources__.

1 - Review reources available.
```
vagrant@local-master:~$ kubectl api-resources -o wide
NAME                              SHORTNAMES   APIGROUP                       NAMESPACED   KIND                             VERBS
bindings                                                                      true         Binding                          [create]
componentstatuses                 cs                                          false        ComponentStatus                  [get list]
configmaps                        cm                                          true         ConfigMap                        [create delete deletecollection get list patch update watch]
endpoints                         ep                                          true         Endpoints                        [create delete deletecollection get list patch update watch]
events                            ev                                          true         Event                            [create delete deletecollection get list patch update watch]
limitranges                       limits                                      true         LimitRange                       [create delete deletecollection get list patch update watch]
namespaces                        ns                                          false        Namespace                        [create delete get list patch update watch]
nodes                             no                                          false        Node                             [create delete deletecollection get list patch update watch]
persistentvolumeclaims            pvc                                         true         PersistentVolumeClaim            [create delete deletecollection get list patch update watch]
persistentvolumes                 pv                                          false        PersistentVolume                 [create delete deletecollection get list patch update watch]
pods                              po                                          true         Pod                              [create delete deletecollection get list patch update watch]
podtemplates                                                                  true         PodTemplate                      [create delete deletecollection get list patch update watch]
replicationcontrollers            rc                                          true         ReplicationController            [create delete deletecollection get list patch update watch]
resourcequotas                    quota                                       true         ResourceQuota                    [create delete deletecollection get list patch update watch]
secrets                                                                       true         Secret                           [create delete deletecollection get list patch update watch]
serviceaccounts                   sa                                          true         ServiceAccount                   [create delete deletecollection get list patch update watch]
services                          svc                                         true         Service                          [create delete get list patch update watch]
mutatingwebhookconfigurations                  admissionregistration.k8s.io   false        MutatingWebhookConfiguration     [create delete deletecollection get list patch update watch]
validatingwebhookconfigurations                admissionregistration.k8s.io   false        ValidatingWebhookConfiguration   [create delete deletecollection get list patch update watch]
customresourcedefinitions         crd,crds     apiextensions.k8s.io           false        CustomResourceDefinition         [create delete deletecollection get list patch update watch]
apiservices                                    apiregistration.k8s.io         false        APIService                       [create delete deletecollection get list patch update watch]
controllerrevisions                            apps                           true         ControllerRevision               [create delete deletecollection get list patch update watch]
daemonsets                        ds           apps                           true         DaemonSet                        [create delete deletecollection get list patch update watch]
deployments                       deploy       apps                           true         Deployment                       [create delete deletecollection get list patch update watch]
replicasets                       rs           apps                           true         ReplicaSet                       [create delete deletecollection get list patch update watch]
statefulsets                      sts          apps                           true         StatefulSet                      [create delete deletecollection get list patch update watch]
tokenreviews                                   authentication.k8s.io          false        TokenReview                      [create]
localsubjectaccessreviews                      authorization.k8s.io           true         LocalSubjectAccessReview         [create]
selfsubjectaccessreviews                       authorization.k8s.io           false        SelfSubjectAccessReview          [create]
selfsubjectrulesreviews                        authorization.k8s.io           false        SelfSubjectRulesReview           [create]
subjectaccessreviews                           authorization.k8s.io           false        SubjectAccessReview              [create]
horizontalpodautoscalers          hpa          autoscaling                    true         HorizontalPodAutoscaler          [create delete deletecollection get list patch update watch]
cronjobs                          cj           batch                          true         CronJob                          [create delete deletecollection get list patch update watch]
jobs                                           batch                          true         Job                              [create delete deletecollection get list patch update watch]
certificatesigningrequests        csr          certificates.k8s.io            false        CertificateSigningRequest        [create delete deletecollection get list patch update watch]
leases                                         coordination.k8s.io            true         Lease                            [create delete deletecollection get list patch update watch]
bgpconfigurations                              crd.projectcalico.org          false        BGPConfiguration                 [delete deletecollection get list patch create update watch]
bgppeers                                       crd.projectcalico.org          false        BGPPeer                          [delete deletecollection get list patch create update watch]
blockaffinities                                crd.projectcalico.org          false        BlockAffinity                    [delete deletecollection get list patch create update watch]
clusterinformations                            crd.projectcalico.org          false        ClusterInformation               [delete deletecollection get list patch create update watch]
felixconfigurations                            crd.projectcalico.org          false        FelixConfiguration               [delete deletecollection get list patch create update watch]
globalnetworkpolicies                          crd.projectcalico.org          false        GlobalNetworkPolicy              [delete deletecollection get list patch create update watch]
globalnetworksets                              crd.projectcalico.org          false        GlobalNetworkSet                 [delete deletecollection get list patch create update watch]
hostendpoints                                  crd.projectcalico.org          false        HostEndpoint                     [delete deletecollection get list patch create update watch]
ipamblocks                                     crd.projectcalico.org          false        IPAMBlock                        [delete deletecollection get list patch create update watch]
ipamconfigs                                    crd.projectcalico.org          false        IPAMConfig                       [delete deletecollection get list patch create update watch]
ipamhandles                                    crd.projectcalico.org          false        IPAMHandle                       [delete deletecollection get list patch create update watch]
ippools                                        crd.projectcalico.org          false        IPPool                           [delete deletecollection get list patch create update watch]
networkpolicies                                crd.projectcalico.org          true         NetworkPolicy                    [delete deletecollection get list patch create update watch]
networksets                                    crd.projectcalico.org          true         NetworkSet                       [delete deletecollection get list patch create update watch]
endpointslices                                 discovery.k8s.io               true         EndpointSlice                    [create delete deletecollection get list patch update watch]
events                            ev           events.k8s.io                  true         Event                            [create delete deletecollection get list patch update watch]
ingresses                         ing          extensions                     true         Ingress                          [create delete deletecollection get list patch update watch]
ingresses                         ing          networking.k8s.io              true         Ingress                          [create delete deletecollection get list patch update watch]
networkpolicies                   netpol       networking.k8s.io              true         NetworkPolicy                    [create delete deletecollection get list patch update watch]
runtimeclasses                                 node.k8s.io                    false        RuntimeClass                     [create delete deletecollection get list patch update watch]
poddisruptionbudgets              pdb          policy                         true         PodDisruptionBudget              [create delete deletecollection get list patch update watch]
podsecuritypolicies               psp          policy                         false        PodSecurityPolicy                [create delete deletecollection get list patch update watch]
clusterrolebindings                            rbac.authorization.k8s.io      false        ClusterRoleBinding               [create delete deletecollection get list patch update watch]
clusterroles                                   rbac.authorization.k8s.io      false        ClusterRole                      [create delete deletecollection get list patch update watch]
rolebindings                                   rbac.authorization.k8s.io      true         RoleBinding                      [create delete deletecollection get list patch update watch]
roles                                          rbac.authorization.k8s.io      true         Role                             [create delete deletecollection get list patch update watch]
priorityclasses                   pc           scheduling.k8s.io              false        PriorityClass                    [create delete deletecollection get list patch update watch]
csidrivers                                     storage.k8s.io                 false        CSIDriver                        [create delete deletecollection get list patch update watch]
csinodes                                       storage.k8s.io                 false        CSINode                          [create delete deletecollection get list patch update watch]
storageclasses                    sc           storage.k8s.io                 false        StorageClass                     [create delete deletecollection get list patch update watch]
volumeattachments                              storage.k8s.io                 false        VolumeAttachment                 [create delete deletecollection get list patch update watch]
```

___-o wide___ shows all the verbs available for each __API-Resource__ published. This will help us to configure roles and grants (we will learn abour Kubernetes' RBAC in chapter 7).


2 - Review API versions. This is very important because they can change between Kubernetes releases. In this case we use __kubectl api-versions__.
```
vagrant@local-master:~$ kubectl api-versions 
admissionregistration.k8s.io/v1
admissionregistration.k8s.io/v1beta1
apiextensions.k8s.io/v1
apiextensions.k8s.io/v1beta1
apiregistration.k8s.io/v1
apiregistration.k8s.io/v1beta1
apps/v1
authentication.k8s.io/v1
authentication.k8s.io/v1beta1
authorization.k8s.io/v1
authorization.k8s.io/v1beta1
autoscaling/v1
autoscaling/v2beta1
autoscaling/v2beta2
batch/v1
batch/v1beta1
certificates.k8s.io/v1beta1
coordination.k8s.io/v1
coordination.k8s.io/v1beta1
crd.projectcalico.org/v1
discovery.k8s.io/v1beta1
events.k8s.io/v1beta1
extensions/v1beta1
networking.k8s.io/v1
networking.k8s.io/v1beta1
node.k8s.io/v1beta1
policy/v1beta1
rbac.authorization.k8s.io/v1
rbac.authorization.k8s.io/v1beta1
scheduling.k8s.io/v1
scheduling.k8s.io/v1beta1
storage.k8s.io/v1
storage.k8s.io/v1beta1
v1
```

To use __Declarative Mode__ for deploying workloads in our cluster, it is required to use appropriate API versions and we have learned in this lab how to get information about available releases.



---

## Lab5 - Create a Local Pod.

In this lab we will learn how to execute local pods, not managed by Kubernetes. This is used for some Kubernetes components and can be useful for monitoring tools or special applications.

1 - Kubelet is prepared to run any __Pod__ declared in __/etc/kubernetes/manifests__. We can review this configuration reading __Kubelet__'s systemd configuration and its associated configuration file:
```
vagrant@local-master:~$ sudo systemctl status kubelet
● kubelet.service - kubelet: The Kubernetes Node Agent
   Loaded: loaded (/lib/systemd/system/kubelet.service; enabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/kubelet.service.d
           └─10-kubeadm.conf
   Active: active (running) since Mon 2020-06-01 15:45:45 CEST; 2h 25min ago
     Docs: https://kubernetes.io/docs/home/
 Main PID: 9573 (kubelet)
    Tasks: 16 (limit: 4660)
   CGroup: /system.slice/kubelet.service
           └─9573 /usr/bin/kubelet --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes

Jun 01 15:47:39 local-master kubelet[9573]: 2020-06-01 15:47:39.540 [INFO][11985] k8s.go 358: Populated endpoint ContainerID=
Jun 01 15:47:39 local-master kubelet[9573]: 2020-06-01 15:47:39.540 [INFO][11985] k8s.go 359: Calico CNI using IPs: [192.168.
Jun 01 15:47:39 local-master kubelet[9573]: 2020-06-01 15:47:39.540 [INFO][11985] network_linux.go 76: Setting the host side 
Jun 01 15:47:39 local-master kubelet[9573]: 2020-06-01 15:47:39.540 [INFO][11985] network_linux.go 400: Disabling IPv4 forwar
Jun 01 15:47:39 local-master kubelet[9573]: 2020-06-01 15:47:39.558 [INFO][11985] k8s.go 385: Added Mac, interface name, and 
Jun 01 15:47:39 local-master kubelet[9573]: 2020-06-01 15:47:39.568 [INFO][11985] k8s.go 417: Wrote updated endpoint to datas
Jun 01 15:47:39 local-master kubelet[9573]: E0601 15:47:39.933285    9573 remote_runtime.go:295] ContainerStatus "2183e471117
Jun 01 15:47:39 local-master kubelet[9573]: E0601 15:47:39.933323    9573 kuberuntime_manager.go:955] getPodContainerStatuses
Jun 01 15:53:04 local-master kubelet[9573]: W0601 15:53:04.233735    9573 prober.go:108] No ref for container "docker://299ce
Jun 01 15:58:39 local-master kubelet[9573]: W0601 15:58:39.928537    9573 container.go:412] Failed to create summary reader f

vagrant@local-master:~$ sudo cat /etc/systemd/system/kubelet.service.d/10-kubeadm.conf 
# Note: This dropin only works with kubeadm and kubelet v1.11+
[Service]
Environment="KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf"
Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"
# This is a file that "kubeadm init" and "kubeadm join" generates at runtime, populating the KUBELET_KUBEADM_ARGS variable dynamically
EnvironmentFile=-/var/lib/kubelet/kubeadm-flags.env
# This is a file that the user can use for overrides of the kubelet args as a last resort. Preferably, the user should use
# the .NodeRegistration.KubeletExtraArgs object in the configuration files instead. KUBELET_EXTRA_ARGS should be sourced from this file.
Environment="KUBELET_EXTRA_ARGS=--fail-swap-on=false"
EnvironmentFile=-/etc/default/kubelet
ExecStart=
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS
```

We can finally read __/var/lib/kubelet/config.yaml__ and look for __staticPodPath__ key value.
```
vagrant@local-master:~$ sudo cat /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    cacheTTL: 0s
    enabled: true
  x509:
    clientCAFile: /etc/kubernetes/pki/ca.crt
authorization:
  mode: Webhook
  webhook:
    cacheAuthorizedTTL: 0s
    cacheUnauthorizedTTL: 0s
clusterDNS:
- 10.96.0.10
clusterDomain: cluster.local
cpuManagerReconcilePeriod: 0s
evictionPressureTransitionPeriod: 0s
fileCheckFrequency: 0s
healthzBindAddress: 127.0.0.1
healthzPort: 10248
httpCheckFrequency: 0s
imageMinimumGCAge: 0s
kind: KubeletConfiguration
nodeStatusReportFrequency: 0s
nodeStatusUpdateFrequency: 0s
rotateCertificates: true
runtimeRequestTimeout: 0s
staticPodPath: /etc/kubernetes/manifests
streamingConnectionIdleTimeout: 0s
syncFrequency: 0s
volumeStatsAggPeriod: 0s
```

2 - Let's move as root to __/etc/kubernetes/manifests__ directory and create
```

2 - Let's move as root to __/etc/kubernetes/manifests__ directory an
vagrant@local-master:~$ sudo -s

root@local-master:~# cd /etc/kubernetes/manifests/

root@local-master:/etc/kubernetes/manifests# cat <<-EOF>sleeping-test.pod.yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
  name: sleeping-test
spec:
  containers:
  - image: busybox
    name: sleeping-test
    args:
    - sleep
    - "300"
EOF
```

3 - Once created, we wait few seconds and review the __Pods__ running in our cluster using ___kubectl get pods___:
```
root@local-master:/etc/kubernetes/manifests# kubectl get pods
NAME                         READY   STATUS    RESTARTS   AGE
sleeping-test-local-master   1/1     Running   0          114s
```

Notice that __Pod__ was created and your hostname was added as sufix to the __Pod__'s name.

Notice that __/etc/kubernetes/manifests__ directory contains other local __Pods__.
```
root@local-master:/etc/kubernetes/manifests# ls -lart
total 28
drwxr-xr-x 4 root root 4096 Jun  1 15:45 ..
-rw------- 1 root root 1120 Jun  1 15:45 kube-scheduler.yaml
-rw------- 1 root root 3051 Jun  1 15:45 kube-controller-manager.yaml
-rw------- 1 root root 3118 Jun  1 15:45 kube-apiserver.yaml
-rw------- 1 root root 1756 Jun  1 15:45 etcd.yaml
-rw-r--r-- 1 root root  164 Jun  1 18:18 sleeping-test.pod.yaml
drwxr-xr-x 2 root root 4096 Jun  1 18:18 .
```

2 - Let's move as root to __/etc/kubernetes/manifests__ directory an __kube-apiserve__ and __etcd__ Kubernetes components deployed as __local Pods__.

If we list all the __Pods__ deployed in our host and filter by its hostname, we obtain all the __Local Pods__ running in our system.
```
root@local-master:/etc/kubernetes/manifests# kubectl get pods -A|grep "$(hostname)"
default       sleeping-test-local-master                 1/1     Running   3          16m
kube-system   etcd-local-master                          1/1     Running   0          168m
kube-system   kube-apiserver-local-master                1/1     Running   0          168m
kube-system   kube-controller-manager-local-master       1/1     Running   1          168m
kube-system   kube-scheduler-local-master                1/1     Running   1          168m
```

In this lab we learned how to implement __Local Pods__ and where to look for in case we have some kind of misconfiguration on Kubernetes Core Components.


---

### What we have learned in these labs?.
