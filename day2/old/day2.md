
# In this Lab:
- We will review __Deployments__ 
- Kubernetes objects
- __InitContainers__ and __Resources Quotas__
- Updates deployments

---
---
# Create/Delete deployment and manage objects

```
apiVersion: apps/v1
kind: Deployment
metadata:
 name: myapp
 labels:
   app: myapp
   type: web
spec:
  replicas: 3
  selector:
    matchLabels:
      type: web
  template:
    metadata:
      name: colors-pod
      labels:
        type: web
    spec:
      containers:
      - name: colors-pod
        image: codegazers/colors:1.5
        ports:
        - name: web
          containerPort: 3000

```

1 - Create deployment

```
$ kubectl create -f deployment.yaml
```
2 - Get deployment and pods
```
$ kubectl get deploy,pods
```
3 - Get all objects with extra information

```
$ kubectl get all -o wide
```
4 - Delete Pods , ReplicaSet
```
$ kubectl delete pod <pod-name>
$ kubectl delete rs <rs-name>
$ kubectl describe deploy <deployment-name>
```

# Add memory request and limits and initContainer

```
apiVersion: apps/v1
kind: Deployment
metadata:
 name: myapp
 labels:
   app: myapp
   type: web
spec:
  replicas: 3
  selector:
    matchLabels:
      type: web
  template:
    metadata:
      name: colors-pod
      labels:
        type: web
    spec:
      containers:
      - name: colors-pod
        image: codegazers/colors:1.5
        ports:
        - name: web
          containerPort: 3000
        resources:
          requests:
            memory: "100Mi"
          limits: 
            memory: "200Mi" 
      initContainers:
      - name: init-service
        image: busybox:1.28
        command: ['sh', '-c', 'until nslookup kubernetes; do echo waiting for kubernetes; sleep 2; done;']

```
1 - Apply new definitions
```
$ kubectl apply -f full-deployment.yaml  --record
$ kubectl describe pod <pod-name>

```
```
$ kubectl scale --replicas=5 <deployment-name> 
```

# Expose deployment
1 - Publish service outside

```
 kubectl expose deployment myapp --port=3000 --type=NodePort
```

# Update deployement
```
 $ kubectl set image deployment.apps/myapp colors-pod=codegazers/colors:1.10 --record

```

```
$ kubectl get all
$ kubectl rollout status deployment.apps/myapp

```

```
 $ kubectl rollout history deployment.apps/myapp
```

```
 $ kubectl rollout undo deployment myapp --to-revision=2

```

```
 $ kubectl delete -f full-deployment.yaml

```