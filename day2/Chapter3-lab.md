# CHAPTER 3

In these labs we will cover all the topics learned on de Chapter3.

- We will learn how to deploy/create/delete **Pods, Replicasets, Deployments, Statefulsets, Deamonsets** and **Services**.

- We will learn how to **update and scaling Deployments**.

---

## LAB1 - PODS 

In this lab we will practice creating and deleting pods, as well as the most common commands to use with them.


1- Create the namespace for these labs.
```
student@master:~$ kubectl create ns ch3
```

2- Create pods from the command line with kubectl.
```
student@master:~$ kubectl run --generator=run-pod/v1 nginx1 --image=nginx -n ch3

student@master:~$ kubectl run -n ch3 nginx2 --image=nginx --restart=Never

student@master:~$ kubectl run -n ch3 alpine --image=alpine --restart=Never -- sleep 3600
```
3- List the pods deployed in the ch3 namespace.
```
student@master:~$ kubectl get pod -n ch3
NAME     READY   STATUS    RESTARTS   AGE
alpine   1/1     Running   0          19s
nginx1   1/1     Running   0          2m41s
nginx2   1/1     Running   0          37s
```

4- Create pods from a yaml file.
```
student@master:~$ cat <<EOF>pod-nginx3.yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: nginx
  name: nginx3
  namespace: ch3
spec:
  containers:
  - image: nginx
    name: nginx3
    env:
    - name: IP_POD
      valueFrom:
        fieldRef:
          fieldPath: status.podIP 
    - name: WORKER_NAME
      valueFrom:
        fieldRef:
          fieldPath: spec.nodeName
    command: ["/bin/sh"]
    args: ["-c", "while true; do 
                          echo '\n' C_NAME:$HOSTNAME NginxV:$NGINX_VERSION C_IP:$IP_POD W_NAME:$WORKER_NAME '\n';date; sleep 30;done"]
    resources: {}
EOF

student@master:~$ kubectl apply -f pod-nginx3.yaml


student@master:~$ cat <<EOF>pod-alpine2.yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: alpine
  name: alpine2
  namespace: ch3
spec:
  containers:
  - command:
    - sleep
    - "3600"
    image: alpine
    name: alpine2
    resources: {}
EOF


student@master:~$ kubectl apply -f pod-alpine2.yaml
```

5- Check that the pods have been deployed correctly.
```
student@master:~$ kubectl get pod -n ch3
NAME      READY   STATUS    RESTARTS   AGE
alpine    1/1     Running   0          3m1s
alpine2   1/1     Running   0          5s
nginx1    1/1     Running   0          5m23s
nginx2    1/1     Running   0          3m19s
nginx3    1/1     Running   0          9s
```

6- To see the configuration of a pod (or any kubernetes resource) at run time we can see it by executing kubectl describe <resource>.
```
student@master:~$ kubectl describe pod nginx1 -n ch3
```

7- We can execute commands inside the pod. If the pod, for example, has the command ping we could execute a ping against the ip of another of the pods, or even against the ip of any of the nodes of the cluster.
For them, we will first find out the ip of one of the pods we want to ping. One way would be the following:
```
student@master:~$ kubectl get pod -n ch3 -o wide

NAME      READY   STATUS      RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
alpine    0/1     Completed   0          88m   11.11.190.164   ubs1804-k8s117-worker1   <none>           <none>
alpine2   1/1     Running     1          85m   11.11.190.133   ubs1804-k8s117-worker1   <none>           <none>
nginx1    1/1     Running     0          90m   11.11.190.191   ubs1804-k8s117-worker1   <none>           <none>
nginx2    1/1     Running     0          88m   11.11.190.131   ubs1804-k8s117-worker1   <none>           <none>
nginx3    1/1     Running     0          18s   11.11.190.141   ubs1804-k8s117-worker1   <none>           <none>

```

8- Execute a ping from the alpine2 pod to the ip of the nginx3 pod and against the ip of the master node.

```
student@master:~$ kubectl exec -n ch3 -ti alpine2 -- ping -c 3 <ip pod nginx3>

student@master:~$ kubectl exec -n ch3 -ti alpine2 -- ping -c 3 <ip nodo master>
```

9- Run a shell on the pod with the name alpine2.
```
student@master:~$ kubectl exec -n ch3 -ti alpine2 -- sh
```

10- Visualize the logs of the pod nginx3.
```
student@master:~$ kubectl logs nginx3 -n ch3
```

11- Delete all the pods created in this lab.
```
student@master:~$ kubectl delete pod --all -n ch3
```

---

## LAB2 - RESOURCE QUOTAS

In this lab we will see how to define/configure the use of resource quotas in the definition/creation of pods.
We are going to create several pods with a resource configuration superior to the resources available in the worker nodes.

1- Let's see how resource quotas are defined at the yaml file level of a pod. To do this we'll look at the contents of the pod-nginx-quotas.yaml file.
```
student@master:~$ cat<<EOF>pod-nginx-quotas.yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: nginx-quotas
  name: nginx-quotas
  namespace: ch3
spec:
  containers:
  - image: nginx
    name: nginx-quotas
    resources:
      limits:
        cpu: 200m
        memory: 512Mi
      requests:
        cpu: 100m
        memory: 256Mi
EOF
```
2- We deployed 3 pods with a memory level resource configuration above the available memory at the worker node level.
```
student@master:~$ kubectl run --generator=run-pod/v1 nginx-quotas1 --image=nginx --requests='cpu=100m,memory=1.5Gi' --limits='cpu=200m,memory=1.7Gi' -n ch3

student@master:~$ kubectl run --generator=run-pod/v1 nginx-quotas2 --image=nginx --requests='cpu=100m,memory=1.5Gi' --limits='cpu=200m,memory=1.7Gi' -n ch3

student@master:~$ kubectl run --generator=run-pod/v1 nginx-quotas3 --image=nginx --requests='cpu=100m,memory=2.5Gi' --limits='cpu=200m,memory=2.7Gi' -n ch3 
```

3- Check the status of the pods deployed in the previous point.
```
student@master:~$ kubectl get pod -n ch3 -o wide

NAME            READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
nginx-quotas1   1/1     Running   0          30m   11.11.190.134   worker1   <none>           <none>
nginx-quotas2   1/1     Running   0          29m   11.11.218.246   worker2   <none>           <none>
nginx-quotas3   0/1     Pending   0          29m   <none>          <none>                   <none>           <none>
```

4- Check the reason why one of the pods has been left in Pending status. Look at the Events section at the end.
```
student@master:~$ kubectl describe pod nginx-quotas3 -n ch3

Events:
  Type     Reason            Age                 From               Message
  ----     ------            ----                ----               -------
  Warning  FailedScheduling  89s (x24 over 32m)  default-scheduler  0/3 nodes are available: 1 node(s) had taints that the pod didn't tolerate, 2 Insufficient memory.
```

5- The problem is that there are not enough memory resources in any of the worker nodes of the cluster.
Check the characteristics of the nodes to find out the memory of each one.
```
student@master:~$ kubectl describe node <nodo worker>
```

6- Remove the pod that is in a pending state and deploy it with sufficient resources to make its deployment possible.
Substitute the XXX for the value you think is appropriate.
```
student@master:~$ kubectl delete pod nginx-quotas3 -n ch3

student@master:~$ kubectl run --generator=run-pod/v1 nginx-quotas3 --image=nginx --requests='cpu=100m,memory=XXXMi' --limits='cpu=200m,memory=XXXMi' -n ch3
```
7- Delete all the elements deployed in this lab.
```
student@master:~$ kubectl delete pod --all -n ch3
```

---  

## LAB3 - INIT CONTAINERS

In this lab we will be working with init-containers.
We will see their configuration and their mode of use, as well as their influence on the deployments of the pods that make use of them.


1- Create a pod with a container whose deployment and execution depends on the previous existence of a file. The file /mnt/existe.txt must exist in the init-container for the pod to fully deploy.
```

student@master:~$ cat <<EOF>init-container-file.yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: init-container-file
  name: init-container-file
  namespace: ch3
spec:
  containers:
  - image: nginx
    name: nginx
    env:
    - name: IP_POD
      valueFrom:
         fieldRef:
           fieldPath: status.podIP 
    - name: WORKER_NAME
      valueFrom:
         fieldRef:
           fieldPath: spec.nodeName
    command: ["/bin/sh"]
    args: ["-c", "while true; do 
                          echo '\n' C_NAME:\$HOSTNAME NginxV:\$NGINX_VERSION C_IP:\$IP_POD W_NAME:\$WORKER_NAME '\n';date; sleep 30;done"]
  initContainers:
   - name: init-file
     image: alpine
     command: ['sh', '-c', "until ls /mnt/existe.txt; do echo waiting for file; sleep 2; done"]
EOF


student@master:~$ kubectl apply -f init-container-file.yaml
```

2- Check the status of the deployed pod.
```
student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS     RESTARTS   AGE
init-container-file   0/1     Init:0/1   0          6s
```

3- The pod is in the Init:0/1 state, which means that the condition set by the init-container has not yet been met.
Run a "describe" on the pod, and check the "state" of both the init-container and the container.
```
student@master:~$ kubectl describe pod init-container-file -n ch3
```

4- Create the file /mnt/existe.txt in the init-container and then check the execution status of the pod.(Check it several times, because sometimes it takes a little longer to create the pod.)
```
student@master:~$ kubectl exec -it -n ch3 init-container-file -c init-file -- touch /mnt/existe.txt

student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS     RESTARTS   AGE
init-container-file   0/1     Init:0/1   0          7m38s

student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS            RESTARTS   AGE
init-container-file   0/1     PodInitializing   0          7m40s

student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS    RESTARTS   AGE
init-container-file   1/1     Running   0          7m42s
```

5- Deploy a new pod (init-container-service). This time the correct deployment of the pod will depend on the existence of a service.
```
student@master:~$ cat<<EOF> init-container-service.yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: init-container-svc
  name: init-container-svc
  namespace: ch3
spec:
  containers:
  - image: nginx
    name: nginx
    env:
    - name: IP_POD
      valueFrom:
         fieldRef:
           fieldPath: status.podIP 
    - name: WORKER_NAME
      valueFrom:
         fieldRef:
           fieldPath: spec.nodeName
    command: ["/bin/sh"]
    args: ["-c", "while true; do 
                          echo '\n' C_NAME:\$HOSTNAME NginxV:\$NGINX_VERSION C_IP:\$IP_POD W_NAME:\$WORKER_NAME '\n';date; sleep 30;done"]
  initContainers:
   - name: init-svc
     image: alpine
     command: ['sh', '-c', "until nslookup my-nginx-service.\$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for myservice; sleep 2; done"]
EOF

```

We deploy this pod:
```
kubectl create -f init-container-service.yaml


student@master:~$ kubectl describe pod init-container-svc -n ch3
```

6- Check the status of the deployed pod.
```
student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS     RESTARTS   AGE
init-container-file   1/1     Running    0          13m
init-container-svc    0/1     Init:0/1   0          4s
```

7- If you want, you can perform the same checks that were done with the previous deployment.

8- We deploy the service without which the deployment of the pod init-container-svc will not take place.
```
student@master:~$ kubectl create svc clusterip my-nginx-service --tcp=80:80 -n ch3
```

9- Check the status of the deployed pod.
```
student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS     RESTARTS   AGE
init-container-file   1/1     Running    0          21m
init-container-svc    0/1     Init:0/1   0          35s

student@master:~$ kubectl get pod -n ch3

NAME                  READY   STATUS            RESTARTS   AGE
init-container-file   1/1     Running           0          21m
init-container-svc    0/1     PodInitializing   0          36s

student@master:~$ kubectl get pod -n ch3

AME                  READY   STATUS    RESTARTS   AGE
init-container-file   1/1     Running   0          21m
init-container-svc    1/1     Running   0          38s
```

10- Check, by looking at the logs of the two pods, that both are running correctly.
```
student@master:~$ kubectl logs init-container-file -n ch3

C_NAME:init-container-file NginxV:1.19.0 C_IP:11.11.190.132 W_NAME:ubs1804-k8s117-worker1 

Wed Jun  3 18:23:24 UTC 2020

 C_NAME:init-container-file NginxV:1.19.0 C_IP:11.11.190.132 W_NAME:ubs1804-k8s117-worker1 

Wed Jun  3 18:23:54 UTC 2020
```

```
student@master:~$ kubectl logs init-container-svc -n ch3

C_NAME:init-container-svc NginxV:1.19.0 C_IP:11.11.190.168 W_NAME:ubs1804-k8s117-worker1 

Wed Jun  3 18:37:35 UTC 2020

 C_NAME:init-container-svc NginxV:1.19.0 C_IP:11.11.190.168 W_NAME:ubs1804-k8s117-worker1 

Wed Jun  3 18:38:05 UTC 2020
```

11- Delete all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch3
```

---


## LAB4 - REPLICASETS

In this lab we will learn how to deploy Replicasets and how to perform scales on them.

1- Given the file replicaset.yaml review its contents to understand its structure.
Note that the replicaset will generate 3 replicas of a pod composed of an nginx container, which will have port 80 exposed. 
```
student@master:~$ cat <<EOF>replicaset.yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
 name: myapp-rs
 namespace: ch3
 labels:
   app: myapp
   type: web
spec:
  replicas: 3
  selector:
    matchLabels:
      type: web
  template:
    metadata:
      name: nginx-pod
      labels:
        type: web
    spec:
      containers:
      - name: nginx-pod
        image: nginx
        ports:
        - name: web
          containerPort: 80
EOF

```


2- Deploy the replicaset defined in the replicaset.yaml file.
```
student@master:~$ kubectl apply -f replicaset.yaml
```

3- Check the deployment and creation of the replicaset.
```
student@master:~$ kubectl get rs -n ch3

NAME       DESIRED   CURRENT   READY   AGE
myapp-rs   3         3         0       3s

NAME       DESIRED   CURRENT   READY   AGE
myapp-rs   3         3         3       3m31s
```
It is noted that of the 3 desired replicas (DESIRED), 3 (CURRENT) have been generated, but that none are yet available (READY) at this time.
A few moments later, the 3 are displayed in the READY state.

4- Check the status and number of the pods "controlled" by the replicaset.
```
student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS    RESTARTS   AGE
myapp-rs-gspjq   1/1     Running   0          7m22s
myapp-rs-qnhv5   1/1     Running   0          7m22s
myapp-rs-xk9kn   1/1     Running   0          7m22s
```
The pods created have as their name prefix, the name of the replicaset.

5- A replicaset is in charge of always keeping running the number of replicas (pods) it has in its configuration, in this case 3.
For that reason if we delete one of the pods, the replicaset will automatically pick up a new one to maintain the number of replicas.
```
student@master:~$ kubectl delete pod myapp-rs-xxxxx -n ch3

pod "myapp-rs-xxxxx" deleted

student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS              RESTARTS   AGE
myapp-rs-fkm4d   0/1     ContainerCreating   0          4s
myapp-rs-qnhv5   1/1     Running             0          12m
myapp-rs-xk9kn   1/1     Running             0          12m

student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS    RESTARTS   AGE
myapp-rs-fkm4d   1/1     Running   0          6s
myapp-rs-qnhv5   1/1     Running   0          12m
myapp-rs-xk9kn   1/1     Running   0          12m
```

6- You can change the number of replicas that a replicaset manages, either by increasing the number of replicas...
```
student@master:~$ kubectl scale --replicas=5 rs/myapp-rs -n ch3

replicaset.apps/myapp-rs scaled

student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS    RESTARTS   AGE
myapp-rs-cxf77   1/1     Running   0          36s
myapp-rs-dwghj   1/1     Running   0          36s
myapp-rs-fkm4d   1/1     Running   0          5m22s
myapp-rs-qnhv5   1/1     Running   0          17m
myapp-rs-xk9kn   1/1     Running   0          17m
```

7- ...or by decreasing the number of replicas....
```
student@master:~$ kubectl scale --replicas=1 rs/myapp-rs -n ch3

replicaset.apps/myapp-rs scaled

student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS        RESTARTS   AGE
myapp-rs-cxf77   1/1     Terminating   0          2m45s
myapp-rs-dwghj   1/1     Terminating   0          2m45s
myapp-rs-fkm4d   0/1     Terminating   0          7m31s
myapp-rs-qnhv5   1/1     Running       0          19m
myapp-rs-xk9kn   0/1     Terminating   0          19m

student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS    RESTARTS   AGE
myapp-rs-qnhv5   1/1     Running   0          20m
```

8- ...even leaving the number of replicas at 0.
```
student@master:~$ kubectl scale --replicas=0 rs/myapp-rs -n ch3

replicaset.apps/myapp-rs scaled

student@master:~$ kubectl get pod -n ch3

NAME             READY   STATUS        RESTARTS   AGE
myapp-rs-qnhv5   1/1     Terminating   0          21m

student@master:~$ kubectl get pod -n ch3

No resources found in ch3 namespace.
```

9- Although there is no replica, the replicaset will still be deployed.
```
student@master:~$ kubectl get rs -n ch3

NAME       DESIRED   CURRENT   READY   AGE
myapp-rs   0         0         0       22m
```

10- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch3
```
---

## LAB5 - DEPLOYMENTS

In this lab we will be working with deployments.
We are going to make deployments of deployment, scaling, upgrades(rollout) and downgrades(rollout undo)

1- Create a deployment from the command line.
```

student@master:~$ kubectl create deploy deploy-lab5-nginx --image=nginx -n ch3

student@master:~$ kubectl run deploy-lab5-nginx --replicas=2 --image=nginx -n ch3 (DEPRECATED COMMAND)
```

The creation of a deployment also implies the automatic creation of a replicaset, which will be in charge of making sure that the number of replicas of the pods is always the one configured.

```
student@master:~$ kubectl get deploy -n ch3

NAME                READY   UP-TO-DATE   AVAILABLE   AGE
deploy-lab5-nginx   1/1     1            1           21m

student@master:~$ kubectl get rs -n ch3

NAME                          DESIRED   CURRENT   READY   AGE
deploy-lab5-nginx-76f7ddbcc   1         1         1       22m

student@master:~$ kubectl get pod -n ch3

NAME                                READY   STATUS    RESTARTS   AGE
deploy-lab5-nginx-76f7ddbcc-kz5jw   1/1     Running   0          22m
```

2- Just like replicaset, deployments can be scaled, both to increase the number of replicas, and to decrease it, and even to have no replicas at all.
```
student@master:~$ kubectl scale --replicas=3 deploy/deploy-lab5-nginx -n ch3

deployment.apps/deploy-lab5-nginx scaled

student@master:~$ kubectl get deploy -n ch3

NAME                READY   UP-TO-DATE   AVAILABLE   AGE
deploy-lab5-nginx   3/3     3            3           21m

student@master:~$ kubectl get rs -n ch3

NAME                          DESIRED   CURRENT   READY   AGE
deploy-lab5-nginx-76f7ddbcc   3         3         3       22m

student@master:~$ kubectl get pod -n ch3

NAME                                 READY   STATUS    RESTARTS   AGE
deploy-lab5-nginx-74f96f84c8-88wf2   1/1     Running   0          28s
deploy-lab5-nginx-74f96f84c8-8qrjn   1/1     Running   0          4m8s
deploy-lab5-nginx-74f96f84c8-h49n6   1/1     Running   0          28s
```

3- In the next exercise we are going to update the nginx version of a deployment and then downgrade it.
To do this we will deploy everything necessary through the file deploy-nginx-complete-pv-nfs.yaml.
This deployment, among other elements, will create 3 nginx pods with version 1.17.10, which will write to disk, in a file, a series of data including the name of the pod, the pod's ip and the version of nginx. In this way, when we update the deployment version, which has nginx version 1.18.10 associated with it, we will be able to track the update process. (Remember to correctly replace the ip of the nfs server, as well as the shared folder.)

We will start by deploying the file yaml deploy-nginx-complete-pv-nfs.yaml.
```

student@master:~$ cat<<EOF> deploy-nginx-complete-pv-nfs.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ch3


---

apiVersion: v1
kind: PersistentVolume
metadata:
  name: ch3-pv-nginx
  namespace: ch3
  labels:
    type: nfs-nginx
spec:
  storageClassName: nfs
  capacity:
    storage: 10Mi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /data/<student-number>
    server: <nfs-server-ip>

---

apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim-nginx
  namespace: ch3
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 10Mi
  storageClassName: nfs
  selector:
    matchLabels:
      type: nfs-nginx


---

kind: Service 
apiVersion: v1 
metadata:
  name: my-nginx-service 
  namespace: ch3
spec:
  type: NodePort
  selector:
    app: my-nginx-ch3
  ports:
    - nodePort: 30163
      port: 8080 
      targetPort: 80
---

apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: my-nginx-ch3
  name: my-nginx-pvnfs-ch3
  namespace: ch3
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-nginx-ch3
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: my-nginx-ch3
    spec:
      containers:
      - image: nginx:1.17.10
        name: nginx
        env:
        - name: IP_POD
          valueFrom:
            fieldRef:
              fieldPath: status.podIP 
        - name: WORKER_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        command: ["/bin/sh"]
        args: ["-c", "while true; do 
                          echo '\n' C_NAME:\$HOSTNAME NginxV:\$NGINX_VERSION C_IP:\$IP_POD W_NAME:\$WORKER_NAME '\n' >> /mnt/salida-nginx.txt; sleep 10;done"]
        volumeMounts:
        - mountPath: "/mnt"
          name: mypd
      volumes:
        - name: mypd
          persistentVolumeClaim:
             claimName: myclaim-nginx
      initContainers:
      - name: init-file
        image: alpine
        #command: ['sh', '-c', "until ls /mnt/existe.txt; do echo waiting for file; sleep 2; done"]
        command: ['sh', '-c', "until nslookup my-nginx-service.\$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for myservice; sleep 2; done"]
EOF



student@master:~$ kubectl apply -f deploy-nginx-complete-pv-nfs.yaml
```

3.1- Check that the deployment has been done correctly by checking that the pods are running.
```
student@master:~$ kubectl get pod -n ch3

NAME                                  READY   STATUS    RESTARTS   AGE
deploy-lab5-nginx-74f96f84c8-88wf2    1/1     Running   0          17m
deploy-lab5-nginx-74f96f84c8-8qrjn    1/1     Running   0          21m
deploy-lab5-nginx-74f96f84c8-h49n6    1/1     Running   0          17m
my-nginx-pvnfs-ch3-68996d65cd-7gsx2   1/1     Running   0          10s
my-nginx-pvnfs-ch3-68996d65cd-nxkjq   1/1     Running   0          10s
my-nginx-pvnfs-ch3-68996d65cd-srlbs   1/1     Running   0          10s
```

3.2- Check that the pods are writing correctly to the output file. To do this, open a new session and connect to the NFS server. The file is located in /data/<student>/salida.txt
```
student@nfsserver:~$ tail -f /data/<student-number>/salida.txt

C_NAME:my-nginx-pvnfs-ch3-68996d65cd-7gsx2 NginxV:1.17.10 C_IP:11.11.218.219 W_NAME:worker2 


C_NAME:my-nginx-pvnfs-ch3-68996d65cd-nxkjq NginxV:1.17.10 C_IP:11.11.190.142 W_NAME:worker1 


C_NAME:my-nginx-pvnfs-ch3-68996d65cd-srlbs NginxV:1.17.10 C_IP:11.11.190.150 W_NAME:worker1 


C_NAME:my-nginx-pvnfs-ch3-68996d65cd-7gsx2 NginxV:1.17.10 C_IP:11.11.218.219 W_NAME:worker2 


C_NAME:my-nginx-pvnfs-ch3-68996d65cd-nxkjq NginxV:1.17.10 C_IP:11.11.190.142 W_NAME:worker1
 
```

3.3- Update the nginx image to the new version 1.18.10, using the --record=true parameter to save the changes we're going to make, otherwise it wouldn't be possible to go back.
```
student@master:~$ kubectl set image deploy/my-nginx-pvnfs-ch3 nginx=nginx:1.18.0 --record=true -n ch3

deployment.apps/my-nginx-pvnfs-ch3 image updated
```

3.4- Check the update status.
```
student@master:~$ kubectl rollout status deploy/my-nginx-pvnfs-ch3 -n ch3

Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 2 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 2 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 2 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 old replicas are pending termination...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 old replicas are pending termination...
deployment "my-nginx-pvnfs-ch3" successfully rolled out
```

3.5- Once the update is finished and through the salida.txt file we can also check the update.
```
student@nfsserver:~$ tail -f /data/<student-number>/salida.txt

 C_NAME:my-nginx-pvnfs-ch3-967dfb97d-gbphr NginxV:1.18.0 C_IP:11.11.190.148 W_NAME:worker1 


 C_NAME:my-nginx-pvnfs-ch3-967dfb97d-gckrm NginxV:1.18.0 C_IP:11.11.190.147 W_NAME:worker1 


 C_NAME:my-nginx-pvnfs-ch3-967dfb97d-qvg5q NginxV:1.18.0 C_IP:11.11.218.222 W_NAME:worker2
```

3.6- Consult the history of the states of the deployment update.
```
student@master:~$ kubectl rollout history deploy/my-nginx-pvnfs-ch3 -n ch3

deployment.apps/my-nginx-pvnfs-ch3 
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl-1.17.3 set image deploy/my-nginx-pvnfs-ch3 nginx=nginx:1.18.0 --kubeconfig=/home/jjfraile/MaquetasLocal/k8s/1.17.3/kubeconfig.conf --record=true --namespace=ch3
```

3.7- Leave the initial version of the deployment again, that is, leave the nginx image in version 1.17.10. To do this we'll do a rollout undo, indicating which revision we want to return to. In this case it would be version 1.
```
student@master:~$ kubectl rollout undo deploy/my-nginx-pvnfs-ch3 --to-revision=1 -n ch3

deployment.apps/my-nginx-pvnfs-ch3 rolled back
```

3.8- Consult the history of the states of the deployment rollback.
```
student@master:~$ kubectl rollout history deploy/my-nginx-pvnfs-ch3 -n ch3

Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 2 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 2 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 2 out of 3 new replicas have been updated...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 old replicas are pending termination...
Waiting for deployment "my-nginx-pvnfs-ch3" rollout to finish: 1 old replicas are pending termination...
deployment "my-nginx-pvnfs-ch3" successfully rolled out
```

3.9- Once the rollback is finished and through the salida.txt file we can also check the rollback.
```
student@nfsserver:~$ tail -f /data/<student-number>/salida.txt

 C_NAME:my-nginx-pvnfs-ch3-68996d65cd-nd6fz NginxV:1.17.10 C_IP:11.11.190.145 W_NAME:worker1 


 C_NAME:my-nginx-pvnfs-ch3-68996d65cd-mtfj9 NginxV:1.17.10 C_IP:11.11.190.144 W_NAME:worker1 


 C_NAME:my-nginx-pvnfs-ch3-68996d65cd-h9rmq NginxV:1.17.10 C_IP:11.11.218.230 W_NAME:worker2
```

4- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch3

student@master:~$ kubectl delete pvc --all -n ch3

student@master:~$ kubectl delete pv --all -n ch3
```
---

## LAB6 - STATEFULSETS

In this lab we will be working with statefulsets.
We're going to deploy a statefulset that uses persistent storage, where each pod will store its data, and we'll also deploy a service, without which the deployment will not take place as it will be required by an init-container.
To do this we will use two yalm definition files, one for the creation of the persistent storage (pv-statefulset-nfs.yaml) and another for the deployment of the service and the statefulset (statefulset-nginx.yaml). 
We will also upgrade the statefulset. (Remember to correctly replace the ip of the nfs server, as well as the shared folder.)


1- Review the contents of the two files to see and understand what each one will deploy.
```
student@master:~$ cat<<EOF>statefulset-nginx.yaml
kind: Service 
apiVersion: v1 
metadata:
  name: my-nginx-service 
  namespace: ch3
spec:
  type: NodePort
  selector:
    app: my-nginx-ch3
  ports:
    - nodePort: 30163
      port: 8080 
      targetPort: 80

---

apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: statefulset-nginx-ch3
  name: statefulset-nginx-ch3
  namespace: ch3
spec:
  serviceName: "statefulset-nginx"
  replicas: 1
  selector:
    matchLabels:
      app: statefulset-nginx-ch3
  template:
    metadata:
      labels:
        app: statefulset-nginx-ch3
    spec:
      containers:
      - image: nginx:1.17.10
        name: nginx
        env:
        - name: IP_POD
          valueFrom:
            fieldRef:
              fieldPath: status.podIP 
        - name: WORKER_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        command: ["/bin/sh"]
        args: ["-c", "while true; do 
                          echo '\n' C_NAME:\$HOSTNAME NginxV:\$NGINX_VERSION C_IP:\$IP_POD W_NAME:\$WORKER_NAME '\n' >> /mnt/LOG_\$HOSTNAME.txt; sleep 10;done"]
        volumeMounts:
        - mountPath: "/mnt"
          name: mypd
      initContainers:
      - name: init-file
        image: alpine
        command: ['sh', '-c', "until nslookup my-nginx-service.\$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for myservice; sleep 2; done"]
  volumeClaimTemplates:
  - metadata:
       name: mypd
    spec:
       accessModes: [ "ReadWriteOnce" ]
       volumeMode: Filesystem
       resources:
         requests:
           storage: 10Mi
       storageClassName: nfs
       selector:
        matchLabels:
          type: nfs-nginx
EOF


student@master:~$ cat<<EOF>pv-statefulset-nfs.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: statefulset-pv0-ch3
  namespace: ch3
  labels:
    type: nfs-nginx
spec:
  storageClassName: nfs
  capacity:
    storage: 10Mi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /data/<student-number>
    server: <nfs-server-ip>

---

apiVersion: v1
kind: PersistentVolume
metadata:
  name: statefulset-pv1-ch3
  namespace: ch3
  labels:
    type: nfs-nginx
spec:
  storageClassName: nfs
  capacity:
    storage: 10Mi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /data/<student-number>
    server: <nfs-server-ip>

---

apiVersion: v1
kind: PersistentVolume
metadata:
  name: statefulset-pv2-ch3
  namespace: ch3
  labels:
    type: nfs-nginx
spec:
  storageClassName: nfs
  capacity:
    storage: 10Mi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /data/<student-number>
    server: <nfs-server-ip>

---

apiVersion: v1
kind: PersistentVolume
metadata:
  name: statefulset-pv3-ch3
  namespace: ch3
  labels:
    type: nfs-nginx
spec:
  storageClassName: nfs
  capacity:
    storage: 10Mi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /data/<student-number>
    server: <nfs-server-ip>
EOF




```

Note that, unlike deployments, each of the statefulset replicas will require its own power-up, i.e. the pods will not share it.

2- Deploy through the yaml files.
```
student@master:~$ kubectl apply -f pv-statefulset-nfs.yaml

persistentvolume/statefulset-pv0-ch3 created
persistentvolume/statefulset-pv1-ch3 created
persistentvolume/statefulset-pv2-ch3 created
persistentvolume/statefulset-pv3-ch3 created

student@master:~$ kubectl apply -f statefulset-nginx.yaml

service/my-nginx-service created
statefulset.apps/statefulset-nginx-ch3 created
```

3- Check all the elements deployed:
- Statefulset
- Service
- Pods
- Pvc(Persistent Volume Claim)
- Pv(Persistent Volume)
The elements service, pvc and pv correspond to the services and persistent storage that we will see more in depth later.
```
student@master:~$ kubectl get sts -n ch3

NAME                    READY   AGE
statefulset-nginx-ch3   1/1     93s

student@master:~$ kubectl get svc -n ch3

NAME               TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
my-nginx-service   NodePort   10.99.104.156   <none>        8080:30163/TCP   42s

student@master:~$ kubectl get pod -n ch3

NAME                      READY   STATUS    RESTARTS   AGE
statefulset-nginx-ch3-0   1/1     Running   0          2m49s

student@master:~$ kubectl get pvc -n ch3

NAME                           STATUS   VOLUME                CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mypd-statefulset-nginx-ch3-0   Bound    statefulset-pv0-ch3   10Mi       RWO            nfs            3m28s

student@master:~$ kubectl get pv -n ch3

NAME                  CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM                              STORAGECLASS   REASON   AGE
statefulset-pv0-ch3   10Mi       RWO            Retain           Bound       ch3/mypd-statefulset-nginx-ch3-0   nfs                     4m54s
statefulset-pv1-ch3   10Mi       RWO            Retain           Available                                      nfs                     4m54s
statefulset-pv2-ch3   10Mi       RWO            Retain           Available                                      nfs                     4m54s
statefulset-pv3-ch3   10Mi       RWO            Retain           Available                                      nfs                     4m54s
```

4- Check that the pod is using its defined persistent storage. It has to have created a file in the nfs server located at /data/<student-number> with the name LOG_statefulset-nginx-ch3-**X**.txt, being X the number of the replica, being in this case 0 because there is only one replica deployed.
```
student@nfsserver:~$ tail -f /data/<student-number>/LOG_statefulset-nginx-ch3-0.txt

 C_NAME:statefulset-nginx-ch3-0 NginxV:1.17.10 C_IP:11.11.190.159 W_NAME:worker1
```

5- As well as deployments, statefulset can also be updated.
We're going to update nginx's version 1.17.10 to the current version 1.18.0.
The procedure is the same as we already used with the deployments.
```
student@master:~$ kubectl set image sts/statefulset-nginx-ch3 nginx=nginx:1.18.0 --record=true -n ch3

statefulset.apps/statefulset-nginx-ch3 image updated

student@master:~$ kubectl rollout status sts/statefulset-nginx-ch3 -n ch3

Waiting for partitioned roll out to finish: 0 out of 1 new pods have been updated...
Waiting for 1 pods to be ready...
Waiting for 1 pods to be ready...
partitioned roll out complete: 1 new pods have been updated...
```

6- Check that the version of nginx has been updated by checking the content of the file located in the nfs server /data/<student-number>/LOG_statefulset-nginx-ch3-0.txt
```
student@nfsserver:~$ tail -f /data/<student-number>/LOG_statefulset-nginx-ch3-0.txt

 C_NAME:statefulset-nginx-ch3-0 NginxV:1.18.0 C_IP:11.11.190.159 W_NAME:worker1
```

7- Increase the number of statefulset replicas to 4.
```
student@master:~$ kubectl scale --replicas=4 sts/statefulset-nginx-ch3 -n ch3

statefulset.apps/statefulset-nginx-ch3 scaled

student@master:~$ kubectl get pod -n ch3

NAME                      READY   STATUS    RESTARTS   AGE
statefulset-nginx-ch3-0   1/1     Running   0          19m
statefulset-nginx-ch3-1   1/1     Running   0          52s
statefulset-nginx-ch3-2   1/1     Running   0          45s
statefulset-nginx-ch3-3   1/1     Running   0          39s
```

8- Check, through the files in the nfs server, that each pod has created its own LOG_statefulset-nginx-ch3-**X**.txt file and that the nginx version of the new mirrors is the new one, i.e. 1.18.0.
```
student@nfsserver:~$ ls -l /data/<student-number>/

total 28
-rw-r--r-- 1 nobody   nogroup  7985 Jun  4 08:47 LOG_statefulset-nginx-ch3-0.txt
-rw-r--r-- 1 nobody   nogroup  2940 Jun  4 08:40 LOG_statefulset-nginx-ch3-1.txt
-rw-r--r-- 1 nobody   nogroup  2842 Jun  4 08:40 LOG_statefulset-nginx-ch3-2.txt
-rw-r--r-- 1 nobody   nogroup  2744 Jun  4 08:40 LOG_statefulset-nginx-ch3-3.txt

student@nfsserver:~$ tail -f /data/<student-number>/LOG_statefulset-nginx-ch3-3.txt

 C_NAME:statefulset-nginx-ch3-3 NginxV:1.18.0 C_IP:11.11.218.202 W_NAME:worker2 


 C_NAME:statefulset-nginx-ch3-3 NginxV:1.18.0 C_IP:11.11.218.202 W_NAME:worker2
```

9- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch3

student@master:~$ kubectl delete pvc --all -n ch3

student@master:~$ kubectl delete pv --all -n ch3
```

---

## LAB7 - DAEMONSETS

In this lab we will work with daemonsets.
We will deploy a daemonset and we will check how it deploys automatically over a "new" worker node of the cluster.

1- Check the existing daemonsets in the cluster.
As you can see, the defined ones are that it creates the k8s installation itself.
```
student@master:~$ kubectl get ds -A

NAMESPACE     NAME          DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR                 AGE
kube-system   calico-node   3         3         3       3            3           beta.kubernetes.io/os=linux   99d
kube-system   kube-proxy    3         3         3       3            3           beta.kubernetes.io/os=linux   99d

student@master:~$ kubectl get pod -n kube-system -o wide|grep -e calico-node -e kube-proxy

calico-node-9mb9m                                1/1     Running   6          99d     192.168.56.150   master1   <none>           <none>
calico-node-tvjgs                                1/1     Running   7          99d     192.168.56.151   worker1   <none>           <none>
calico-node-xqxdc                                1/1     Running   7          99d     192.168.56.152   worker2   <none>           <none>
kube-proxy-8qlgj                                 1/1     Running   4          7d22h   192.168.56.151   worker1   <none>           <none>
kube-proxy-hs7n8                                 1/1     Running   4          7d22h   192.168.56.152   worker2   <none>           <none>
kube-proxy-lm5p9                                 1/1     Running   1          7d22h   192.168.56.150   master1   <none>           <none>
```

2- Check the yaml file for the daemonset definition that we are going to deploy.
```
student@master:~$ cat<<EOF>daemonset-alpine.yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    app: ds-alpine-ch3
  name: ds-alpine-ch3
  namespace: ch3
spec:
  selector:
    matchLabels:
      app: ds-alpine-ch3
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: ds-alpine-ch3
    spec:
      containers:
      - image: alpine
        name: alpine
        env:
        - name: IP_POD
          valueFrom:
            fieldRef:
              fieldPath: status.podIP 
        - name: WORKER_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: WORKER_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        command: ["/bin/sh"]
        args: ["-c", "echo '\n' CNAME:\$HOSTNAME CIP:\$IP_POD WNAME:\$WORKER_NAME WIP:\$WORKER_IP '\n';sleep 3600"]
EOF
```

3- Check that nothing is deployed in the ch3 namespace and then deploy the daemonset through the yaml file.
```
student@master:~$ kubectl get all -n ch3

No resources found in ch3 namespace.

student@master:~$ kubectl apply -f daemonset-alpine.yaml
```

4- Check the deployment of the daemonset. There must be a damemonset deployed in the ch3 namespace and a daemonset pod in each of the worker nodes.
```
student@master:~$ kubectl get ds -n ch3

NAME            DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
ds-alpine-ch3   2         2         2       2            2           <none>          2m35s

student@master:~$ kubectl get pod -n ch3 -o wide

NAME                  READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
ds-alpine-ch3-mp7nf   1/1     Running   0          16s   11.11.218.216   worker2   <none>           <none>
ds-alpine-ch3-pv77l   1/1     Running   0          16s   11.11.190.172   worker1   <none>           <none>
```

5- Even if we delete any or all of the pods created by the daemonset, the daemonset itself will redeploy the deleted pods to the appropriate worker nodes.
```
student@master:~$ kubectl delete pod ds-alpine-ch3-pv77l -n ch3

pod "ds-alpine-ch3-pv77l" deleted

student@master:~$ kubectl get pod -n ch3 -o wide

NAME                  READY   STATUS    RESTARTS   AGE     IP              NODE                     NOMINATED NODE   READINESS GATES
ds-alpine-ch3-5mjkg   1/1     Running   0          7s      11.11.190.162   worker1   <none>           <none>
ds-alpine-ch3-mp7nf   1/1     Running   0          4m43s   11.11.218.216   worker2   <none>           <none>
```

6- Remove the deployed daemon set.
```
student@master:~$ kubectl delete ds ds-alpine-ch3 -n ch3

daemonset.apps "ds-alpine-ch3" deleted

student@master:~$ kubectl get ds -n ch3

No resources found in ch3 namespace.
```

7- Connect to the worker1 node and stop the docker service.
```
ssh student@<ip worker1>

sudo systemctl stop docker

sudo systemctl status docker 

 docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Thu 2020-06-04 10:41:53 CEST; 19s ago
     Docs: https://docs.docker.com
  Process: 1288 ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock (code=exited, status=0/SUCCESS)
 Main PID: 1288 (code=exited, status=0/SUCCESS)

Jun 04 10:39:18 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:39:18.459741125+02:00" level=info msg="ignoring event" module=libcontainerd name
Jun 04 10:41:53 ubs1804-k8s117-worker1 systemd[1]: Stopping Docker Application Container Engine...
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.173763764+02:00" level=info msg="Processing signal 'terminated'"
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.250666547+02:00" level=info msg="ignoring event" module=libcontainerd name
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.259848245+02:00" level=info msg="ignoring event" module=libcontainerd name
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.261350821+02:00" level=info msg="ignoring event" module=libcontainerd name
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.283881135+02:00" level=info msg="ignoring event" module=libcontainerd name
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.516670485+02:00" level=info msg="stopping event stream following graceful 
Jun 04 10:41:53 ubs1804-k8s117-worker1 dockerd[1288]: time="2020-06-04T10:41:53.516891246+02:00" level=info msg="Daemon shutdown complete"
Jun 04 10:41:53 ubs1804-k8s117-worker1 systemd[1]: Stopped Docker Application Container Engine.
```

8- Deploy the daemonset again using the yaml definition file for this. here will be only one pod deployed in the only operative worker node (worker2).
```

student@master:~$ cat<<EOF>daemonset-alpine.yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    app: ds-alpine-ch3
  name: ds-alpine-ch3
  namespace: ch3
spec:
  selector:
    matchLabels:
      app: ds-alpine-ch3
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: ds-alpine-ch3
    spec:
      containers:
      - image: alpine
        name: alpine
        env:
        - name: IP_POD
          valueFrom:
            fieldRef:
              fieldPath: status.podIP 
        - name: WORKER_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: WORKER_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        command: ["/bin/sh"]
        args: ["-c", "echo '\n' CNAME:\$HOSTNAME CIP:\$IP_POD WNAME:\$WORKER_NAME WIP:\$WORKER_IP '\n';sleep 3600"]
EOF



student@master:~$ kubectl apply -f daemonset-alpine.yaml

student@master:~$ kubectl pod -n ch3 -o wide

NAME                  READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
ds-alpine-ch3-v4k7q   1/1     Running   0          12s   11.11.218.226   worker2   <none>           <none>
```

9- Start the docker service in the worker1 node and check how a daemonset is automatically deployed in that worker.
```
ssh student@<ip worker1>

sudo systemctl start docker

sudo systemctl status docker 

● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2020-06-04 10:44:10 CEST; 44min ago
     Docs: https://docs.docker.com
 Main PID: 4968 (dockerd)
    Tasks: 18
   CGroup: /system.slice/docker.service
           └─4968 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Jun 04 10:44:09 ubs1804-k8s117-worker1 dockerd[4968]: time="2020-06-04T10:44:09.670684760+02:00" level=info msg="Loading containers: start."
Jun 04 10:44:09 ubs1804-k8s117-worker1 dockerd[4968]: time="2020-06-04T10:44:09.802076557+02:00" level=info msg="Default bridge (docker0) is assigned with
Jun 04 10:44:09 ubs1804-k8s117-worker1 dockerd[4968]: time="2020-06-04T10:44:09.838782950+02:00" level=info msg="Loading containers: done."
Jun 04 10:44:10 ubs1804-k8s117-worker1 dockerd[4968]: time="2020-06-04T10:44:10.303896501+02:00" level=info msg="Docker daemon" commit=9d988398e7 graphdri
Jun 04 10:44:10 ubs1804-k8s117-worker1 dockerd[4968]: time="2020-06-04T10:44:10.303947610+02:00" level=info msg="Daemon has completed initialization"
Jun 04 10:44:10 ubs1804-k8s117-worker1 dockerd[4968]: time="2020-06-04T10:44:10.545252671+02:00" level=info msg="API listen on /var/run/docker.sock"
Jun 04 10:44:10 ubs1804-k8s117-worker1 systemd[1]: Started Docker Application Container Engine.

student@master:~$ kubectl pod -n ch3 -o wide

NAME                  READY   STATUS              RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
ds-alpine-ch3-n474m   0/1     ContainerCreating   0          2s    <none>          worker1   <none>           <none>
ds-alpine-ch3-v4k7q   1/1     Running             0          52s   11.11.218.226   worker2   <none>           <none>
```

10- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch3

pod "ds-alpine-ch3-n474m" deleted
pod "ds-alpine-ch3-v4k7q" deleted
daemonset.apps "ds-alpine-ch3" deleted
```

---

## LAB8 - SERVICES

In this lab we are going to work with the services.
We are going to create different types of services **(clusterip, nodeport, headless)** and we are going to deploy a deployment and a pod to test the services we deploy.
**The ips and names of the items will be different from those in the commands here.**

1- We're going to start by deploying an nginx deployment, which will be the target of the services we create, and a busybox pod that we'll use for connection and dns resolution testing of the services.
The deployment will be done with the file yaml *deploy-nginx-hello-txt.yaml* and the *busybox* pod will be deployed from the command line.
```
student@master:~$ cat<<EOF>deploy-nginx-hello-txt.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: my-nginx-ch3
  name: my-nginx-ch3-lab8
  namespace: ch3
spec:
  replicas: 1
  selector:
    matchLabels:
      app: my-nginx-ch3
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: my-nginx-ch3
    spec:
      containers:
      - image: nginxdemos/hello:plain-text
        name: nginx-ch3-lab8
EOF



student@master:~$ kubectl apply -f deploy-nginx-hello-txt.yaml

student@master:~$ kubectl run -n ch3 busybox --image=radial/busyboxplus:curl --restart=Never -- sleep 7200

student@master:~$ kubectl get pod -n ch3 -o wide

NAME                                 READY   STATUS    RESTARTS   AGE   IP              NODE                     NOMINATED NODE   READINESS GATES
busybox                              1/1     Running   0          24s   11.11.190.154   worker1   <none>           <none>
my-nginx-ch3-lab8-868f6c7795-hlbjl   1/1     Running   0          45s   11.11.190.157   worker1   <none>           <none>
```

2- Run a curl from the busybox pod to the ip of the nginx deployment container to check that nginx is working correctly. 
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://11.11.190.157

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:10:04:36 +0000
URI: /
Request ID: f2af11315a2b948c2eced1cce5aa4c84
```

3- Deploy the 3 types of service with which we will perform the laboratory: *clusterIP, nodePort, headless*.
For it, use the 3 yaml files where the services are defined: *service-cip-nginx.yaml, service-nodeport-nginx.yaml, service-headless-nginx.yaml*
Once deployed, we check their status in the cluster.
```
student@master:~$ cat<<EOF>service-cip-nginx.yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: svc-cip-nginx
  name: svc-cip-nginx
  namespace: ch3
spec:
  ports:
  - name: http
    port: 8080
    protocol: TCP
    targetPort: 80
  selector:
    app: my-nginx-ch3
  type: ClusterIP
EOF



student@master:~$ kubectl apply -f service-cip-nginx.yaml

service/svc-cip-nginx created



student@master:~$ cat<<EOF>service-nodeport-nginx.yaml
kind: Service 
apiVersion: v1 
metadata:
  name: svc-nodeport-nginx 
  namespace: ch3
spec:
  type: NodePort
  selector:
    app: my-nginx-ch3
  ports:
    - nodePort: 30090
      port: 8090 
      targetPort: 80
EOF


student@master:~$ kubectl apply -f service-nodeport-nginx.yaml

service/svc-nodeport-nginx created


student@master:~$ cat<<EOF>service-headless-nginx.yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: svc-headless-nginx
  name: svc-headless-nginx
  namespace: ch3
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: my-nginx-ch3
  type: ClusterIP
  clusterIP: None
EOF


student@master:~$ kubectl apply -f service-headless-nginx.yaml

service/svc-headless-nginx created

student@master:~$ kubectl get svc -n ch3

NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
svc-cip-nginx        ClusterIP   10.107.197.111   <none>        8080/TCP         107s
svc-headless-nginx   ClusterIP   None             <none>        80/TCP           28s
svc-nodeport-nginx   NodePort    10.110.21.25     <none>        8090:30090/TCP   70s
```

4- Check that each one of the services has an endpoint, whose ip corresponds to the ip of the pod of the deployment.
```
student@master:~$ kubectl get pod -n ch3 -o wide

NAME                                 READY   STATUS    RESTARTS   AGE     IP              NODE                     NOMINATED NODE   READINESS GATES
busybox                              1/1     Running   0          99m     11.11.190.154   worker1   <none>           <none>
my-nginx-ch3-lab8-868f6c7795-hlbjl   1/1     Running   0          99m     11.11.190.157   worker1   <none>           <none>

student@master:~$ kubectl get ep -n ch3

NAME                 ENDPOINTS                                            AGE
svc-cip-nginx        11.11.190.157:80   71m
svc-headless-nginx   11.11.190.157:80   70m
svc-nodeport-nginx   11.11.190.157:80   71m
```


5- The service type clusterip has to be accessible from inside the cluster of kubernetes, as much by the ip of the service as by the fqdn name of the service, and from any of the nodes that form the cluster by the ip of the service.
To check it, we are going to execute a curl from the pod buysbox and from the worker1 node.

5.1- Checking from the Busyboxy Pod.
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://10.107.197.111:8080/SVC-IP

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:10:49:08 +0000
URI: /SVC-IP
Request ID: c2fba7e1c9aed618381922d669d8b17b


student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://svc-cip-nginx.ch3.svc.cluster.local:8080/SVC-DNS-NAME

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:10:50:56 +0000
URI: /SVC-DNS-NAME
Request ID: 5451ebd8ae8cfc87743e2098b776c27a
```

5.2- Checking from the worker1 node.
```
student@master:~$ ssh student@worker1

student@worker1:~$ curl http://10.107.197.111:8080/SVC-IP-FROM-WORKER1

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:10:52:42 +0000
URI: /SVC-IP-FROM-WORKER1
Request ID: 17ea14ca3dc5b24ff208d67ccb655473

student@worker1:~$ curl http://svc-cip-nginx.ch3.svc.cluster.local:8080/SVC-DNS-NAME-FROM-WORKER1

curl: (6) Could not resolve host: svc-cip-nginx.ch3.svc.cluster.local
```

6- Check the resolution of the service name *svc-cip-nginx* from the busybox pod, with the complete fqdn format used by kubernetes.
```
student@master:~$ kubectl exec -n ch3 -ti busybox nslookup svc-cip-nginx.ch3.svc.cluster.local

Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      svc-cip-nginx.ch3.svc.cluster.local
Address 1: 10.107.197.111 svc-cip-nginx.ch3.svc.cluster.local
```

7- The service type nodeport has to be accessible from inside the cluster of kubernetes, both by the ip of the service and by the name fqdn of the service, from any of the nodes that form the cluster by the ip of the service and from any place outside the cluster that has access to the ips of the nodes of the cluster and to the port exposed by the service.
To check it, we are going to execute a curl from the pod buysbox and from the worker1 node and from an external node to the cluster of kubernetes.

7.1- Checking from the Busyboxy Pod.
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://10.110.21.25:8090/SVC-NODEPORT-IP

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:11:07:20 +0000
URI: /SVC-NODEPORT-IP
Request ID: b8a09ed786fe5c4f9d5bb366f80b7a5c

student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://svc-nodeport-nginx.ch3.svc.cluster.local:8090/SVC-NODEPORT-DNS-NAME

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:11:09:02 +0000
URI: /SVC-NODEPORT-DNS-NAME
Request ID: 9bce9aad4e70036e9e74fbeb6e725974
```

7.2- Checking from the worker1 node.
```
student@master:~$ ssh student@worker1

student@worker1:~$ curl http://10.110.21.25:8090/SVC-NODEPORT-IP-FROM-WORKER1

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:11:13:12 +0000
URI: /SVC-NODEPORT-IP-FROM-WORKER1
Request ID: afc23da4ee21e3bfcace5e61c3b4475b
```

7.3- Checking from my pc.
```
root@mypc:~$ curl http://<ip worker1>:30090/SVC-NODEPORT-IP-FROM-MYPC

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:11:16:31 +0000
URI: /SVC-NODEPORT-IP-FROM-MYPC
Request ID: 42842dc144e26a00089387aad15f6f70
```

8- Check the resolution of the service name *svc-nodeport-nginx* from the busybox pod, with the complete fqdn format used by kubernetes.
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- nslookup svc-nodeport-nginx.ch3.svc.cluster.local

Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      svc-nodeport-nginx.ch3.svc.cluster.local
Address 1: 10.110.21.25 svc-nodeport-nginx.ch3.svc.cluster.local
```

9- The headless service, in principle, will only be accessible from within the cluster of kubernetes and only by name resolution, since this service does not have ip.
The headless service redirects requests to any of the ips of the pods that are part of that service. The service-pod relationship is established through labels selectors.
To check this we will run a curl from the buysbox pod.

9.1- Checking from the Busyboxy Pod.
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://svc-headless-nginx.ch3.svc.cluster.local

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:11:25:56 +0000
URI: /
Request ID: 1a793a268f989e90ed43ff7888deb82b
```

9.1- Checking name resolution from the Busyboxy Pod.
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- nslookup svc-headless-nginx.ch3.svc.cluster.local

Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      svc-headless-nginx.ch3.svc.cluster.local
Address 1: 11.11.190.157 11-11-190-157.svc-cip-nginx.ch3.svc.cluster.local
```

10- Scale the deploy so that it has 3 replicas.
```
student@master:~$ kubectl scale --replicas=3 deploy/my-nginx-ch3-lab8 -n ch3

deployment.apps/my-nginx-ch3-lab8 scaled
```
11- Check that each one of the services now has 3 endpoints, whose ips correspond to the ips of the pods of the deployment.
```
student@master:~$ kubectl get pod -n ch3 -o wide

NAME                                 READY   STATUS    RESTARTS   AGE     IP              NODE                     NOMINATED NODE   READINESS GATES
busybox                              1/1     Running   0          99m     11.11.190.154   worker1   <none>           <none>
my-nginx-ch3-lab8-868f6c7795-g2vbp   1/1     Running   0          4m47s   11.11.190.175   worker1   <none>           <none>
my-nginx-ch3-lab8-868f6c7795-hlbjl   1/1     Running   0          99m     11.11.190.157   worker1   <none>           <none>
my-nginx-ch3-lab8-868f6c7795-qt2vc   1/1     Running   0          4m47s   11.11.218.229   worker2   <none>           <none>

student@master:~$ kubectl get ep -n ch3

NAME                 ENDPOINTS                                            AGE
svc-cip-nginx        11.11.190.157:80,11.11.190.175:80,11.11.218.229:80   71m
svc-headless-nginx   11.11.190.157:80,11.11.190.175:80,11.11.218.229:80   70m
svc-nodeport-nginx   11.11.190.157:80,11.11.190.175:80,11.11.218.229:80   71m
```

12- Perform the checks of the previous exercises to verify that each check returns a different ip (from each of the 3 pods that are now part of the deployment).

For example, checking headless service.
```
student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://svc-headless-nginx.ch3.svc.cluster.local

Server address: 11.11.218.229:80
Server name: my-nginx-ch3-lab8-868f6c7795-qt2vc
Date: 04/Jun/2020:11:52:08 +0000
URI: /
Request ID: 67314bed655da66468cc336f57c362cf

student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://svc-headless-nginx.ch3.svc.cluster.local

Server address: 11.11.190.175:80
Server name: my-nginx-ch3-lab8-868f6c7795-g2vbp
Date: 04/Jun/2020:11:52:10 +0000
URI: /
Request ID: 59543aeeab26ae04e4f00407d6e2c006

student@master:~$ kubectl exec -n ch3 -ti busybox -- curl http://svc-headless-nginx.ch3.svc.cluster.local

Server address: 11.11.190.157:80
Server name: my-nginx-ch3-lab8-868f6c7795-hlbjl
Date: 04/Jun/2020:11:52:11 +0000
URI: /
Request ID: 55877e39847092d08cd4d677f952520c
```

13- Remove all the elements deployed in this lab.
```
student@master:~$ kubectl delete all --all -n ch3

pod "busybox" deleted
pod "my-nginx-ch3-lab8-868f6c7795-g2vbp" deleted
pod "my-nginx-ch3-lab8-868f6c7795-hlbjl" deleted
pod "my-nginx-ch3-lab8-868f6c7795-qt2vc" deleted
service "svc-cip-nginx" deleted
service "svc-headless-nginx" deleted
service "svc-nodeport-nginx" deleted
deployment.apps "my-nginx-ch3-lab8" deleted
replicaset.apps "my-nginx-ch3-lab8-868f6c7795" deleted
```


 

