## This example illustrates Docker Engine running in 'rootless' mode.

>NOTE: Docker Engine Rootless Mode (Rootless mode was introduced in Docker Engine 19.03.)

1 - We will install Docker Engine in 'rootless' mode, because it has a different binary:
``
$ curl -fsSL https://get.docker.com/rootless | sh
``
2 - We will start Docker daemon process:
```
$ export PATH=/home/vagrant/bin:$PATH
$ export DOCKER_HOST=unix:///run/user/1000/docker.sock
$ systemctl --user start docker
```
3 - We can review current containers before executing a quick test:
```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
4 - We will then execute a simple Nginx container exposing its service on high ports:
```
$  export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock
$ docker run -d -P nginx:alpine
```
5 - We can verify that Docker deploys everything under user's home directory:
```
$ ls -lart ~/.local/share/docker
total 60
drwx--x--x  3 vagrant vagrant 4096 Apr  1 10:56 ..
drwx------  3 vagrant vagrant 4096 Apr  1 10:56 containerd
drwx------  2 vagrant vagrant 4096 Apr  1 10:56 runtimes
drwx------  4 vagrant vagrant 4096 Apr  1 10:56 plugins
drwx------  2 vagrant vagrant 4096 Apr  1 10:56 volumes
drwx------  3 vagrant vagrant 4096 Apr  1 10:56 image
drwx------  2 vagrant vagrant 4096 Apr  1 10:56 trust
drwxr-x---  3 vagrant vagrant 4096 Apr  1 10:56 network
drwx------  2 vagrant vagrant 4096 Apr  1 10:56 swarm
drwx------  2 vagrant vagrant 4096 Apr  1 10:56 builder
drwx--x--x 15 vagrant vagrant 4096 Apr  1 10:56 .
drwx--x--x  4 vagrant vagrant 4096 Apr  1 10:56 buildkit
drwx------  2 vagrant vagrant 4096 Apr  1 10:59 tmp
drwx------  7 vagrant vagrant 4096 Apr  1 10:59 overlay2
drwx------  3 vagrant vagrant 4096 Apr  1 10:59 containers
```
6 - Our Nginx contaienr will be up and running as we expected:
```
$ docker container ls
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                   NAMES
3de885bcb167        nginx:alpine        "nginx -g 'daemon of…"   3 minutes ago       Up 3 minutes        0.0.0.0:32768->80/tcp   sharp_bohr
```
7 - We can access Nginx servce's port:
```
$ curl -I 0.0.0.0:32768
HTTP/1.1 200 OK
Server: nginx/1.17.9
Date: Wed, 01 Apr 2020 09:02:58 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 03 Mar 2020 17:36:53 GMT
Connection: keep-alive
ETag: "5e5e95b5-264"
Accept-Ranges: bytes
```
8 - We notice that Nginx processes will run as our __non-root__ user (vagrant in this example).
```
$ ps -ef|grep nginx
vagrant   5117  5099  0 10:59 ?        00:00:00 nginx: master process nginx -g daemon off;
165636    5146  5117  0 10:59 ?        00:00:00 nginx: worker process
165636    5147  5117  0 10:59 ?        00:00:00 nginx: worker process
vagrant   5336  4477  0 11:03 pts/0    00:00:00 grep --color=auto nginx
```


>NOTE:
>
>Here is a list of known 'rootless' mode limitations:
> - Only vfs graphdriver is supported. However, on Ubuntu and Debian 10, overlay2 and overlay are also supported.
> - Following features are not supported:
>   - Cgroups (including docker top, which depends on the cgroups)
>   - AppArmor
>   - Checkpoint
>   - Overlay network
>   - Exposing SCTP ports
>   - To use ping command, see Routing ping packets
>   - To expose privileged TCP/UDP ports (< 1024), see Exposing privileged ports
>
>


