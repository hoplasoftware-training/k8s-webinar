# In this Lab:
- We will review some __Podman__ and __Buildah__ features.

- We will learn how __runc__ runs a simple container.

- We can also learn about new [Docker Engine 'RootLess' mode](DockerRootlessMode.md).

---
---



# Podman and Buildah Install

```
sudo apt-get update -qq
sudo apt-get install -qq -y software-properties-common
sudo add-apt-repository -y ppa:projectatomic/ppa
sudo apt-get update -qq
sudo apt-get -qq -y install buildah podman
```

# Install RunC
```
$ sudo apt-get install -qq runc
```

# Configure registries

Create following entries in __/etc/containers/registries.conf__.
```
[registries.search]
registries = ['docker.io']
```


# Build an image from a Dockerfile
We can use either podmn or buildah for creating images. In this example we will use buildah.
1 - First, we will create simpltest.Dockerfile file with following content:

```
FROM alpine:latest
RUN apk add --update --no-cache curl httpie \
&& rm -rf /var/cache/apk/*
```
2 - We will then build a container image using __build-using-dockerfile__ argument:
```
$ buildah build-using-dockerfile --file simpletest.Dockerfile -t simpletest .
STEP 1: FROM alpine:latest
STEP 2: RUN apk add --update --no-cache curl httpie && rm -rf /var/cache/apk/*
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/22) Installing ca-certificates (20191127-r1)
(2/22) Installing nghttp2-libs (1.40.0-r0)
(3/22) Installing libcurl (7.67.0-r0)
(4/22) Installing curl (7.67.0-r0)
(5/22) Installing libbz2 (1.0.8-r1)
(6/22) Installing expat (2.2.9-r1)
(7/22) Installing libffi (3.2.1-r6)
(8/22) Installing gdbm (1.13-r1)
(9/22) Installing xz-libs (5.2.4-r0)
(10/22) Installing ncurses-terminfo-base (6.1_p20200118-r2)
(11/22) Installing ncurses-libs (6.1_p20200118-r2)
(12/22) Installing readline (8.0.1-r0)
(13/22) Installing sqlite-libs (3.30.1-r1)
(14/22) Installing python3 (3.8.2-r0)
(15/22) Installing py3-chardet (3.0.4-r3)
(16/22) Installing py3-idna (2.8-r3)
(17/22) Installing py3-certifi (2019.9.11-r2)
(18/22) Installing py3-urllib3 (1.25.7-r1)
(19/22) Installing py3-requests (2.22.0-r0)
(20/22) Installing py3-setuptools (42.0.2-r0)
(21/22) Installing py3-pygments (2.5.2-r0)
(22/22) Installing httpie (1.0.3-r1)
Executing busybox-1.31.1-r9.trigger
Executing ca-certificates-20191127-r1.trigger
OK: 79 MiB in 36 packages
STEP 3: COMMIT simpletest
Getting image source signatures
Copying blob beee9f30bc1f skipped: already exists
Copying blob 28a59282d6c2 done
Copying config 35dc39add2 done
Writing manifest to image destination
Storing signatures
35dc39add291e254c02692020a3e5fd0a446ca1326eeefa96de10ad0770db73d
```

# Execute a simple container with the image just created

1 - We can use either podmn or buildah for creating images. Containers must be created using podman.
```
$ podman container run --name test simpletest ls -lrt /
total 12
drwxrwxrwt    2 root     root             6 Mar 23 20:12 tmp
drwxr-xr-x   12 root     root           137 Mar 23 20:12 var
drwxr-xr-x    2 root     root             6 Mar 23 20:12 srv
drwxr-xr-x    2 root     root          4096 Mar 23 20:12 sbin
drwx------    2 root     root             6 Mar 23 20:12 root
drwxr-xr-x    2 root     root             6 Mar 23 20:12 opt
drwxr-xr-x    2 root     root             6 Mar 23 20:12 mnt
drwxr-xr-x    5 root     root            44 Mar 23 20:12 media
drwxr-xr-x    5 root     root           185 Mar 23 20:12 lib
drwxr-xr-x    2 root     root             6 Mar 23 20:12 home
drwxr-xr-x    2 root     root          4096 Mar 23 20:12 bin
drwxr-xr-x    2 root     root            27 Apr  3 16:55 run
drwxr-xr-x   17 root     root          4096 Apr  3 16:55 etc
drwxr-xr-x    8 root     root            81 Apr  3 16:55 usr
dr-xr-xr-x   13 nobody   nobody           0 Apr  3 17:43 sys
dr-xr-xr-x  331 nobody   nobody           0 Apr  3 17:43 proc
drwxr-xr-x    5 root     root           340 Apr  3 17:43 dev

```
Notice that we are not running as root. Review graph-driver to notice vfs instead of overlay for users.

>NOTE:
>
>We can push our images to registry, but first we must authenticate...
>```
>$ buildah login docker.io
>Username: frjaraur
>Password: 
>Login Succeeded!
>```
>Then we tag our image with the full regitry repository name:
>```
>$ podman image tag simpletest frjaraur/simpletest:1.0
>```
>Then we will be able to push to docker registry in this case:
>```
>$ buildah push frjaraur/simpletest:1.0
>Getting image source signatures
>Copying blob 575a4a90d308 done  
>Copying blob beee9f30bc1f skipped: already exists  
>Copying config 2b1474772b done  
>Writing manifest to image destination
>Storing signatures
>```

# Execute a container with runc
1 - Export test container root fs:
```
$ podman export test -o simpletest_rootfs.tar
```

This will be our simplest image rootfs.

2 - We will extract its content in simpletest_rootfs directory :
````
$ mkdir -p runc/rootfs
$ mkdir cd runc
$ tar -xf simpletest_rootfs.tar -C ./rootfs
````


3 - Next, we will generate a sample runc specications file (c__onfig.json__) in __'rootless'__ mode:
```
$ runc spec --rootless
```

4 - We are now ready to execute a simple test container using runc and previously generated roofs:
```
$ runc run simpletest
/ # 
```

>## NOTE:
>
>We can add a test file inside roofs directory and verify it exists on container:
>```
>$ touch rootfs/home/TESFILE
>$ runc run simpletest
>/ # ls -lart /home/TESFILE
>-rw-rw-r--    1 root     root             0 Apr  3 17:32 /home/TESFILE
>/ # 
>```



