# Chapter 1
In these labs we will cover all the topics learned on Chapter1.
- We will learn basic Dockerfile keys.

- We will review some __Podman__ and __Buildah__ features.

- We will learn how __runc__ runs a simple container.

---

## Lab1 - Creating images with buildah

This lab will show us how to create images using ___buildah___.

1 - First we will install Podman and Buildah packages on our __"standalone"__ host.

```
student@standalone:~$ sudo apt-get update -qq
student@standalone:~$ sudo apt-get install -qq -y software-properties-common
student@standalone:~$ sudo add-apt-repository -y ppa:projectatomic/ppa
student@standalone:~$ sudo apt-get update -qq
student@standalone:~$ sudo apt-get -qq -y install buildah podman
```

2 - Install RunC
```
student@standalone:~$ sudo apt-get install -qq runc
```

3 - Configure registries
Create following entries in __/etc/containers/registries.conf__.
```
student@standalone:~$ cat <<-EOF|sudo tee -a /etc/containers/registries.conf
[registries.search]
registries = ['docker.io']
EOF
```

4 - Build an image from a Dockerfile
We can use either podmn or buildah for creating images. In this example we will use buildah.
1 - First, we will create [simpletest.Dockerfile](./simpletest.Dockerfile) file with following content:

```
FROM alpine:latest
RUN apk add --update --no-cache curl httpie \
&& rm -rf /var/cache/apk/*
```

5 - We will then build a container image using __build-using-dockerfile__ argument:
```
$ buildah build-using-dockerfile --file  simpletest.Dockerfile -t simpletest .
STEP 1: FROM alpine:latest
STEP 2: RUN apk add --update --no-cache curl httpie && rm -rf /var/cache/apk/*
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/22) Installing ca-certificates (20191127-r1)
(2/22) Installing nghttp2-libs (1.40.0-r0)
(3/22) Installing libcurl (7.67.0-r0)
(4/22) Installing curl (7.67.0-r0)
(5/22) Installing libbz2 (1.0.8-r1)
(6/22) Installing expat (2.2.9-r1)
(7/22) Installing libffi (3.2.1-r6)
(8/22) Installing gdbm (1.13-r1)
(9/22) Installing xz-libs (5.2.4-r0)
(10/22) Installing ncurses-terminfo-base (6.1_p20200118-r2)
(11/22) Installing ncurses-libs (6.1_p20200118-r2)
(12/22) Installing readline (8.0.1-r0)
(13/22) Installing sqlite-libs (3.30.1-r1)
(14/22) Installing python3 (3.8.2-r0)
(15/22) Installing py3-chardet (3.0.4-r3)
(16/22) Installing py3-idna (2.8-r3)
(17/22) Installing py3-certifi (2019.9.11-r2)
(18/22) Installing py3-urllib3 (1.25.7-r1)
(19/22) Installing py3-requests (2.22.0-r0)
(20/22) Installing py3-setuptools (42.0.2-r0)
(21/22) Installing py3-pygments (2.5.2-r0)
(22/22) Installing httpie (1.0.3-r1)
Executing busybox-1.31.1-r9.trigger
Executing ca-certificates-20191127-r1.trigger
OK: 79 MiB in 36 packages
STEP 3: COMMIT simpletest
Getting image source signatures
Copying blob beee9f30bc1f skipped: already exists
Copying blob 28a59282d6c2 done
Copying config 35dc39add2 done
Writing manifest to image destination
Storing signatures
35dc39add291e254c02692020a3e5fd0a446ca1326eeefa96de10ad0770db73d
```

In this lab we learned how to build images with ___buildah___. We can alo use ___podman image build___ command-line to create images using ___podman___.

---

## Lab2 - Execute a simple container with the image just created

In this lab we will use ___podman___ to execute containers using the previously created image.

1 - We can use either podmn or buildah for creating images. Containers must be created using podman.
```
$ podman container run --name test simpletest ls -lrt /
total 12
drwxrwxrwt    2 root     root             6 Mar 23 20:12 tmp
drwxr-xr-x   12 root     root           137 Mar 23 20:12 var
drwxr-xr-x    2 root     root             6 Mar 23 20:12 srv
drwxr-xr-x    2 root     root          4096 Mar 23 20:12 sbin
drwx------    2 root     root             6 Mar 23 20:12 root
drwxr-xr-x    2 root     root             6 Mar 23 20:12 opt
drwxr-xr-x    2 root     root             6 Mar 23 20:12 mnt
drwxr-xr-x    5 root     root            44 Mar 23 20:12 media
drwxr-xr-x    5 root     root           185 Mar 23 20:12 lib
drwxr-xr-x    2 root     root             6 Mar 23 20:12 home
drwxr-xr-x    2 root     root          4096 Mar 23 20:12 bin
drwxr-xr-x    2 root     root            27 Apr  3 16:55 run
drwxr-xr-x   17 root     root          4096 Apr  3 16:55 etc
drwxr-xr-x    8 root     root            81 Apr  3 16:55 usr
dr-xr-xr-x   13 nobody   nobody           0 Apr  3 17:43 sys
dr-xr-xr-x  331 nobody   nobody           0 Apr  3 17:43 proc
drwxr-xr-x    5 root     root           340 Apr  3 17:43 dev
```

2 - We can remove "test" container and execute something more interesting.
```
student@standalone:~$ podman container rm test
36dc829c201a370d3d0afd215f365afd3e90bf3e5df7d83097fef584dd3e0116

student@standalone:~$ podman container run -d --name test simpletest ping 8.8.8.8
c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742

student@standalone:~$ podman container ls
CONTAINER ID  IMAGE                        COMMAND       CREATED         STATUS             PORTS  NAMES
c843ffc5befc  localhost/simpletest:latest  ping 8.8.8.8  11 seconds ago  Up 10 seconds ago         test
```

And now we can execute another process using the same namespaces.
```
student@standalone:~$ podman container exec -ti test  ls /
bin    dev    etc    home   lib    media  mnt    opt    proc   root   run    sbin   srv    sys    tmp    usr    var
```

Notice that we are not running as root. 
```
student@standalone:~$ ps -ef|grep ping
student   8782  8772  0 13:20 ?        00:00:00 ping 8.8.8.8
student   8919  5175  0 13:29 pts/0    00:00:00 grep --color=auto ping

student@standalone:~$ ps -ef|grep 8772
student   8772     1  0 13:20 ?        00:00:00 /usr/bin/conmon --api-version 1 -c c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742 -u c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742 -r /usr/sbin/runc -b /home/student/.local/share/containers/storage/vfs-containers/c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742/userdata -p /run/user/1002/vfs-containers/c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742/userdata/pidfile -l k8s-file:/home/student/.local/share/containers/storage/vfs-containers/c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742/userdata/ctr.log --exit-dir /run/user/1002/libpod/tmp/exits --socket-dir-path /run/user/1002/libpod/tmp/socket --log-level error --runtime-arg --log-format=json --runtime-arg --log --runtime-arg=/run/user/1002/vfs-containers/c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742/userdata/oci-log --conmon-pidfile /run/user/1002/vfs-containers/c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742/userdata/conmon.pid --exit-command /usr/bin/podman --exit-command-arg --root --exit-command-arg /home/student/.local/share/containers/storage --exit-command-arg --runroot --exit-command-arg /run/user/1002 --exit-command-arg --log-level --exit-command-arg error --exit-command-arg --cgroup-manager --exit-command-arg cgroupfs --exit-command-arg --tmpdir --exit-command-arg /run/user/1002/libpod/tmp --exit-command-arg --runtime --exit-command-arg runc --exit-command-arg --storage-driver --exit-command-arg vfs --exit-command-arg --events-backend --exit-command-arg journald --exit-command-arg container --exit-command-arg cleanup --exit-command-arg c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742
student   8782  8772  0 13:20 ?        00:00:00 ping 8.8.8.8
student   8923  5175  0 13:29 pts/0    00:00:00 grep --color=auto 8772
```

3 - We can review podman information.
```
student@standalone:~$ podman info
host:
  BuildahVersion: 1.11.3
  CgroupVersion: v1
  Conmon:
    package: 'conmon: /usr/bin/conmon'
    path: /usr/bin/conmon
    version: 'conmon version 2.0.3, commit: unknown'
  Distribution:
    distribution: ubuntu
    version: "18.04"
  IDMappings:
    gidmap:
    - container_id: 0
      host_id: 1002
      size: 1
    - container_id: 1
      host_id: 296608
      size: 65536
    uidmap:
    - container_id: 0
      host_id: 1002
      size: 1
    - container_id: 1
      host_id: 296608
      size: 65536
  MemFree: 2591293440
  MemTotal: 4086132736
  OCIRuntime:
    name: runc
    package: 'runc: /usr/sbin/runc'
    path: /usr/sbin/runc
    version: 'runc version spec: 1.0.1-dev'
  SwapFree: 2065690624
  SwapTotal: 2065690624
  arch: amd64
  cpus: 2
  eventlogger: journald
  hostname: standalone
  kernel: 4.15.0-50-generic
  os: linux
  rootless: true
  slirp4netns:
    Executable: /usr/bin/slirp4netns
    Package: 'slirp4netns: /usr/bin/slirp4netns'
    Version: |-
      slirp4netns version 0.4.2
      commit: unknown
  uptime: 1h 48m 6.77s (Approximately 0.04 days)
registries:
  blocked: null
  insecure: null
  search:
  - docker.io
store:
  ConfigFile: /home/student/.config/containers/storage.conf
  ContainerStore:
    number: 1
  GraphDriverName: vfs
  GraphOptions: {}
  GraphRoot: /home/student/.local/share/containers/storage
  GraphStatus: {}
  ImageStore:
    number: 2
  RunRoot: /run/user/1002
  VolumePath: /home/student/.local/share/containers/storage/volumes
```

4 - We can push our images to registry, but first we must authenticate...
We can use ___podman login___ and ___buildah login___.
```
student@standalone:~$ buildah login docker.io
Username: frjaraur
Password: 
Login Succeeded!
```

or
```
student@standalone:~$ podman login docker.io
Username: frjaraur
Password: 
Login Succeeded!
```

5 - Then we tag our image with the full regitry repository name:
```
student@standalone:~$ podman image tag simpletest frjaraur/simpletest:1.0
```

6 - And now we will be able to push to docker registry in this case:
```
student@standalone:~$ podman image  push frjaraur/simpletest:1.0
Getting image source signatures
Copying blob 3e207b409db3 done
Copying blob 79b530986bcf done
Copying config 31fcacd920 done
Writing manifest to image destination
Storing signatures
```

or 
```
student@standalone:~$  buildah push frjaraur/simpletest:1.0
Getting image source signatures
Copying blob 575a4a90d308 done  
Copying blob beee9f30bc1f skipped: already exists  
Copying config 2b1474772b done  
Writing manifest to image destination
Storing signatures
```

7 - Review where is all containers and images data.
```
student@standalone:~$ ls -lart ~/.local/share/containers/
total 16
drwx------  3 student student 4096 May 25 13:16 ..
drwx------ 10 student student 4096 May 25 13:20 storage
drwx------  4 student student 4096 May 25 14:55 .
drwx------  2 student student 4096 May 25 14:55 cache
```

We can notice that "storage" directory includes containers storage.
```
student@standalone:~$ podman container exec -ti test  touch /testfile

student@standalone:~$ ls -lart ~/.local/share/containers/^C

student@standalone:~$ find ~/.local/share/containers/ -name testfile
/home/student/.local/share/containers/storage/vfs/dir/c7c99321f88019346237045412e456c23c17a1633e1c312a57ae69ebb8496605/testfile
```

8 - Let's start a new container using volumes.
First, we remove previous "test" container.
```
student@standalone:~$ podman container rm -fv test
c843ffc5befc30a322446ea8811cfa0740d8d763269682765d74d4a18b1cf742
```

Now we first create an empty volume.
```
student@standalone:~$ podman volume create test-volume
test-volume
```

And now we use it to simple create "testfile-within-volume" file inside "test-volume" volume.
```
student@standalone:~$ podman container run --rm --name test-with-volume -v test-volume:/test-dir simpletest touch /test-dir/testfile-within-volume
```

Notice that we created "testfile-within-volume" inside "test-volume".
```
student@standalone:~$ find ~/.local/share/containers/ -name testfile-within-volume
/home/student/.local/share/containers/storage/volumes/test-volume/_data/testfile-within-volume
```

As expected, we can run again another container using defined volume and "testfile-within-volume" is still there.
```
student@standalone:~$ podman container run --rm -ti --name new-container -v test-volume:/new-dir simpletest sh
/ # ls -lart /new-dir
total 8
-rw-r--r--    1 root     root             0 May 25 13:08 testfile-within-volume
drwxr-xr-x    2 root     root          4096 May 25 13:08 .
drwxr-xr-x   20 root     root          4096 May 25 13:19 ..
/ # exit
```

In this lab we learned how to run containers using ___podman___. We also review how containers' related files will be distributed in our system because we are running containers as a common user. We are not using root or any daemon to execute them.

---

## Lab3 - Execute a container with runc

In ths lab we will learn how to directly use ___runc___ to execute a container. We will review all the steps required to use this command-line.

1 - Export "test" container root fs:
```
student@standalone:~$ podman export test -o simpletest_rootfs.tar
```

This will be our simplest's image's  rootfs.

2 - We will extract its content in "simpletest_rootfs" directory :
````
student@standalone:~$ podman export test -o simpletest_rootfs.tar

student@standalone:~$ mkdir -p runc/rootfs

student@standalone:~$ cd runc/

student@standalone:~/runc$ tar -xf ../simpletest_rootfs.tar -C ./rootfs
````


3 - Next, we will generate a sample runc specications file __config.json__ in __'rootless'__ mode:
```
student@standalone:~/runc$ runc spec --rootless

student@standalone:~/runc$ ls
config.json  rootfs

student@standalone:~/runc$ cat config.json 
{
        "ociVersion": "1.0.1-dev",
        "process": {
                "terminal": true,
                "user": {
                        "uid": 0,
                        "gid": 0
                },
                "args": [
                        "sh"
                ],
                "env": [
                        "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                        "TERM=xterm"
                ],
                "cwd": "/",
                "capabilities": {
                        "bounding": [
                                "CAP_AUDIT_WRITE",
                                "CAP_KILL",
                                "CAP_NET_BIND_SERVICE"
                        ],
                        "effective": [
                                "CAP_AUDIT_WRITE",
                                "CAP_KILL",
                                "CAP_NET_BIND_SERVICE"
                        ],
                        "inheritable": [
                                "CAP_AUDIT_WRITE",
                                "CAP_KILL",
                                "CAP_NET_BIND_SERVICE"
                        ],
                        "permitted": [
                                "CAP_AUDIT_WRITE",
                                "CAP_KILL",
                                "CAP_NET_BIND_SERVICE"
                        ],
                        "ambient": [
                                "CAP_AUDIT_WRITE",
                                "CAP_KILL",
                                "CAP_NET_BIND_SERVICE"
                        ]
                },
                "rlimits": [
                        {
                                "type": "RLIMIT_NOFILE",
                                "hard": 1024,
                                "soft": 1024
                        }
                ],
                "noNewPrivileges": true
        },
        "root": {
                "path": "rootfs",
                "readonly": true
        },
        "hostname": "runc",
        "mounts": [
                {
                        "destination": "/proc",
                        "type": "proc",
                        "source": "proc"
                },
                {
                        "destination": "/dev",
                        "type": "tmpfs",
                        "source": "tmpfs",
                        "options": [
                                "nosuid",
                                "strictatime",
                                "mode=755",
                                "size=65536k"
                        ]
                },
                {
                        "destination": "/dev/pts",
                        "type": "devpts",
                        "source": "devpts",
                        "options": [
                                "nosuid",
                                "noexec",
                                "newinstance",
                                "ptmxmode=0666",
                                "mode=0620"
                        ]
                },
                {
                        "destination": "/dev/shm",
                        "type": "tmpfs",
                        "source": "shm",
                        "options": [
                                "nosuid",
                                "noexec",
                                "nodev",
                                "mode=1777",
                                "size=65536k"
                        ]
                },
                {
                        "destination": "/dev/mqueue",
                        "type": "mqueue",
                        "source": "mqueue",
                        "options": [
                                "nosuid",
                                "noexec",
                                "nodev"
                        ]
                },
                {
                        "destination": "/sys",
                        "type": "none",
                        "source": "/sys",
                        "options": [
                                "rbind",
                                "nosuid",
                                "noexec",
                                "nodev",
                                "ro"
                        ]
                }
        ],
        "linux": {
                "uidMappings": [
                        {
                                "containerID": 0,
                                "hostID": 1002,
                                "size": 1
                        }
                ],
                "gidMappings": [
                        {
                                "containerID": 0,
                                "hostID": 1002,
                                "size": 1
                        }
                ],
                "namespaces": [
                        {
                                "type": "pid"
                        },
                        {
                                "type": "ipc"
                        },
                        {
                                "type": "uts"
                        },
                        {
                                "type": "mount"
                        },
                        {
                                "type": "user"
                        }
                ],
                "maskedPaths": [
                        "/proc/acpi",
                        "/proc/asound",
                        "/proc/kcore",
                        "/proc/keys",
                        "/proc/latency_stats",
                        "/proc/timer_list",
                        "/proc/timer_stats",
                        "/proc/sched_debug",
                        "/sys/firmware",
                        "/proc/scsi"
                ],
                "readonlyPaths": [
                        "/proc/bus",
                        "/proc/fs",
                        "/proc/irq",
                        "/proc/sys",
                        "/proc/sysrq-trigger"
                ]
        }
}
```

4 - We are now ready to execute a simple test container using runc and previously generated roofs:
```
student@standalone:~/runc$ runc run simpletest
/ # exit
```

From another terminal, we can observe that this container is not managed by podman. We created it manually hence podman doesn't know anything about it.
```
(TERMINAL2) student@standalone:~$ podman container ls
CONTAINER ID  IMAGE  COMMAND  CREATED  STATUS  PORTS  NAMES
```

Notice also that this container is running using host's network namespace.
```
student@standalone:~/runc$ runc run simpletest 
/ # ip add
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN qlen 1000
    link/ether 08:00:27:cd:c7:10 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic eth0
       valid_lft 65189sec preferred_lft 65189sec
    inet6 fe80::a00:27ff:fecd:c710/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN qlen 1000
    link/ether 08:00:27:bc:86:ec brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.10/24 brd 10.10.10.255 scope global eth1
       valid_lft forever preferred_lft forever
    inet 10.10.10.150/24 brd 10.10.10.255 scope global secondary dynamic eth1
       valid_lft 977sec preferred_lft 977sec
    inet6 fe80::a00:27ff:febc:86ec/64 scope link 
       valid_lft forever preferred_lft forever
4: eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN qlen 1000
    link/ether 08:00:27:f4:48:a6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.200.30/24 brd 192.168.200.255 scope global dynamic eth2
       valid_lft 21989sec preferred_lft 21989sec
    inet6 fe80::a00:27ff:fef4:48a6/64 scope link 
       valid_lft forever preferred_lft forever
/ # 
```

>NOTE: We can add a test file inside roofs directory and verify it exists on container:
>```
>$ touch rootfs/home/TESFILE
>$ runc run simpletest
>/ # ls -lart /home/TESFILE
>-rw-rw-r--    1 root     root             0 Apr  3 17:32 /home/TESFILE
>/ # 
>```

Let's modify a bit our __config.json__ file. In this case we will change process behavior and arguments.
```
student@standalone:~/runc$ cat config.json 
{
        "ociVersion": "1.0.1-dev",
        "process": {
                "terminal": false,
                "user": {
                        "uid": 0,
                        "gid": 0
                },
                "args": [
                        "/bin/sleep","300"
                ],
...
...
```

Let's create now this container again using create and start actions to execute it 


```
student@standalone:~/runc$ sudo runc create simpletest 
```

```
student@standalone:~/runc$ sudo runc start simpletest 
```


```
student@standalone:~/runc$ sudo runc list
ID           PID         STATUS      BUNDLE               CREATED                         OWNER
simpletest   11721       running     /home/student/runc   2020-05-25T16:12:43.77328252Z   root

student@standalone:~/runc$ sudo runc state simpletest 
{
  "ociVersion": "1.0.1-dev",
  "id": "simpletest",
  "pid": 11721,
  "status": "running",
  "bundle": "/home/student/runc",
  "rootfs": "/home/student/runc/rootfs",
  "created": "2020-05-25T16:12:43.77328252Z",
  "owner": ""
}
```

Container "simpletest" is running (notice that owner is root).

Now we kill this container:
```
(TERMINAL2) student@standalone:~/runc$ sudo runc kill --all simpletest KILL

(TERMINAL2) student@standalone:~/runc$ sudo runc list
ID           PID         STATUS      BUNDLE               CREATED                         OWNER
simpletest   0           stopped     /home/student/runc   2020-05-25T16:12:43.77328252Z   root

(TERMINAL2) student@standalone:~/runc$ sudo runc state simpletest 
{
  "ociVersion": "1.0.1-dev",
  "id": "simpletest",
  "pid": 0,
  "status": "stopped",
  "bundle": "/home/student/runc",
  "rootfs": "/home/student/runc/rootfs",
  "created": "2020-05-25T16:12:43.77328252Z",
  "owner": ""
}
```

Container is stopped but it still in our system. We can remove this container usind ___runc delete___.

```
student@standalone:~/runc$ sudo runc delete simpletest

student@standalone:~/runc$ sudo runc list
ID          PID         STATUS      BUNDLE      CREATED     OWNER
```

This lab showed us how simple is to run containers as isolated filesystems using a rootfs included in our system as a simple directory.

---

### What we have learned in these labs?.
