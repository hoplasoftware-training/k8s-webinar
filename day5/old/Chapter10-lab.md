# Chapter 10

In these labs we will review some Kubernetes maintenace tasks.

- We will learn how to execute Etcd backup and restore operations.

- We will learn how backup all the resources created within Kubernetes cluster.

- We will have learn how to force maintenance on Kubernetes nodes in order to execute some hardware or operating system related maintenace tasks.


---

## Lab1 - Backup and restore of Etcd.

We will learn how to execute Etcd backup and restore operations. In this case we are executing a one-master-only cluster. 

>NOTE: This lab requires an already running Kubernetes cluster.


1 - First we stop the Kube-APIServer. This will ensure a locked environemnt, without any modifications.

```
root@local-master:~# docker ps|grep api
cb4ce879172b        c5c678ed2546              "kube-apiserver --ad…"   About a minute ago   Up About a minute                       k8s_kube-apiserver_kube-apiserver-local-master_kube-system_4ec8bac8fdebdd69d161e399726bd58e_1
524cbb2fc6d5        k8s.gcr.io/pause:3.1      "/pause"                 About a minute ago   Up About a minute                       k8s_POD_kube-apiserver-local-master_kube-system_4ec8bac8fdebdd69d161e399726bd58e_1
```

Then we will just remove __kube-apiserver.yaml__ from __/etc/kubernetes/manifests/__ manifests directory.
```
root@local-master:~# mv /etc/kubernetes/manifests/kube-apiserver.yaml .
```

Then we check again
```
root@local-master:~# docker ps|grep api
```


2 - We will now create an Etcd backup by executing a snapshot capture of current database. We will simply use official __bitnami/etcd__ image.

Our endpoint will be any __Etcd__ node. In this case we will use internal master IP address. We can use ___hostname -i___ to get its first available IP address (if you are using Vagrant do not take 10.0.2.15 internal IP address).
We will run this container as __root__ becase certificates directory, __/etc/kubernetes/pki/etcd/__ requires special access.
```
vagrant@local-master:~$ sudo -s

root@local-master:~# root@local-master:~# docker run -it --user=0 \
-e ALLOW_NONE_AUTHENTICATION=yes -e ETCDCTL_API=3 \
-v /etc/kubernetes/pki/etcd/:/opt/bitnami/etcd/keys:ro --rm \
bitnami/etcd etcdctl --cacert=./keys/ca.crt \
--cert=./keys/server.crt --key=./keys/server.key \
--endpoints=https://10.10.10.11:2379 member list

Unable to find image 'bitnami/etcd:latest' locally
latest: Pulling from bitnami/etcd
8639931041da: Pull complete 
5f367e3beb43: Pull complete 
2df574070186: Pull complete 
f378e4dbe8ea: Pull complete 
e95be95f7d83: Pull complete 
347bff04a773: Pull complete 
5c9c7505926a: Pull complete 
e2e82262b7d8: Pull complete 
Digest: sha256:43d0df8b24259ed5447e76a138153e30b360839e893fbc4f8401b5b2fd8bf831
Status: Downloaded newer image for bitnami/etcd:latest

You set the environment variable ALLOW_NONE_AUTHENTICATION=yes. For safety reasons, do not use this flag in a production environment.
36993bb67a1292d7, started, local-master, https://10.10.10.11:2380, https://10.10.10.11:2379, false
```

Now that we are sure that the endpoint works, we can execute a snapshot.

First we will just remove __kube-apiserver.yaml__ from __/etc/kubernetes/manifests/__ manifests directory to stop Kube-APIServer.
```
root@local-master:~# mv /etc/kubernetes/manifests/kube-apiserver.yaml .
```

We will add an extra local bind volume to extract the snapshot from the container:
```
root@local-master:~# docker run -it --user=0 \
-e ALLOW_NONE_AUTHENTICATION=yes -e ETCDCTL_API=3 \
-v /etc/kubernetes/pki/etcd/:/opt/bitnami/etcd/keys:ro --rm \
-v /tmp:/backup \
bitnami/etcd etcdctl --cacert=./keys/ca.crt \
--cert=./keys/server.crt --key=./keys/server.key \
--endpoints=https://10.10.10.11:2379   snapshot save /backup/snapshotdb
You set the environment variable ALLOW_NONE_AUTHENTICATION=yes. For safety reasons, do not use this flag in a production environment.
{"level":"info","ts":1591260067.4569561,"caller":"snapshot/v3_snapshot.go:119","msg":"created temporary db file","path":"/backup/snapshotdb.part"}
{"level":"info","ts":"2020-06-04T08:41:07.471Z","caller":"clientv3/maintenance.go:200","msg":"opened snapshot stream; downloading"}
{"level":"info","ts":1591260067.4715993,"caller":"snapshot/v3_snapshot.go:127","msg":"fetching snapshot","endpoint":"https://10.10.10.11:2379"}
{"level":"info","ts":"2020-06-04T08:41:07.491Z","caller":"clientv3/maintenance.go:208","msg":"completed snapshot read; closing"}
{"level":"info","ts":1591260067.5205438,"caller":"snapshot/v3_snapshot.go:142","msg":"fetched snapshot","endpoint":"https://10.10.10.11:2379","size":"2.1 MB","took":0.063549148}
{"level":"info","ts":1591260067.520618,"caller":"snapshot/v3_snapshot.go:152","msg":"saved","path":"/backup/snapshotdb"}
Snapshot saved at /backup/snapshotdb
```

Now we have an __Etcd__'s snapshot in __/tmp/snapshotdb__:
```
root@local-master:~# ls -lart /tmp/snapshotdb 
-rw------- 1 root root 2846752 Jun  3 18:48 /tmp/snapshotdb
```

```
docker run -it --user=0 \
-e ALLOW_NONE_AUTHENTICATION=yes -e ETCDCTL_API=3 \
-v /tmp:/backup \
bitnami/etcd etcdctl --write-out=table snapshot status /backup/snapshotdb

You set the environment variable ALLOW_NONE_AUTHENTICATION=yes. For safety reasons, do not use this flag in a production environment.
+----------+----------+------------+------------+
|   HASH   | REVISION | TOTAL KEYS | TOTAL SIZE |
+----------+----------+------------+------------+
| a5a7a391 |     6495 |       1522 |     2.1 MB |
+----------+----------+------------+------------+

```

Now we can start Kube-APIServer again. We will just move __kube-apiserver.yaml__ to __/etc/kubernetes/manifests/__ manifests directory.
```
root@local-master:~# mv kube-apiserver.yaml /etc/kubernetes/manifests/
```


4 - Before moving to restore operation, let's create a simple Nginx __Deployment__:
```
root@local-master:~# kubectl create deployment web --image=nginx:alpine
deployment.apps/web created

root@local-master:~# kubectl get pods
NAME                 READY   STATUS    RESTARTS   AGE
web-c946dc86-k4cxs   1/1     Running   0          24s
```

5 - Now, we will restore previously created database. In order to restore created snapshot, we need to ensure that none requests arrives Kube-APIServer. Thus, we stop the Kube-APIServer.


```
root@local-master:~# docker ps|grep api
cb4ce879172b        c5c678ed2546              "kube-apiserver --ad…"   About a minute ago   Up About a minute                       k8s_kube-apiserver_kube-apiserver-local-master_kube-system_4ec8bac8fdebdd69d161e399726bd58e_1
524cbb2fc6d5        k8s.gcr.io/pause:3.1      "/pause"                 About a minute ago   Up About a minute                       k8s_POD_kube-apiserver-local-master_kube-system_4ec8bac8fdebdd69d161e399726bd58e_1
```

Then we will just remove __kube-apiserver.yaml__ from __/etc/kubernetes/manifests/__ manifests directory.
```
root@local-master:~# mv /etc/kubernetes/manifests/kube-apiserver.yaml .
```

Then we check again
```
root@local-master:~# docker ps|grep api
```

6 - Then we must stop Etcd. We will just remove __etcd.yaml__ from __/etc/kubernetes/manifests/__ manifests directory. 
```
root@local-master:~# mv /etc/kubernetes/manifests/etcd.yaml .
```
Therefore __Etcd__ goes down.
```
root@local-master:~# docker ps |grep etcd
```

7 - Now we can recover __Etcd__. We will use a new directory  
```
root@local-master:~# docker run -it --user=0 --rm \
-e ALLOW_NONE_AUTHENTICATION=yes \
-v /etc/kubernetes/pki/etcd/:/tmp/keys:ro \
-v /tmp:/backup \
-v /var/lib/:/var/lib/ \
bitnami/etcd etcdctl snapshot restore /backup/snapshotdb \
--name=local-master \
--cacert=/tmp/keys/ca.crt \
--cert=/tmp/keys/server.crt \
--key=/tmp/keys/server.key \
--data-dir /var/lib/etcd.NEW \
--initial-cluster=local-master=https://10.10.10.11:2380 \
--initial-advertise-peer-urls=https://10.10.10.11:2380

You set the environment variable ALLOW_NONE_AUTHENTICATION=yes. For safety reasons, do not use this flag in a production environment.
{"level":"info","ts":1591206376.0882802,"caller":"snapshot/v3_snapshot.go:296","msg":"restoring snapshot","path":"/backup/snapshotdb","wal-dir":"/var/lib/etcd.NEW/member/wal","data-dir":"/var/lib/etcd.NEW","snap-dir":"/var/lib/etcd.NEW/member/snap"}
{"level":"info","ts":1591206376.1568425,"caller":"mvcc/kvstore.go:380","msg":"restored last compact revision","meta-bucket-name":"meta","meta-bucket-name-key":"finishedCompactRev","restored-compact-revision":78171}
{"level":"info","ts":1591206376.1742976,"caller":"membership/cluster.go:392","msg":"added member","cluster-id":"8879ffbd85ff9506","local-member-id":"0","added-peer-id":"36993bb67a1292d7","added-peer-peer-urls":["https://10.10.10.11:2380"]}
{"level":"info","ts":1591206376.236108,"caller":"snapshot/v3_snapshot.go:309","msg":"restored snapshot","path":"/backup/snapshotdb","wal-dir":"/var/lib/etcd.NEW/member/wal","data-dir":"/var/lib/etcd.NEW","snap-dir":"/var/lib/etcd.NEW/member/snap"}
```


8 - Now we change in __etcd.yaml__ the data directory to a the location changing the __hostPath__ in the volume associated with __etcd-data__:
```
  - hostPath:
      path: /var/lib/etcd.NEW
      type: DirectoryOrCreate
    name: etcd-data
```
Edit __etcd.yaml__ with your favorite editor and you should have something like this:
```
root@local-master:~# cat etcd.yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    component: etcd
    tier: control-plane
  name: etcd
  namespace: kube-system
spec:
  containers:
  - command:
    - etcd
    - --advertise-client-urls=https://10.10.10.11:2379
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt
    - --client-cert-auth=true
    - --data-dir=/var/lib/etcd
    - --initial-advertise-peer-urls=https://10.10.10.11:2380
    - --initial-cluster=local-master=https://10.10.10.11:2380
    - --key-file=/etc/kubernetes/pki/etcd/server.key
    - --listen-client-urls=https://127.0.0.1:2379,https://10.10.10.11:2379
    - --listen-metrics-urls=http://127.0.0.1:2381
    - --listen-peer-urls=https://10.10.10.11:2380
    - --name=local-master
    - --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt
    - --peer-client-cert-auth=true
    - --peer-key-file=/etc/kubernetes/pki/etcd/peer.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    - --snapshot-count=10000
    - --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    image: k8s.gcr.io/etcd:3.4.3-0
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /health
        port: 2381
        scheme: HTTP
      initialDelaySeconds: 15
      timeoutSeconds: 15
    name: etcd
    resources: {}
    volumeMounts:
    - mountPath: /var/lib/etcd
      name: etcd-data
    - mountPath: /etc/kubernetes/pki/etcd
      name: etcd-certs
  hostNetwork: true
  priorityClassName: system-cluster-critical
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/etcd
      type: DirectoryOrCreate
    name: etcd-certs
  - hostPath:
      path: /var/lib/etcd.NEW
      type: DirectoryOrCreate
    name: etcd-data
status: {}
```


9 - We move both yaml files to kubelet's manifests directory to allow  etcd.yaml and kube-apiserver.yaml again.
```
root@local-master:~# mv etcd.yaml /etc/kubernetes/manifests/
root@local-master:~# mv kube-apiserver.yaml /etc/kubernetes/manifests/
```

And cluster is ready again.

```
root@local-master:~# kubectl get nodes
NAME            STATUS   ROLES    AGE   VERSION
local-master    Ready    master   88m   v1.17.6
local-worker1   Ready    <none>   82m   v1.17.6
local-worker2   Ready    <none>   75m   v1.17.6
```

Take a look at your __Deployments__...
```
root@local-master:~# kubectl get deploy -A
NAMESPACE     NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   calico-kube-controllers   0/1     1            0           90m
kube-system   coredns                   0/2     2            0           91m
root@local-master:~# 
```

Notice that the one created after the backup isn't there.

>### NOTE:
>### Described procedure is valid for non-high availability environments. Review Training slides for full procedure for 3 Master nodes cluster.
 


In this lab we learned how to create and restore Etcd's snapshots. It is easy but you have to ensure the process.


---

## Lab2 - Backup and restore of Kubernetes objects

In this lab we are going to learn different aproaches of backup  our resources in case users need to restore specific objects of the cluster.

>NOTE: Running Kubernetes cluster and recommended deployments from chapter7, or some deployments in a specific namespace

It is important to have the information **about everything that is deployed in our cluster**. In case that it is needed to restore a part of our cluster and do not depend on a etcd restauration. It is also to restore resouces that have been deployed since our last backup

They are different aproaches for this, it can be used a **source versioning** tool as gitlab,github...but this approach has a problem, people can still deployed important resources on the cluster and not upload them on the source versioning. It can be avoid doing a full backup from resouces, doing petitions to the etcd database where everything is store.

We are going to see different backup aproches and a restore of a full namespaced resources.


1 - Create backups from our resources using different kubectl commands.

Backup of resources using **kubectl get**, we can get resources from all our cluster or from some namespaces. **But we are going to use another aproched.**

```
vagrant@local-master:~$ kubectl get all --all-namespaces -o yaml > whole-$(date +%Y-%m-%d-%H:%M).back
vagrant@local-master:~$ kubectl get all -n ch7-dev -o yaml > ch7-dev-$(date +%Y-%m-%d-%H:%M).back
```

kubectl also has a proper command-line (**kubectl cluster-info dump**) in order to get backup of all our namespaces or specific. **This is the one we are going to use in order to have a full backup.**
```
vagrant@local-master:~$ mkdir dump
vagrant@local-master:~$ kubectl cluster-info dump --all-namespaces --output-directory=/home/vagrant/dump

vagrant@local-master:~$ /dump$ ls
ch7-dev  ch7-prod  default  kube-node-lease  kube-public  kube-system  nodes.json

```
This command would create a folder with every namespaces and create all resources in that namespace in json format.

```

```

It can also be use this approch to have backup from **specific namespace** but it would also backup the kube-system namespace and nodes information.

```
vagrant@local-master:~$ kubectl cluster-info dump -n ch7-dev --output-directory=/home/vagrant/dump

vagrant@local-master:~$ /dump$ ls
ch7-dev  kube-system  nodes.json
```

2 - Now we are going to restore a full namespace which was deleted by mistake.

So we delete namespace ch7-dev  and restore, take into consideration that it is going to be needed to create de namespaces as this is a non-namespaced resource that is not back up

First check everything that is deploy in order to make sure restore is properly done.
```
vagrant@local-master:~$ kubectl get all -n ch7-dev
NAME                            READY   STATUS    RESTARTS   AGE
pod/firstdev-569f7fbf8f-cvkls   1/1     Running   0          2m36s
pod/firstdev-569f7fbf8f-nqz7w   1/1     Running   0          2m36s
pod/seconddev-5d765f45b-t2k5v   1/1     Running   0          2m36s

NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/firstdev    ClusterIP   10.105.6.57     <none>        3000/TCP   2m36s
service/seconddev   ClusterIP   10.101.146.92   <none>        3000/TCP   2m36s

NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/firstdev    2/2     2            2           2m37s
deployment.apps/seconddev   1/1     1            1           2m36s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/firstdev-569f7fbf8f   2         2         2       2m36s
replicaset.apps/seconddev-5d765f45b   1         1         1       2m36s

```

```
vagrant@local-master:~$ kubectl delete namespace ch7-dev

# check thath nothing exits
vagrant@local-master:~$ kubectl get pods  -n ch7-dev
vagrant@local-master:~$ kubectl create namespace ch7-dev
```
Create everything from your backup folder from the command line **kubectl cluster-info dump** in or case backup is in /home/vagrant/dump/ch7-dev.
```
kubectl create -f /home/vagrant/dump/ch7-dev
deployment.apps/firstdev created
deployment.apps/seconddev created
event/firstdev-569f7fbf8f-d8gds.16155322bb7c0696 created
event/firstdev-569f7fbf8f-d8gds.161553231ddcbf61 created
event/firstdev-569f7fbf8f-d8gds.161553231fa16533 created
event/firstdev-569f7fbf8f-d8gds.161553232d3995db created
event/firstdev-569f7fbf8f-hxpck.161552 [OMITED]
```

Check that the namespace has everything as in the beginning
```
vagrant@local-master:~$  kubectl get all -n ch7-dev
NAME                            READY   STATUS    RESTARTS   AGE
pod/firstdev-569f7fbf8f-6nxsw   1/1     Running   0          91s
pod/firstdev-569f7fbf8f-v6xv4   1/1     Running   0          91s
pod/seconddev-5d765f45b-s6jj7   1/1     Running   0          91s

NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/firstdev    ClusterIP   10.105.6.57     <none>        3000/TCP   91s
service/seconddev   ClusterIP   10.101.146.92   <none>        3000/TCP   91s

NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/firstdev    2/2     2            2           91s
deployment.apps/seconddev   1/1     1            1           91s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/firstdev-569f7fbf8f   2         2         2       91s
replicaset.apps/seconddev-5d765f45b   1         1         1       91s

```
So in this lab we were able to recover from lost resources without doing a full restore of the database.


## Lab3 - Maintenance in a node

In this lab we are going to simulated and maintenance activity in one of our nodes and how we can avoid any deployment on this noed while we are working on it.

>NOTE: Running Kubernetes cluster and deployments from ch7 or any other deployments.

1 - Get the nodes an decided get the one which is goin to suffer some maintenance activity.

>IMPORTANT: Take into consideration that maintenances activities should be done one by one, node by node. When maintenance is finish in one of the node everything needs to be working properly in order to start in other node.

Get all nodes from the cluster
```
vagrant@local-master:~$ kubectl get nodes
NAME    STATUS   ROLES    AGE     VERSION
node1   Ready    master   8m38s   v1.17.0
node2   Ready    <none>   5m15s   v1.17.0
node3   Ready    <none>   2m9s    v1.17.0

```

2 - We are going to make maintenance on the node2.


See how many workloads are in node2:
```

vagrant@local-master:~$ kubectl get pod --all-namespaces -o wide 
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE     IP               NODE    NOMINATED NODE   READINESS GATES
ch7-dev       firstdev-569f7fbf8f-7bbbg                  1/1     Running   0          78s     10.244.135.2     node3   <none>           <none>
ch7-dev       firstdev-569f7fbf8f-v5jv4                  1/1     Running   0          78s     10.244.104.1     node2   <none>           <none>
ch7-dev       seconddev-5d765f45b-md5wp                  1/1     Running   0          78s     10.244.135.1     node3   <none>           <none>
ch7-prod      prodonly-794f7885c8-qv275                  1/1     Running   0          78s     10.244.135.3     node3   <none>           <none>
kube-system   calico-kube-controllers-589b5f594b-xj27n   1/1     Running   0          12m     10.244.166.130   node1   <none>           <none>
[OMITED]
```
We are going to **drain** the node in order for all de deployed resources to go to other nodes in the cluster, in this case **node2**. The drain command force eviction of all resouces and stop any workload to be deployed on that node while it is drain.


```
vagrant@local-master:~$ kubectl drain node2
 kubectl drain node2
node/node2 cordoned
evicting pod "firstdev-569f7fbf8f-v5jv4"
pod/firstdev-569f7fbf8f-v5jv4 evicted
node/node2 evicted

```

Now would be time to perform all the **maintenance activities** in the node. All pods (if they can) would be distributed across the rest of the workers nodes.

Check where resources are:
```
vagrant@local-master:~$ kubectl get pod --all-namespaces -o wide
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE     IP               NODE    NOMINATED NODE   READINESS GATES
ch7-dev       firstdev-569f7fbf8f-5ttc9                  1/1     Running   0          2m30s   10.244.135.4     node3   <none>           <none>
ch7-dev       firstdev-569f7fbf8f-7bbbg                  1/1     Running   0          4m21s   10.244.135.2     node3   <none>           <none>
ch7-dev       seconddev-5d765f45b-md5wp                  1/1     Running   0          4m21s   10.244.135.1     node3   <none>           <none>
ch7-prod      prodonly-794f7885c8-qv275                  1/1     Running   0          4m21s   10.244.135.3     node3   <none>           <none>
kube-system   calico-node-nsc6j                          1/1     Running   0          12m     10.10.10.12      node2   <none>           <none>
kube-system   kube-proxy-5szzk                           1/1     Running   0          12m     10.10.10.12      node2   <none>           <none>

[OMITED]
```
> NOTE As you can see calico and kube-proxy are still deployed on node2 this is because calico is install as daemonset you can take a look. Daemonset do not need rescheduled in order to give service as its nature is to be deployed in every node if it can.




It can be check the status of the nodes.
```
vagrant@local-master:~$ kubectl get nodes
NAME    STATUS                     ROLES    AGE   VERSION
node1   Ready                      master   16m   v1.17.0
node2   Ready,SchedulingDisabled   <none>   13m   v1.17.0
node3   Ready                      <none>   10m   v1.17.0

```
Once maintenances activities have finish is time to put in ready state our node, in this case node2.

```
vagrant@local-master:~$ kubectl uncordon node2

 kubectl get nodes
NAME    STATUS   ROLES    AGE     VERSION
node1   Ready    master   18m   v1.17.0
node2   Ready    <none>   15m    v1.17.0
node3   Ready    <none>   12m   v1.17.0
vagrant@node1:~$ 

vagrant@local-master:~$ kubectl get pods --all-namespaces
kubectl get pods --all-namespaces -o wide
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE   IP               NODE    NOMINATED NODE   READINESS GATES
ch7-dev       firstdev-569f7fbf8f-5ttc9                  1/1     Running   0          27m   10.244.135.4     node3   <none>           <none>
ch7-dev       firstdev-569f7fbf8f-7bbbg                  1/1     Running   0          29m   10.244.135.2     node3   <none>           <none>
ch7-dev       seconddev-5d765f45b-md5wp                  1/1     Running   0          29m   10.244.135.1     node3   <none>           <none>
ch7-prod      prodonly-794f7885c8-qv275                  1/1     Running   0          29m   10.244.135.3     node3   <none>           <none>
kube-system   calico-kube-controllers-589b5f594b-xj27n   1/1     Running   0          40m   10.244.166.130   node1   <none>           <none>
[OMITED]

```
As you can see pods are not going to be moved to node 2, but new pods would be deployed on this node as nothing is been used in this node.

The default tendency of scheduling is virtually place pods on nodes and see which nodes have more free resources and then deploy resources on those nodes. 
We can tried this assumption. Deploy a nginx deployment
```
vagrant@local-master:~$ kubectl create deploy --image=nginx nginx-node2
vagrant@local-master:~$ kubect get pods -n default -o wide | grep nginx-node2
nginx-node2-dc777cf9b-6nznl   1/1     Running   0          52s   10.244.104.2   node2   <none>           <none>

```

> DIFFERENCES BETWEEN CORDON AND DRAIN
>
> vagrant@local-master:~$  kubectl get all --all-namespaces
>
> vagrant@local-master:~$  kubectl cordon node2
vagrant@local-master:~$  >kubectl get all --all-namespaces
>
> when you cordon a node actual object are not evicted fron the node while if drain is done, they would be evicted


> EXTRA: We can tried to drain everything in node3 included daemonsets:
>
>kubectl drain node3 --ignore-daemonsets
>Check that any container is deployed in node3, not even daemonsets






### What we have learned in these labs?.

- We learn to perform backups of out databases and our resources
- Learn to perform restores of our databases and resources
- Also maintenances jobs on the nodes
