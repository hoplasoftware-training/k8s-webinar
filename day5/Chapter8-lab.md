# Chapter 8
In these labs we will cover all the topics learned on Chapter8.

- In this lab we are going to configure basic monitoring using metrics server.

- We will also execute Prometheus to monitor basic Kubernetes environment.

---

## Lab1 - Deploying Metrics Server

In this lab we will deploy metrics server and get soma basic monitoring data.

1 -  We will deploy metrics server using [Kubernetes Metrics Server Deployment of required resources](https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml).


```
student@master:~$ curl -L -s -o metrics-server.all https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml
```
Then we add add following lines to "metrics-server" __Deployment__:
 - --kubelet-insecure-tls=true
 - --kubelet-preferred-address-types=InternalIP

Modified Kubernetes Metrics Server Deployment with all components will be contained in [metrics-server.all.yml](./metrics-server.all.yml).

We can generate this file using following snippet:
```
$ cat<<EOF> metrics-server.all.yml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: system:aggregated-metrics-reader
  labels:
    rbac.authorization.k8s.io/aggregate-to-view: "true"
    rbac.authorization.k8s.io/aggregate-to-edit: "true"
    rbac.authorization.k8s.io/aggregate-to-admin: "true"
rules:
- apiGroups: ["metrics.k8s.io"]
  resources: ["pods", "nodes"]
  verbs: ["get", "list", "watch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: metrics-server:system:auth-delegator
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: metrics-server-auth-reader
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: extension-apiserver-authentication-reader
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: apiregistration.k8s.io/v1beta1
kind: APIService
metadata:
  name: v1beta1.metrics.k8s.io
spec:
  service:
    name: metrics-server
    namespace: kube-system
  group: metrics.k8s.io
  version: v1beta1
  insecureSkipTLSVerify: true
  groupPriorityMinimum: 100
  versionPriority: 100
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: metrics-server
  namespace: kube-system
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: metrics-server
  namespace: kube-system
  labels:
    k8s-app: metrics-server
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  template:
    metadata:
      name: metrics-server
      labels:
        k8s-app: metrics-server
    spec:
      serviceAccountName: metrics-server
      volumes:
      # mount in tmp so we can safely use from-scratch images and/or read-only containers
      - name: tmp-dir
        emptyDir: {}
      containers:
      - name: metrics-server
        image: k8s.gcr.io/metrics-server-amd64:v0.3.6
        imagePullPolicy: IfNotPresent
        args:
          - --cert-dir=/tmp
          - --secure-port=4443
          - --kubelet-insecure-tls=true
          - --kubelet-preferred-address-types=InternalIP
        ports:
        - name: main-port
          containerPort: 4443
          protocol: TCP
        securityContext:
          readOnlyRootFilesystem: true
          runAsNonRoot: true
          runAsUser: 1000
        volumeMounts:
        - name: tmp-dir
          mountPath: /tmp
      nodeSelector:
        kubernetes.io/os: linux
        kubernetes.io/arch: "amd64"
---
apiVersion: v1
kind: Service
metadata:
  name: metrics-server
  namespace: kube-system
  labels:
    kubernetes.io/name: "Metrics-server"
    kubernetes.io/cluster-service: "true"
spec:
  selector:
    k8s-app: metrics-server
  ports:
  - port: 443
    protocol: TCP
    targetPort: main-port
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: system:metrics-server
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - nodes
  - nodes/stats
  - namespaces
  - configmaps
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: system:metrics-server
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:metrics-server
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
EOF
```

Then we deploy this full-components file.
```
student@master:~$ kubectl create -f metrics-server.all.yml
clusterrole.rbac.authorization.k8s.io/system:aggregated-metrics-reader created
clusterrolebinding.rbac.authorization.k8s.io/metrics-server:system:auth-delegator created
rolebinding.rbac.authorization.k8s.io/metrics-server-auth-reader created
apiservice.apiregistration.k8s.io/v1beta1.metrics.k8s.io created
serviceaccount/metrics-server created
deployment.apps/metrics-server created
service/metrics-server created
clusterrole.rbac.authorization.k8s.io/system:metrics-server created
clusterrolebinding.rbac.authorization.k8s.io/system:metrics-server created
```

2 - Once Kubernetes Metrics Server is running, we will wait a couple of minutes to have some data.
```
student@master:~$ kubectl get pods -n kube-system
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-7994b948dd-f5gtp   1/1     Running   0          5h33m
calico-node-b6k86                          1/1     Running   0          5h25m
calico-node-sfvwp                          1/1     Running   0          5h33m
calico-node-zsbhw                          1/1     Running   0          5h19m
coredns-6955765f44-96hjr                   1/1     Running   0          5h33m
coredns-6955765f44-tmqsv                   1/1     Running   0          5h33m
etcd-master                          1/1     Running   0          5h33m
kube-apiserver-master                1/1     Running   0          5h33m
kube-controller-manager-master       1/1     Running   1          5h33m
kube-proxy-g86kf                           1/1     Running   0          5h19m
kube-proxy-k2lz4                           1/1     Running   0          5h25m
kube-proxy-ss2zk                           1/1     Running   0          5h33m
kube-scheduler-master                1/1     Running   1          5h33m
metrics-server-754c775cc5-gs6kh            1/1     Running   0          85s
student@master:~$ 
```

We can use ___kubectl top nodes ___ to get an overview off all node's resources usage:
```
student@master:~$ kubectl top nodes 
NAME            CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
master    148m         7%     1462Mi          38%       
local-worker1   60m          3%     1067Mi          27%       
local-worker2   68m          3%     1036Mi          26%     
```

To get an overview of resources used by running __Pods__ we will use ___kubectl top pods ___. We have to wait a couple of minutes to obtain some data. We will use ___-A___ or ___--all--namepsaces___ to show all __Namespaces__' __Pods__:
```
student@master:~$ kubectl top pods -A
NAMESPACE     NAME                                       CPU(cores)   MEMORY(bytes)   
kube-system   calico-kube-controllers-7994b948dd-f5gtp   2m           6Mi             
kube-system   calico-node-b6k86                          20m          22Mi            
kube-system   calico-node-sfvwp                          15m          22Mi            
kube-system   calico-node-zsbhw                          16m          22Mi            
kube-system   coredns-6955765f44-96hjr                   3m           6Mi             
kube-system   coredns-6955765f44-tmqsv                   2m           6Mi             
kube-system   etcd-master                          15m          31Mi            
kube-system   kube-apiserver-master                30m          248Mi           
kube-system   kube-controller-manager-master       10m          38Mi            
kube-system   kube-proxy-g86kf                           1m           8Mi             
kube-system   kube-proxy-k2lz4                           1m           8Mi             
kube-system   kube-proxy-ss2zk                           1m           8Mi             
kube-system   kube-scheduler-master                3m           11Mi            
kube-system   metrics-server-754c775cc5-gs6kh            1m           11Mi   
```


In this lab we learned how to deploy the Kubernetes' Metrics Server which is required for many tools. We also learned ___kubectl top___ for either __Nodes__ and __Pods__.

---

## Lab2 - Deploying basic Prometheus for monitoring Kubernetes platform.

In ths lab we will deploy __Prometheus__ and learn some basic usage.

1 - First we create a new __Namespace__ for monitoring resources:
```
student@master:~$ kubectl create namespace monitoring
```


2 -  We will deploy __Prometheus__, using its official __Helm's Chart__ in "monitoring" __Namespace__.

If you don't have __Helm__ and/or __kubernetes-charts.storage.googleapis.com__ repository, execute following commands:
```
student@master:~$ curl -s https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | sudo bash
student@master:~$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
student@master:~$ helm repo update
```

Once __kubernetes-charts.storage.googleapis.com__ is in our local repository list and __Helm__'s cache is updated, we can install __Prometheus Chart__. We will simply execute ___helm install___ using defined namespace:
```
student@master:~$ helm install prometheus prometheus-community/prometheus --namespace monitoring                  
NAME: prometheus
LAST DEPLOYED: Wed Jun  3 10:29:41 2020
NAMESPACE: monitoring
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-server.monitoring.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  student@master:~$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  student@master:~$ kubectl --namespace monitoring port-forward $POD_NAME 9090


The Prometheus alertmanager can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-alertmanager.monitoring.svc.cluster.local


Get the Alertmanager URL by running these commands in the same shell:
  student@master:~$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=alertmanager" -o jsonpath="{.items[0].metadata.name}")
  student@master:~$ kubectl --namespace monitoring port-forward $POD_NAME 9093
#################################################################################
######   WARNING: Pod Security Policy has been moved to a global property.  #####
######            use .Values.podSecurityPolicy.enabled with pod-based      #####
######            annotations                                               #####
######            (e.g. .Values.nodeExporter.podSecurityPolicy.annotations) #####
#################################################################################


The Prometheus PushGateway can be accessed via port 9091 on the following DNS name from within your cluster:
prometheus-pushgateway.monitoring.svc.cluster.local


Get the PushGateway URL by running these commands in the same shell:
  student@master:~$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=pushgateway" -o jsonpath="{.items[0].metadata.name}")
  student@master:~$ kubectl --namespace monitoring port-forward $POD_NAME 9091

For more information on running Prometheus, visit:
https://prometheus.io/
student@master:~$ 
```

We used default __Values__ defined for __Prometheus' Chart__. We are following a common workflow.


3 - We review defined __Service__ resources to obtain __Prometheus__' port.
```
student@master:~$ kubectl get svc -n monitoring
NAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE                                    
prometheus-alertmanager         ClusterIP   10.98.229.119    <none>        80/TCP     9m28s                                  
prometheus-kube-state-metrics   ClusterIP   10.102.189.45    <none>        8080/TCP   9m28s                                  
prometheus-node-exporter        ClusterIP   None             <none>        9100/TCP   9m28s                                  
prometheus-pushgateway          ClusterIP   10.99.181.206    <none>        9091/TCP   9m28s                                  
prometheus-server               ClusterIP   10.101.167.131   <none>        80/TCP     9m28s                                  
```

__Prometheus__ is not published. Therefore we need to either use Ingress Controller or expose __Prometheus__' ports using __NodePort__ service. In this case, for this lab we will just use __NodePort__ becasue it is simpler. Don't use this in production.

To create a service we need to review __Deployments__ to verify their __Pods__ and their ports:
```
student@master:~$ kubectl get deploy -n monitoring                                                                     
NAME                            READY   UP-TO-DATE   AVAILABLE   AGE                                                         
prometheus-alertmanager         0/1     1            0           11m                                                         
prometheus-kube-state-metrics   1/1     1            1           11m                                                         
prometheus-pushgateway          1/1     1            1           11m                                                         
prometheus-server               0/1     1            0           11m                                                         

student@master:~$ kubectl get pods -n monitoring                                                                       
NAME                                            READY   STATUS    RESTARTS   AGE                                             
prometheus-alertmanager-8466cfd79b-thgjx        0/2     Pending   0          11m                                             
prometheus-kube-state-metrics-6756bbbb8-9gzdc   1/1     Running   0          11m                                             
prometheus-node-exporter-k8bf9                  1/1     Running   0          11m                                             
prometheus-node-exporter-xvztd                  1/1     Running   0          11m                                             
prometheus-pushgateway-6545968759-vqjk7         1/1     Running   0          11m
prometheus-server-679b746f8d-kcbsv              0/2     Pending   0          11m
```

We notice that something is wrong.
```
student@master:~$ kubectl describe pod/prometheus-server-679b746f8d-kcbsv -n monitoring
Name:           prometheus-server-679b746f8d-kcbsv
Namespace:      monitoring
Priority:       0
Node:           <none>
Labels:         app=prometheus
                chart=prometheus-11.3.0
                component=server
                heritage=Helm
                pod-template-hash=679b746f8d
                release=prometheus
Annotations:    <none>
Status:         Pending
IP:             
IPs:            <none>
Controlled By:  ReplicaSet/prometheus-server-679b746f8d
Containers:
  prometheus-server-configmap-reload:
    Image:      jimmidyson/configmap-reload:v0.3.0
    Port:       <none>
    Host Port:  <none>
    Args:
      --volume-dir=/etc/config
      --webhook-url=http://127.0.0.1:9090/-/reload
    Environment:  <none>
    Mounts:
      /etc/config from config-volume (ro)
      /var/run/secrets/kubernetes.io/serviceaccount from prometheus-server-token-m8xzm (ro)
  prometheus-server:
    Image:      prom/prometheus:v2.18.1
    Port:       9090/TCP
    Host Port:  0/TCP
    Args:
      --storage.tsdb.retention.time=15d
      --config.file=/etc/config/prometheus.yml
      --storage.tsdb.path=/data
      --web.console.libraries=/etc/prometheus/console_libraries
      --web.console.templates=/etc/prometheus/consoles
      --web.enable-lifecycle
    Liveness:     http-get http://:9090/-/healthy delay=30s timeout=30s period=10s #success=1 #failure=3
    Readiness:    http-get http://:9090/-/ready delay=30s timeout=30s period=10s #success=1 #failure=3
    Environment:  <none>
    Mounts:
      /data from storage-volume (rw)
      /etc/config from config-volume (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from prometheus-server-token-m8xzm (ro)
Conditions:
  Type           Status
  PodScheduled   False 
Volumes:
  config-volume:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      prometheus-server
    Optional:  false
  storage-volume:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  prometheus-server
    ReadOnly:   false
  prometheus-server-token-m8xzm:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  prometheus-server-token-m8xzm
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason            Age        From               Message
  ----     ------            ----       ----               -------
  Warning  FailedScheduling  <unknown>  default-scheduler  error while running "VolumeBinding" filter plugin for pod "prometheus-server-679b746f8d-kcbsv": pod has unbound immediate PersistentVolumeClaims
  Warning  FailedScheduling  <unknown>  default-scheduler  error while running "VolumeBinding" filter plugin for pod "prometheus-server-679b746f8d-kcbsv": pod has unbound immediate PersistentVolumeClaims
```

Pods are pending because __Prometheus' Chart__ is using volumes by default (they should be required for production because Prometehus' data must be persistent). For this simple lab, we will remove persistence.

4 - We need to reinstall "prometheus" (this is the name given to our __Helm__ deployment).
First we will uninstall "prometheus" completly. 
```
student@master:~$ helm uninstall prometheus -n monitoring
release "prometheus" uninstalled
```

Then we will get all leys available to change persistence behavior.
```
student@master:~$ helm show values prometheus-community/prometheus >values.prometheus.yaml
```

Then we edit created "values.prometheus.yaml" file and look for "persistentVolume" keys.
```
student@master:~$ vi values.prometheus.yaml

...
  persistentVolume:
    ## If true, alertmanager will create/use a Persistent Volume Claim
    ## If false, use emptyDir
    ##
    enabled: true

    ## alertmanager data Persistent Volume access modes
    ## Must match those of existing PV or dynamic provisioner
    ## Ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
    ##
,,,
```

We will change all "persistentVolume" keys to "enable: false" (there are at least one for AlertManager and other for PrometheusServer).
```
...
  persistentVolume:
    ## If true, alertmanager will create/use a Persistent Volume Claim
    ## If false, use emptyDir
    ##
    enabled: true
...
```

Once all persistentvolume settings are set to false, we can installaagain, using customized values file. 
```
student@master:~$ helm install --values values.prometheus.yaml prometheus prometheus-community/prometheus --namespace monitoring
NAME: prometheus
LAST DEPLOYED: Wed Jun  3 10:49:22 2020
NAMESPACE: monitoring
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-server.monitoring.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  student@master:~$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  student@master:~$ kubectl --namespace monitoring port-forward $POD_NAME 9090


The Prometheus alertmanager can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-alertmanager.monitoring.svc.cluster.local


Get the Alertmanager URL by running these commands in the same shell:
  student@master:~$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=alertmanager" -o jsonpath="{.items[0].metadata.name}")
  student@master:~$ kubectl --namespace monitoring port-forward $POD_NAME 9093
#################################################################################
######   WARNING: Persistence is disabled!!! You will lose your data when   #####
######            the AlertManager pod is terminated.                       #####
#################################################################################
#################################################################################
######   WARNING: Pod Security Policy has been moved to a global property.  #####
######            use .Values.podSecurityPolicy.enabled with pod-based      #####
######            annotations                                               #####
######            (e.g. .Values.nodeExporter.podSecurityPolicy.annotations) #####
#################################################################################


The Prometheus PushGateway can be accessed via port 9091 on the following DNS name from within your cluster:
prometheus-pushgateway.monitoring.svc.cluster.local


Get the PushGateway URL by running these commands in the same shell:
  student@master:~$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=pushgateway" -o jsonpath="{.items[0].metadata.name}")
  student@master:~$ kubectl --namespace monitoring port-forward $POD_NAME 9091

For more information on running Prometheus, visit:
https://prometheus.io/

```

After these changes, __Prometheus__ will work as expected.
```
student@master:~$ kubectl get pods -n monitoring
NAME                                            READY   STATUS    RESTARTS   AGE
prometheus-alertmanager-54758cd8c9-b2jfb        2/2     Running   0          10m
prometheus-kube-state-metrics-6756bbbb8-g7nbx   1/1     Running   0          10m
prometheus-node-exporter-pbg4q                  1/1     Running   0          10m
prometheus-node-exporter-qdvqm                  1/1     Running   0          10m
prometheus-pushgateway-6545968759-2c5sf         1/1     Running   0          10m
prometheus-server-64cd8cbf99-577nx              2/2     Running   0          10m


student@master:~$ kubectl get deploy -n monitoring
NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
prometheus-alertmanager         1/1     1            1           11m
prometheus-kube-state-metrics   1/1     1            1           11m
prometheus-pushgateway          1/1     1            1           11m
prometheus-server               1/1     1            1           11m
```

5 - Let's publish now the "prometheus-server" __Deployment__. __Prometheus__ will expose its web server process on port 9090 by default.
```
student@master:~$ kubectl expose deploy -n monitoring prometheus-server --type=NodePort --name prometheus-server-9090 --port 9090 
service/prometheus-server-9090 exposed
```

We can review the port assigned for this new service.
```
student@master:~$ kubectl get svc -n monitoringNAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
prometheus-alertmanager         ClusterIP   10.99.137.7      <none>        80/TCP           15m
prometheus-kube-state-metrics   ClusterIP   10.101.58.133    <none>        8080/TCP         15m
prometheus-node-exporter        ClusterIP   None             <none>        9100/TCP         15m
prometheus-pushgateway          ClusterIP   10.102.25.243    <none>        9091/TCP         15m
prometheus-server               ClusterIP   10.100.254.3     <none>        80/TCP           15m
prometheus-server-9090          NodePort    10.100.200.219   <none>        9090:32284/TCP   14s
```

And now we can simply test it locally to verify that it is working.
```
student@master:~$ curl 0.0.0.0:32284
<a href="/graph">Found</a>.
```

It works and we can access __Prometheus__ then.

>NOTE: This is a lab, therefore neither TLS or Ingress resource were created to expose prometheus service. Take care and use appropriate resources for ypur production environment. You can use __Helm__ with appropriate valeus. Take a look at created __prometheus.values.yaml file__.

6 - __Prometheus__ is up and accessible and we can connect to its Web UI using some cluster IP and defined port (may change from your environment).
We will use our favorite web browser and access http://<WORKER_OR_MANAER_NODE_IP>:<PUBLISHED_RANDOM_NODEPORT>

>NOTE: We haven't selected an specific port for __Prometheus__ but we can do it, of course. __Promehetus__ web server is published on port __9090__ by default.


![Prometheus Dashboard](../Images/Prometheus_lab.jpg)


6 - Then we can write a couple of good and interesting queries.

- ### __Pod's CPU usage, avoiding intermediate containers, named "POD"__.
```
sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod!=""}[5m])) by (pod)
```

![Graph Results 1](../Images/Prometheus_lab_q1.jpg)

- ### __Pod's CPU usage out of monitoring namespace__
```
sum(rate(container_cpu_usage_seconds_total{namespace!="monitoring"}[5m])) by (pod)
```

![Graph Results 2](../Images/Prometheus_lab_q2.jpg)

- ### __Pod's CPU usage by namespace__
```
sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod!=""}[5m])) by (namespace)
```

![Graph Results 3](../Images/Prometheus_lab_q3.jpg)

- ### __CPU requests__
```
sum(kube_pod_container_resource_requests_cpu_cores)
```

![Graph Results 4](../Images/Prometheus_lab_q4.jpg)

- ### __CPU requests by Pod__
```
sum(kube_pod_container_resource_requests_cpu_cores) by (pod)
```

![Graph Results 5](../Images/Prometheus_lab_q5.jpg)

- ### __CPU load per node__
```
sum(node_load1) by (kubernetes_node)
```

![Graph Results 6](../Images/Prometheus_lab_q6.jpg)

- ### __FS disk space usage per node__
```
(node_filesystem_size_bytes - node_filesystem_free_bytes)/node_filesystem_size_bytes * 100
```

![Graph Results 7](../Images/Prometheus_lab_q7.jpg)

7 - We notice that we are only getting values for worker nodes. We review deployment __Prometheus__ components looking for "node agents" components. With __Prometheus__ we use __exporters__ for getting application and resources data. For nodes we will use __node-exporter__.
```  
student@master:~$ kubectl get pods -n monitoring
NAME                                            READY   STATUS    RESTARTS   AGE
prometheus-alertmanager-54758cd8c9-b2jfb        2/2     Running   0          111m
prometheus-kube-state-metrics-6756bbbb8-g7nbx   1/1     Running   0          111m
prometheus-node-exporter-pbg4q                  1/1     Running   0          111m
prometheus-node-exporter-qdvqm                  1/1     Running   0          111m
prometheus-pushgateway-6545968759-2c5sf         1/1     Running   0          111m
prometheus-server-64cd8cbf99-577nx              2/2     Running   0          111m
```

Then we take a look at __DaemonSets__:
```
student@master:~$ kubectl get daemonsets -n monitoring
NAME                       DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
prometheus-node-exporter   2         2         2       2            2           <none>          112m
```

Althoug __Prometheus__ defined a __DaemonSet__ not all nodes get workloads.

We take a look at master node looking for some __Taints__.
```
student@master:~$ kubectl describe node master
Name:               master
Roles:              master
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=master
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/master=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 192.168.56.11/24
                    projectcalico.org/IPv4IPIPTunnelAddr: 192.168.71.128
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Wed, 03 Jun 2020 09:44:59 +0200
Taints:             node-role.kubernetes.io/master:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  master
  AcquireTime:     <unset>
  RenewTime:       Wed, 03 Jun 2020 13:07:06 +0200
Conditions:
...
...
```

We notice that __node-role.kubernetes.io/master:NoSchedule__ taint is avoiding scheduling on master node, even for __Daemonsets__. We will remove this taint and take a look again at previous __Prometheus__ queries. 
```
student@master:~$ kubectl taint node master node-role.kubernetes.io/master:NoSchedule-
node/master untainted
```

8 - We review again "prometheus-node-exporter" ___Daemonset__ and now we have 3 instances:
```
student@master:~$ kubectl get daemonsets -n monitoring
NAME                       DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
prometheus-node-exporter   3         3         3       3            3           <none>          132m
student@master:~$ 
```

Now we execute again the query to obtain FS disk space usage per node.
```
(node_filesystem_size_bytes - node_filesystem_free_bytes)/node_filesystem_size_bytes * 100
```

And now all the nodes are visible.

- ### __FS disk space usage per node__

![Query Results](../Images/Prometheus_lab_q8.jpg)

And the graph shows all the nodes 

![FS Results 3 Nodes](../Images/Prometheus_lab_q9.jpg)


In this lab we learned how to deploy Prometheus and some tricks around the tasks commonly used to deploy monitoring applications. We also learned the basics to create some simple queries in __PromSQL language__ for __Prometheus__. 




---

## Lab3 - Use autoscaling features

In this lab we will deploy __Horizontal Pod Autoscale Example__ and learn some basic usage.

> ### NOTE: 
> ### To avoid performance issues on master nodes, taint your master node again by executing 
> ### ___kubectl taint nodes master node-role.kubernetes.io/master="":NoSchedule___
> 
> 



1 - First we create a new __Namespace__ for "ch8" resources:
```
student@master:~$ kubectl create namespace ch8
namespace/ch8 created
```

2 - We now create a simple "stress" deployment. We will stress 2 CPUs. Be careful if you test this in your own environment.
```
student@master:~$ cat<<EOF> stress-cpu.deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: stress-cpu
spec:
  replicas: 1
  selector:
    matchLabels:
      app: stress-cpu 
  template:
    metadata:
      name: stress-cpu
      labels:
        app: stress-cpu
    spec:  
      nodeSelector:
        node-role.kubernetes.io/worker: "" # Ensure you already have deployed this label on your worker nodes!!!
      containers:
      - name: stress-cpu
        image: polinux/stress
        resources:
          requests:
            memory: "50Mi"
            cpu: "1000m"
          limits:
            memory: "100Mi"
        command: ["stress"]
        args: ["--cpu","2"]
        # We can also use --vm-bytes 200M to force OOMKiller to kill deployment's Pods continuously.
EOF

```

Then we deploy this new deployment.
```
student@master:~$ kubectl create -f stress-cpu.deployment.yaml
deployment.apps/stress-cpu created
```

3 - We will now create an autoscale resource for this deployment.
```
student@master:~$  kubectl autoscale deployment --cpu-percent=50 --min=1 --max=2 stress-cpu -n ch8
horizontalpodautoscaler.autoscaling/stress-cpu autoscaled
```

We asked for 2 CPUs in our stress application and we monitored 50% of 1000m (stress application will reach 1948m). In case of overpassing this 50%, a new replica will be created, up to a maximum of 2 replicas.

4 - Then we just review this HorizontalPodAutoscaling resource:
```
student@master:~$ kubectl get hpa -n ch8
NAME         REFERENCE               TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
stress-cpu   Deployment/stress-cpu   <unknown>/50%   1         2         1          28s
student@master:~$ 
```

5 - We will wait some seconds to allow metrics to be colllected and we try again.
```
student@master:~$ kubectl get hpa -n ch8
NAME         REFERENCE               TARGETS    MINPODS   MAXPODS   REPLICAS   AGE
stress-cpu   Deployment/stress-cpu   195%/50%   1         2         2          83s
```

We notice that although we configured 1 replica, we are now running 2 replicas. Autoscaling worked and a maximum of 2 replicas were deployed.
```
student@master:~$ kubectl top pods -n ch8
NAME                         CPU(cores)   MEMORY(bytes)   
stress-cpu-cd4b44dff-f25mp   1948m        0Mi             
stress-cpu-cd4b44dff-nkfmw   1940m        0Mi  
```

6 - We can now remove this lab by removing "ch8" namespace.

